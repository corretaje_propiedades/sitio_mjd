Estructura del Sistema
======================

Ambiente Virtual
****************

Árbol de Carpetas
*****************

El árbol de carpetas lo dividimos en dos partes:

* Carpetas de proyecto

.. code-block:: bash
    :linenos:

    contributors.txt
    documentacion
    .git
    .gitignore
    gulp-configuration.sh
    gulpfile.js
    .idea
    mjd
    node_modules
    .pycharm-bash
    requirements.txt

* Sistema Django

.. code-block:: bash
    :linenos:

    .
    ├── apps
    │   ├── contactform
    │   │   ├── migrations
    │   │   ├── __pycache__
    │   │   ├── static
    │   │   └── templates
    │   ├── localizacion
    │   │   ├── migrations
    │   │   ├── __pycache__
    │   │   ├── scripts
    │   │   ├── static
    │   │   └── templates
    │   ├── pagina
    │   │   ├── migrations
    │   │   ├── __pycache__
    │   │   ├── static
    │   │   └── templates
    │   ├── principal
    │   │   ├── migrations
    │   │   ├── __pycache__
    │   │   ├── static
    │   │   └── templates
    │   └── propiedad
    │       ├── migrations
    │       ├── __pycache__
    │       └── static
    ├── files
    │   ├── geo
    │   ├── media
    │   ├── static
    │   │   ├── admin
    │   │   ├── css
    │   │   ├── django_extensions
    │   │   ├── flags
    │   │   ├── geo
    │   │   ├── gis
    │   │   ├── js
    │   │   ├── scss
    │   │   ├── semantic
    │   │   ├── static
    │   │   ├── templates
    │   │   └── vendor
    │   └── templates
    └── mjd
        ├── __pycache__
        └── settings
            └── __pycache__


Estructura de modelos o tablas
******************************

El esquema general del modelo del sistema django se puede obtener a través de la utilidad **django-extensions** ( `documentación <https://django-extensions.readthedocs.org/en/latest/>`_)

.. figure:: ../../../mjd/models.png
   :scale: 50 %
   :alt: Modelos del sistema MJD
   :align: center

Autodocumentación
*****************

La `autodocumentación <https://docs.djangoproject.com/en/1.9/ref/contrib/admin/admindocs/>`_ es un servicio que se puede implementar en el proyecto django, que describe la totalidad de las clases utilizadas y diseñadas en el sistema.

Se accede a través del sistema de administración del sitio y es de mucha utilidad para conocer los componentes, sin ser necesario indagar específicamente en el código.
