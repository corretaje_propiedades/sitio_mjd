Introducción
============

Este proyecto, desarrollado en Django (Python), consiste en un sitio para una corredora de propiedades que ofrece estas (casas, departamentos, oficinas, etc) para venta u arriendo.

Entre los requerimientos debe tener una interfaz web moderna con un sistema de búsqueda eficiente, la presentación de las propiedades con la información suficiente para entregar una experiencia satisfactoria a la búsqueda de los posibles clientes.

El desarrollo de interfaces web ha tenido una enorme evolución en sus componentes tecnológicos, en la actualidad podemos decir que los tres grandes ejes del desarrollo web se componen

* HTML
* CSS
* Javascript (JS)

Con el tiempo, la complejidad creciente de estos elementos ha requerido la generación de herramientas de trabajo que nos faciliten la tarea de integrar y desarrollar un sistema web.

Tanto así que se han creado Marcos de Trabajo (Frameworks) sujetos a lenguajes específicos que permiten la creación modular y estandarizada del elemento. De esta manera, para HTML el lenguaje Python nos facilita la vida con Django, para CSS se han creado los pseudolenguajes SASS o LESS que permiten agregar mayores funcionalidades al CSS, y Node para JS.

Al ser Javascript un lenguaje de programación que se ejecuta en cliente y Node la herramienta que permite hacer correr en servidor, se han creado sistemas de automatización de tareas que nos ahorrarán las labores de compilación y recarga del Browser.

En conjunto con esto, se utilizan herramientas de **posicionamiento geográfico** para lograr el despliegue de mapas y generación de búsquedas a través de consultas posicionales.

Pues bien, la plataforma es entonces un sistema de localización de propiedades.

