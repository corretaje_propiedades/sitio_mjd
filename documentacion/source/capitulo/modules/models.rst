Modelos
=======

Los modelos de MJD representan la estructura del sistema de base de datos para la generación de contenidos, estos derivan de cada aplicación en particular.

Localización
************

.. automodule:: apps.localizacion.models
    :members: