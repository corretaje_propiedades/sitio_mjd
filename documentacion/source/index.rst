.. mjd documentation master file, created by
   sphinx-quickstart on Sat Mar 19 10:05:52 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion sitio MJD Propiedades!
====================================

Esta es la documentación del sitio MJD que permitirá al lector el entendimiento cabal de la estructura funcional
interfaz y metodologías de desarrollo implementadas en la plataforma.

Contenidos:

.. toctree::
   :maxdepth: 2

   capitulo/intro
   capitulo/estructura
   capitulo/apps
   capitulo/interfaces
   capitulo/dev_method
   capitulo/features
   capitulo/automate

Ayuda SPHINX
============

* Poner imágenes o figuras en `documentacion <http://rest-sphinx-memo.readthedocs.org/en/latest/ReST.html#image-and-figure>`_

* Dibujar tablas en `documentacion <http://rest-sphinx-memo.readthedocs.org/en/latest/ReST.html#tables>`_

* Insertar códigos de diversos lenguaces con `code-block <http://build-me-the-docs-please.readthedocs.org/en/latest/Using_Sphinx/ShowingCodeExamplesInSphinx.html>`_

Recursos Django
===============

* Descripcion de Clases Vistas `ClassViews <http://ccbv.co.uk/>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

