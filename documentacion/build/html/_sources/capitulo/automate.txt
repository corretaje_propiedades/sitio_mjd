Desarrollo Automatizado
=======================

Configuración GULP
******************

Una de las opciones más destacadas y que se está usando bastante es el sistema Gulp, que permite ejecutar tareas en vivo a la medida que se va desarrollando el proyecto.

La instalación de Node recomendada es mediante compilzación de las fuentes (src), si se tienen las dependencias necesarias será posible instalar con lo siguiente:

.. code-block:: bash
    :linenos:

    ./configure
    make -j 4
    sudo make install

Visita la web `GULP <http://gulpjs.com/>`_

Se puede configurar gulp para que esté pendiende de los archivos modificados y actualice el navegador, tanto las plantillas html como los css o js que se modifiquen o creen.

En los siguientes enlaces explican como integrar estas `herramientas de frontend <https://lincolnloop.com/blog/integrating-front-end-tools-your-django-project/>`_

Browser `automatizado con Django:
<https://dezoito.github.io/2016/01/06/django-automate-browsersync/>`_

Solucion:

https://gist.github.com/dpineiden/b14f153e4884e17b847d

.. code-block:: js
    :linenos:

    var gulp = require('gulp');
    //var path = require('path');
    var sass = require('gulp-sass');
    var watch = require('gulp-watch');
    var minifycss = require('gulp-clean-css');
    var rename = require('gulp-rename');
    var gzip = require('gulp-gzip');
    var browserSync = require('browser-sync');
    var reload      = browserSync.reload;

    var gzip_options = {
        threshold: '1kb',
        gzipOptions: {
            level: 9
        }
    };
    var sassDir = './mjd/apps/'
    /* Compile Our Sass */
    gulp.task('sass', function() {
        return gulp.src(sassDir+'**/static/scss/*.scss')
            .pipe(sass())
           // .pipe(parsePath())
            .pipe(
                rename(function(path){
                    path.dirname += "/../css";
                })
            )
            .pipe(gulp.dest(sassDir))
            .pipe(rename({suffix: '.min'}))
            .pipe(minifycss())
            .pipe(gulp.dest(sassDir))
            .pipe(gzip(gzip_options))
            .pipe(gulp.dest(sassDir))
            //.pipe(livereload())
    });

    /* Watch Files For Changes */
    gulp.task('watch', function() {
        //livereload.listen(10000);
        gulp.watch('**/static/scss/*.scss', ['sass']);
        /* Trigger a live reload on any Django template changes */
        browserSync.init({
            notify: false,
            proxy: "localhost:8000"
        });
        gulp.watch(['./**/*.{scss,css,html,py,js}'], reload);

    });


    gulp.task('default', ['sass', 'watch']);

Configuración SPHINX
********************
