--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: tiger; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tiger;


ALTER SCHEMA tiger OWNER TO postgres;

--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: postgis_tiger_geocoder; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder WITH SCHEMA tiger;


--
-- Name: EXTENSION postgis_tiger_geocoder; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_tiger_geocoder IS 'PostGIS tiger geocoder and reverse geocoder';


--
-- Name: topology; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA topology;


ALTER SCHEMA topology OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: address_standardizer; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS address_standardizer WITH SCHEMA public;


--
-- Name: EXTENSION address_standardizer; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION address_standardizer IS 'Used to parse an address into constituent elements. Generally used to support geocoding address normalization step.';


--
-- Name: address_standardizer_data_us; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS address_standardizer_data_us WITH SCHEMA public;


--
-- Name: EXTENSION address_standardizer_data_us; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION address_standardizer_data_us IS 'Address Standardizer US dataset example';


--
-- Name: postgis_sfcgal; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;


--
-- Name: EXTENSION postgis_sfcgal; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';


--
-- Name: postgis_topology; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;


--
-- Name: EXTENSION postgis_topology; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_tools_dashboard_preferences; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE admin_tools_dashboard_preferences (
    id integer NOT NULL,
    data text NOT NULL,
    dashboard_id character varying(100) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE admin_tools_dashboard_preferences OWNER TO mjd_db;

--
-- Name: admin_tools_dashboard_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE admin_tools_dashboard_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_tools_dashboard_preferences_id_seq OWNER TO mjd_db;

--
-- Name: admin_tools_dashboard_preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE admin_tools_dashboard_preferences_id_seq OWNED BY admin_tools_dashboard_preferences.id;


--
-- Name: admin_tools_menu_bookmark; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE admin_tools_menu_bookmark (
    id integer NOT NULL,
    url character varying(255) NOT NULL,
    title character varying(255) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE admin_tools_menu_bookmark OWNER TO mjd_db;

--
-- Name: admin_tools_menu_bookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE admin_tools_menu_bookmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_tools_menu_bookmark_id_seq OWNER TO mjd_db;

--
-- Name: admin_tools_menu_bookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE admin_tools_menu_bookmark_id_seq OWNED BY admin_tools_menu_bookmark.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO mjd_db;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO mjd_db;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO mjd_db;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO mjd_db;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO mjd_db;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO mjd_db;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO mjd_db;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO mjd_db;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO mjd_db;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO mjd_db;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO mjd_db;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO mjd_db;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: contactform_contact; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE contactform_contact (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    pub_date timestamp with time zone NOT NULL,
    telefono integer NOT NULL,
    email character varying(254) NOT NULL,
    asunto character varying(120) NOT NULL,
    mensaje text NOT NULL
);


ALTER TABLE contactform_contact OWNER TO mjd_db;

--
-- Name: contactform_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE contactform_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contactform_contact_id_seq OWNER TO mjd_db;

--
-- Name: contactform_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE contactform_contact_id_seq OWNED BY contactform_contact.id;


--
-- Name: contactform_contact_servicio; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE contactform_contact_servicio (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    pagina_id integer NOT NULL
);


ALTER TABLE contactform_contact_servicio OWNER TO mjd_db;

--
-- Name: contactform_contact_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE contactform_contact_servicio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contactform_contact_servicio_id_seq OWNER TO mjd_db;

--
-- Name: contactform_contact_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE contactform_contact_servicio_id_seq OWNED BY contactform_contact_servicio.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO mjd_db;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO mjd_db;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_comment_flags; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_comment_flags (
    id integer NOT NULL,
    flag character varying(30) NOT NULL,
    flag_date timestamp with time zone NOT NULL,
    comment_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE django_comment_flags OWNER TO mjd_db;

--
-- Name: django_comment_flags_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_comment_flags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_comment_flags_id_seq OWNER TO mjd_db;

--
-- Name: django_comment_flags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_comment_flags_id_seq OWNED BY django_comment_flags.id;


--
-- Name: django_comments; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_comments (
    id integer NOT NULL,
    object_pk text NOT NULL,
    user_name character varying(50) NOT NULL,
    user_email character varying(254) NOT NULL,
    user_url character varying(200) NOT NULL,
    comment text NOT NULL,
    submit_date timestamp with time zone NOT NULL,
    ip_address inet,
    is_public boolean NOT NULL,
    is_removed boolean NOT NULL,
    content_type_id integer NOT NULL,
    site_id integer NOT NULL,
    user_id integer
);


ALTER TABLE django_comments OWNER TO mjd_db;

--
-- Name: django_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_comments_id_seq OWNER TO mjd_db;

--
-- Name: django_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_comments_id_seq OWNED BY django_comments.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO mjd_db;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO mjd_db;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO mjd_db;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO mjd_db;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO mjd_db;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE django_site OWNER TO mjd_db;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_site_id_seq OWNER TO mjd_db;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE easy_thumbnails_source (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL
);


ALTER TABLE easy_thumbnails_source OWNER TO mjd_db;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE easy_thumbnails_source_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_source_id_seq OWNER TO mjd_db;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE easy_thumbnails_source_id_seq OWNED BY easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE easy_thumbnails_thumbnail (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL
);


ALTER TABLE easy_thumbnails_thumbnail OWNER TO mjd_db;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE easy_thumbnails_thumbnail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_thumbnail_id_seq OWNER TO mjd_db;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE easy_thumbnails_thumbnail_id_seq OWNED BY easy_thumbnails_thumbnail.id;


--
-- Name: easy_thumbnails_thumbnaildimensions; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE easy_thumbnails_thumbnaildimensions (
    id integer NOT NULL,
    thumbnail_id integer NOT NULL,
    width integer,
    height integer,
    CONSTRAINT easy_thumbnails_thumbnaildimensions_height_check CHECK ((height >= 0)),
    CONSTRAINT easy_thumbnails_thumbnaildimensions_width_check CHECK ((width >= 0))
);


ALTER TABLE easy_thumbnails_thumbnaildimensions OWNER TO mjd_db;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE easy_thumbnails_thumbnaildimensions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE easy_thumbnails_thumbnaildimensions_id_seq OWNER TO mjd_db;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE easy_thumbnails_thumbnaildimensions_id_seq OWNED BY easy_thumbnails_thumbnaildimensions.id;


--
-- Name: guardian_groupobjectpermission; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE guardian_groupobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE guardian_groupobjectpermission OWNER TO mjd_db;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE guardian_groupobjectpermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE guardian_groupobjectpermission_id_seq OWNER TO mjd_db;

--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE guardian_groupobjectpermission_id_seq OWNED BY guardian_groupobjectpermission.id;


--
-- Name: guardian_userobjectpermission; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE guardian_userobjectpermission (
    id integer NOT NULL,
    object_pk character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    permission_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE guardian_userobjectpermission OWNER TO mjd_db;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE guardian_userobjectpermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE guardian_userobjectpermission_id_seq OWNER TO mjd_db;

--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE guardian_userobjectpermission_id_seq OWNED BY guardian_userobjectpermission.id;


--
-- Name: localizacion_comuna; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE localizacion_comuna (
    id integer NOT NULL,
    nombre character varying(150) NOT NULL,
    object_id integer,
    content_type_id integer,
    region_id integer NOT NULL,
    CONSTRAINT localizacion_comuna_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE localizacion_comuna OWNER TO mjd_db;

--
-- Name: localizacion_comuna_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE localizacion_comuna_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE localizacion_comuna_id_seq OWNER TO mjd_db;

--
-- Name: localizacion_comuna_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE localizacion_comuna_id_seq OWNED BY localizacion_comuna.id;


--
-- Name: localizacion_localizacion; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE localizacion_localizacion (
    id integer NOT NULL,
    ubicacion character varying(200) NOT NULL,
    numero integer NOT NULL,
    geo_point geometry(Point,4326) NOT NULL,
    object_id integer,
    comuna_id integer NOT NULL,
    content_type_id integer,
    CONSTRAINT localizacion_localizacion_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE localizacion_localizacion OWNER TO mjd_db;

--
-- Name: localizacion_localizacion_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE localizacion_localizacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE localizacion_localizacion_id_seq OWNER TO mjd_db;

--
-- Name: localizacion_localizacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE localizacion_localizacion_id_seq OWNED BY localizacion_localizacion.id;


--
-- Name: localizacion_region; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE localizacion_region (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    numero_decimal character varying(100) NOT NULL,
    numero_romano character varying(80) NOT NULL,
    pais character varying(2) NOT NULL,
    object_id integer,
    content_type_id integer,
    CONSTRAINT localizacion_region_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE localizacion_region OWNER TO mjd_db;

--
-- Name: localizacion_region_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE localizacion_region_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE localizacion_region_id_seq OWNER TO mjd_db;

--
-- Name: localizacion_region_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE localizacion_region_id_seq OWNED BY localizacion_region.id;


--
-- Name: pagina_clasificacion; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE pagina_clasificacion (
    id integer NOT NULL,
    tipo character varying(20) NOT NULL,
    slug_tipo character varying(50) NOT NULL,
    descripccion text NOT NULL,
    fecha_publicacion timestamp with time zone NOT NULL,
    object_id integer,
    content_type_id integer,
    CONSTRAINT pagina_clasificacion_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE pagina_clasificacion OWNER TO mjd_db;

--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE pagina_clasificacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pagina_clasificacion_id_seq OWNER TO mjd_db;

--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE pagina_clasificacion_id_seq OWNED BY pagina_clasificacion.id;


--
-- Name: pagina_pagina; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE pagina_pagina (
    id integer NOT NULL,
    titulo character varying(100) NOT NULL,
    slug_titulo character varying(50) NOT NULL,
    imagen character varying(100) NOT NULL,
    fecha_publicacion timestamp with time zone NOT NULL,
    resumen text NOT NULL,
    cuerpo text NOT NULL,
    object_id integer,
    clasificacion_id integer,
    content_type_id integer,
    CONSTRAINT pagina_pagina_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE pagina_pagina OWNER TO mjd_db;

--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE pagina_pagina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pagina_pagina_id_seq OWNER TO mjd_db;

--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE pagina_pagina_id_seq OWNED BY pagina_pagina.id;


--
-- Name: principal_busqueda; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_busqueda (
    id integer NOT NULL,
    tipo_adquisicion character varying(20) NOT NULL,
    estado character varying(30) NOT NULL,
    tipo_propiedad character varying(30) NOT NULL,
    tiempo_publicacion date NOT NULL,
    fecha_busqueda timestamp with time zone NOT NULL,
    object_id integer,
    content_type_id integer,
    CONSTRAINT principal_busqueda_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE principal_busqueda OWNER TO mjd_db;

--
-- Name: principal_busqueda_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_busqueda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_busqueda_id_seq OWNER TO mjd_db;

--
-- Name: principal_busqueda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_busqueda_id_seq OWNED BY principal_busqueda.id;


--
-- Name: principal_busqueda_set_resultados; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_busqueda_set_resultados (
    id integer NOT NULL,
    busqueda_id integer NOT NULL,
    propiedad_id integer NOT NULL
);


ALTER TABLE principal_busqueda_set_resultados OWNER TO mjd_db;

--
-- Name: principal_busqueda_set_resultados_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_busqueda_set_resultados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_busqueda_set_resultados_id_seq OWNER TO mjd_db;

--
-- Name: principal_busqueda_set_resultados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_busqueda_set_resultados_id_seq OWNED BY principal_busqueda_set_resultados.id;


--
-- Name: principal_busqueda_ubicacion; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_busqueda_ubicacion (
    id integer NOT NULL,
    gm2m_src_id integer NOT NULL,
    gm2m_ct_id integer NOT NULL,
    gm2m_pk character varying(16) NOT NULL
);


ALTER TABLE principal_busqueda_ubicacion OWNER TO mjd_db;

--
-- Name: principal_busqueda_ubicacion_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_busqueda_ubicacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_busqueda_ubicacion_id_seq OWNER TO mjd_db;

--
-- Name: principal_busqueda_ubicacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_busqueda_ubicacion_id_seq OWNED BY principal_busqueda_ubicacion.id;


--
-- Name: principal_configuracionhome; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_configuracionhome (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    mostrar_ultimas_propiedades boolean NOT NULL,
    cantidad_propiedades integer NOT NULL,
    orden_propiedades boolean NOT NULL,
    clase_css_propiedades_home character varying(100) NOT NULL,
    mostrar_destacados boolean NOT NULL,
    cantidad_destacados integer NOT NULL,
    aleatorio_destacados boolean NOT NULL,
    primera_fecha_destacados date NOT NULL,
    clase_css_propiedades_destacadas_home character varying(100) NOT NULL,
    activar_blog boolean NOT NULL,
    set_id integer NOT NULL,
    slug_nombre character varying(50) NOT NULL,
    CONSTRAINT principal_configuracionhome_cantidad_destacados_check CHECK ((cantidad_destacados >= 0)),
    CONSTRAINT principal_configuracionhome_cantidad_propiedades_check CHECK ((cantidad_propiedades >= 0))
);


ALTER TABLE principal_configuracionhome OWNER TO mjd_db;

--
-- Name: principal_configuracionhome_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_configuracionhome_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_configuracionhome_id_seq OWNER TO mjd_db;

--
-- Name: principal_configuracionhome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_configuracionhome_id_seq OWNED BY principal_configuracionhome.id;


--
-- Name: principal_homeimage; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_homeimage (
    id integer NOT NULL,
    imagen character varying(100) NOT NULL,
    etiqueta character varying(100) NOT NULL,
    set_id integer NOT NULL
);


ALTER TABLE principal_homeimage OWNER TO mjd_db;

--
-- Name: principal_homeimage_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_homeimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_homeimage_id_seq OWNER TO mjd_db;

--
-- Name: principal_homeimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_homeimage_id_seq OWNED BY principal_homeimage.id;


--
-- Name: principal_sethomeimage; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE principal_sethomeimage (
    id integer NOT NULL,
    nombre character varying(200) NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE principal_sethomeimage OWNER TO mjd_db;

--
-- Name: principal_sethomeimage_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE principal_sethomeimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE principal_sethomeimage_id_seq OWNER TO mjd_db;

--
-- Name: principal_sethomeimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE principal_sethomeimage_id_seq OWNED BY principal_sethomeimage.id;


--
-- Name: propiedad_arquitecto; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE propiedad_arquitecto (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    website character varying(300) NOT NULL,
    email character varying(254) NOT NULL,
    fono integer NOT NULL,
    object_id integer,
    content_type_id integer,
    CONSTRAINT propiedad_arquitecto_fono_check CHECK ((fono >= 0)),
    CONSTRAINT propiedad_arquitecto_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE propiedad_arquitecto OWNER TO mjd_db;

--
-- Name: propiedad_arquitecto_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE propiedad_arquitecto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE propiedad_arquitecto_id_seq OWNER TO mjd_db;

--
-- Name: propiedad_arquitecto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE propiedad_arquitecto_id_seq OWNED BY propiedad_arquitecto.id;


--
-- Name: propiedad_imagenpropiedad; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE propiedad_imagenpropiedad (
    id integer NOT NULL,
    imagen character varying(100) NOT NULL,
    etiqueta character varying(100) NOT NULL,
    principal boolean NOT NULL,
    propiedad_id integer NOT NULL
);


ALTER TABLE propiedad_imagenpropiedad OWNER TO mjd_db;

--
-- Name: propiedad_imagenpropiedad_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE propiedad_imagenpropiedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE propiedad_imagenpropiedad_id_seq OWNER TO mjd_db;

--
-- Name: propiedad_imagenpropiedad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE propiedad_imagenpropiedad_id_seq OWNED BY propiedad_imagenpropiedad.id;


--
-- Name: propiedad_preciopropiedad; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE propiedad_preciopropiedad (
    id integer NOT NULL,
    moneda character varying(100) NOT NULL,
    cantidad double precision NOT NULL,
    propiedad_id integer NOT NULL
);


ALTER TABLE propiedad_preciopropiedad OWNER TO mjd_db;

--
-- Name: propiedad_preciopropiedad_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE propiedad_preciopropiedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE propiedad_preciopropiedad_id_seq OWNER TO mjd_db;

--
-- Name: propiedad_preciopropiedad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE propiedad_preciopropiedad_id_seq OWNED BY propiedad_preciopropiedad.id;


--
-- Name: propiedad_propiedad; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE propiedad_propiedad (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    estado character varying(30) NOT NULL,
    caracter character varying(100) NOT NULL,
    tipo_propiedad character varying(100) NOT NULL,
    fecha_registro date NOT NULL,
    cantidad_dormitorios smallint NOT NULL,
    "cantidad_baños" integer NOT NULL,
    area double precision NOT NULL,
    descripcion text NOT NULL,
    fecha_entrega date NOT NULL,
    en_oferta boolean NOT NULL,
    object_id integer,
    arquitecto_id integer NOT NULL,
    content_type_id integer,
    localizacion_id integer NOT NULL,
    propietario_id integer NOT NULL,
    usuario_id integer NOT NULL,
    CONSTRAINT "propiedad_propiedad_cantidad_baños_check" CHECK (("cantidad_baños" >= 0)),
    CONSTRAINT propiedad_propiedad_cantidad_dormitorios_check CHECK ((cantidad_dormitorios >= 0)),
    CONSTRAINT propiedad_propiedad_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE propiedad_propiedad OWNER TO mjd_db;

--
-- Name: propiedad_propiedad_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE propiedad_propiedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE propiedad_propiedad_id_seq OWNER TO mjd_db;

--
-- Name: propiedad_propiedad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE propiedad_propiedad_id_seq OWNED BY propiedad_propiedad.id;


--
-- Name: propiedad_propietario; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE propiedad_propietario (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    website character varying(300) NOT NULL,
    email character varying(254) NOT NULL,
    fono integer NOT NULL,
    object_id integer,
    content_type_id integer,
    CONSTRAINT propiedad_propietario_fono_check CHECK ((fono >= 0)),
    CONSTRAINT propiedad_propietario_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE propiedad_propietario OWNER TO mjd_db;

--
-- Name: propiedad_propietario_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE propiedad_propietario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE propiedad_propietario_id_seq OWNER TO mjd_db;

--
-- Name: propiedad_propietario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE propiedad_propietario_id_seq OWNED BY propiedad_propietario.id;


--
-- Name: tagging_tag; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE tagging_tag (
    id integer NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE tagging_tag OWNER TO mjd_db;

--
-- Name: tagging_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE tagging_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tagging_tag_id_seq OWNER TO mjd_db;

--
-- Name: tagging_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE tagging_tag_id_seq OWNED BY tagging_tag.id;


--
-- Name: tagging_taggeditem; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE tagging_taggeditem (
    id integer NOT NULL,
    object_id integer NOT NULL,
    content_type_id integer NOT NULL,
    tag_id integer NOT NULL,
    CONSTRAINT tagging_taggeditem_object_id_check CHECK ((object_id >= 0))
);


ALTER TABLE tagging_taggeditem OWNER TO mjd_db;

--
-- Name: tagging_taggeditem_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE tagging_taggeditem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tagging_taggeditem_id_seq OWNER TO mjd_db;

--
-- Name: tagging_taggeditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE tagging_taggeditem_id_seq OWNED BY tagging_taggeditem.id;


--
-- Name: usuario_profileusermjd; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE usuario_profileusermjd (
    id integer NOT NULL,
    interaction integer NOT NULL,
    user_id integer NOT NULL,
    CONSTRAINT usuario_profileusermjd_interaction_check CHECK ((interaction >= 0))
);


ALTER TABLE usuario_profileusermjd OWNER TO mjd_db;

--
-- Name: usuario_profileusermjd_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE usuario_profileusermjd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_profileusermjd_id_seq OWNER TO mjd_db;

--
-- Name: usuario_profileusermjd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE usuario_profileusermjd_id_seq OWNED BY usuario_profileusermjd.id;


--
-- Name: usuario_usuariomjd; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE usuario_usuariomjd (
    user_ptr_id integer NOT NULL,
    telefono integer NOT NULL,
    imagen character varying(100) NOT NULL,
    grupo character varying(50) NOT NULL
);


ALTER TABLE usuario_usuariomjd OWNER TO mjd_db;

--
-- Name: zinnia_category; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_category (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    CONSTRAINT zinnia_category_level_check CHECK ((level >= 0)),
    CONSTRAINT zinnia_category_lft_check CHECK ((lft >= 0)),
    CONSTRAINT zinnia_category_rght_check CHECK ((rght >= 0)),
    CONSTRAINT zinnia_category_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE zinnia_category OWNER TO mjd_db;

--
-- Name: zinnia_category_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_category_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_category_id_seq OWNED BY zinnia_category.id;


--
-- Name: zinnia_entry; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_entry (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    status integer NOT NULL,
    start_publication timestamp with time zone,
    end_publication timestamp with time zone,
    creation_date timestamp with time zone NOT NULL,
    last_update timestamp with time zone NOT NULL,
    content text NOT NULL,
    comment_enabled boolean NOT NULL,
    pingback_enabled boolean NOT NULL,
    trackback_enabled boolean NOT NULL,
    comment_count integer NOT NULL,
    pingback_count integer NOT NULL,
    trackback_count integer NOT NULL,
    excerpt text NOT NULL,
    image character varying(100) NOT NULL,
    featured boolean NOT NULL,
    tags character varying(255) NOT NULL,
    login_required boolean NOT NULL,
    password character varying(50) NOT NULL,
    content_template character varying(250) NOT NULL,
    detail_template character varying(250) NOT NULL,
    image_caption text NOT NULL,
    lead text NOT NULL,
    publication_date timestamp with time zone NOT NULL
);


ALTER TABLE zinnia_entry OWNER TO mjd_db;

--
-- Name: zinnia_entry_authors; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_entry_authors (
    id integer NOT NULL,
    entry_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE zinnia_entry_authors OWNER TO mjd_db;

--
-- Name: zinnia_entry_authors_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_entry_authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_entry_authors_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_entry_authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_entry_authors_id_seq OWNED BY zinnia_entry_authors.id;


--
-- Name: zinnia_entry_categories; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_entry_categories (
    id integer NOT NULL,
    entry_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE zinnia_entry_categories OWNER TO mjd_db;

--
-- Name: zinnia_entry_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_entry_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_entry_categories_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_entry_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_entry_categories_id_seq OWNED BY zinnia_entry_categories.id;


--
-- Name: zinnia_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_entry_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_entry_id_seq OWNED BY zinnia_entry.id;


--
-- Name: zinnia_entry_related; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_entry_related (
    id integer NOT NULL,
    from_entry_id integer NOT NULL,
    to_entry_id integer NOT NULL
);


ALTER TABLE zinnia_entry_related OWNER TO mjd_db;

--
-- Name: zinnia_entry_related_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_entry_related_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_entry_related_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_entry_related_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_entry_related_id_seq OWNED BY zinnia_entry_related.id;


--
-- Name: zinnia_entry_sites; Type: TABLE; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE TABLE zinnia_entry_sites (
    id integer NOT NULL,
    entry_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE zinnia_entry_sites OWNER TO mjd_db;

--
-- Name: zinnia_entry_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: mjd_db
--

CREATE SEQUENCE zinnia_entry_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zinnia_entry_sites_id_seq OWNER TO mjd_db;

--
-- Name: zinnia_entry_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: mjd_db
--

ALTER SEQUENCE zinnia_entry_sites_id_seq OWNED BY zinnia_entry_sites.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY admin_tools_dashboard_preferences ALTER COLUMN id SET DEFAULT nextval('admin_tools_dashboard_preferences_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY admin_tools_menu_bookmark ALTER COLUMN id SET DEFAULT nextval('admin_tools_menu_bookmark_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY contactform_contact ALTER COLUMN id SET DEFAULT nextval('contactform_contact_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY contactform_contact_servicio ALTER COLUMN id SET DEFAULT nextval('contactform_contact_servicio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comment_flags ALTER COLUMN id SET DEFAULT nextval('django_comment_flags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comments ALTER COLUMN id SET DEFAULT nextval('django_comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_source_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions ALTER COLUMN id SET DEFAULT nextval('easy_thumbnails_thumbnaildimensions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_groupobjectpermission ALTER COLUMN id SET DEFAULT nextval('guardian_groupobjectpermission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_userobjectpermission ALTER COLUMN id SET DEFAULT nextval('guardian_userobjectpermission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_comuna ALTER COLUMN id SET DEFAULT nextval('localizacion_comuna_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_localizacion ALTER COLUMN id SET DEFAULT nextval('localizacion_localizacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_region ALTER COLUMN id SET DEFAULT nextval('localizacion_region_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY pagina_clasificacion ALTER COLUMN id SET DEFAULT nextval('pagina_clasificacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY pagina_pagina ALTER COLUMN id SET DEFAULT nextval('pagina_pagina_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda ALTER COLUMN id SET DEFAULT nextval('principal_busqueda_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_set_resultados ALTER COLUMN id SET DEFAULT nextval('principal_busqueda_set_resultados_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_ubicacion ALTER COLUMN id SET DEFAULT nextval('principal_busqueda_ubicacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_configuracionhome ALTER COLUMN id SET DEFAULT nextval('principal_configuracionhome_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_homeimage ALTER COLUMN id SET DEFAULT nextval('principal_homeimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_sethomeimage ALTER COLUMN id SET DEFAULT nextval('principal_sethomeimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_arquitecto ALTER COLUMN id SET DEFAULT nextval('propiedad_arquitecto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_imagenpropiedad ALTER COLUMN id SET DEFAULT nextval('propiedad_imagenpropiedad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_preciopropiedad ALTER COLUMN id SET DEFAULT nextval('propiedad_preciopropiedad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad ALTER COLUMN id SET DEFAULT nextval('propiedad_propiedad_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propietario ALTER COLUMN id SET DEFAULT nextval('propiedad_propietario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY tagging_tag ALTER COLUMN id SET DEFAULT nextval('tagging_tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY tagging_taggeditem ALTER COLUMN id SET DEFAULT nextval('tagging_taggeditem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY usuario_profileusermjd ALTER COLUMN id SET DEFAULT nextval('usuario_profileusermjd_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_category ALTER COLUMN id SET DEFAULT nextval('zinnia_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry ALTER COLUMN id SET DEFAULT nextval('zinnia_entry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_authors ALTER COLUMN id SET DEFAULT nextval('zinnia_entry_authors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_categories ALTER COLUMN id SET DEFAULT nextval('zinnia_entry_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_related ALTER COLUMN id SET DEFAULT nextval('zinnia_entry_related_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_sites ALTER COLUMN id SET DEFAULT nextval('zinnia_entry_sites_id_seq'::regclass);


--
-- Data for Name: admin_tools_dashboard_preferences; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY admin_tools_dashboard_preferences (id, data, dashboard_id, user_id) FROM stdin;
1	{}	dashboard	1
\.


--
-- Name: admin_tools_dashboard_preferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('admin_tools_dashboard_preferences_id_seq', 1, true);


--
-- Data for Name: admin_tools_menu_bookmark; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY admin_tools_menu_bookmark (id, url, title, user_id) FROM stdin;
\.


--
-- Name: admin_tools_menu_bookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('admin_tools_menu_bookmark_id_seq', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add source	7	add_source
20	Can change source	7	change_source
21	Can delete source	7	delete_source
22	Can add thumbnail	8	add_thumbnail
23	Can change thumbnail	8	change_thumbnail
24	Can delete thumbnail	8	delete_thumbnail
25	Can add thumbnail dimensions	9	add_thumbnaildimensions
26	Can change thumbnail dimensions	9	change_thumbnaildimensions
27	Can delete thumbnail dimensions	9	delete_thumbnaildimensions
28	Can add user object permission	10	add_userobjectpermission
29	Can change user object permission	10	change_userobjectpermission
30	Can delete user object permission	10	delete_userobjectpermission
31	Can add group object permission	11	add_groupobjectpermission
32	Can change group object permission	11	change_groupobjectpermission
33	Can delete group object permission	11	delete_groupobjectpermission
34	Can add Clasificación	12	add_clasificacion
35	Can change Clasificación	12	change_clasificacion
36	Can delete Clasificación	12	delete_clasificacion
37	Can add Página	13	add_pagina
38	Can change Página	13	change_pagina
39	Can delete Página	13	delete_pagina
40	Can add Busqueda de propiedades	14	add_busqueda
41	Can change Busqueda de propiedades	14	change_busqueda
42	Can delete Busqueda de propiedades	14	delete_busqueda
43	Can add Formulario de Contacto	15	add_contact
44	Can change Formulario de Contacto	15	change_contact
45	Can delete Formulario de Contacto	15	delete_contact
46	Can add Region	16	add_region
47	Can change Region	16	change_region
48	Can delete Region	16	delete_region
49	Can add Comuna	17	add_comuna
50	Can change Comuna	17	change_comuna
51	Can delete Comuna	17	delete_comuna
52	Can add Localización	18	add_localizacion
53	Can change Localización	18	change_localizacion
54	Can delete Localización	18	delete_localizacion
55	Can add Propietario	19	add_propietario
56	Can change Propietario	19	change_propietario
57	Can delete Propietario	19	delete_propietario
58	Can add Arquitecto	20	add_arquitecto
59	Can change Arquitecto	20	change_arquitecto
60	Can delete Arquitecto	20	delete_arquitecto
61	Can add Propiedad	21	add_propiedad
62	Can change Propiedad	21	change_propiedad
63	Can delete Propiedad	21	delete_propiedad
64	Can add user	22	add_usuariomjd
65	Can change user	22	change_usuariomjd
66	Can delete user	22	delete_usuariomjd
67	Can add Perfil	23	add_profileusermjd
68	Can change Perfil	23	change_profileusermjd
69	Can delete Perfil	23	delete_profileusermjd
70	Can add imagen	24	add_imagenpropiedad
71	Can change imagen	24	change_imagenpropiedad
72	Can delete imagen	24	delete_imagenpropiedad
73	Can add Precio Propiedad	25	add_preciopropiedad
74	Can change Precio Propiedad	25	change_preciopropiedad
75	Can delete Precio Propiedad	25	delete_preciopropiedad
76	Can add set-imagenes-home	26	add_sethomeimage
77	Can change set-imagenes-home	26	change_sethomeimage
78	Can delete set-imagenes-home	26	delete_sethomeimage
79	Can add imagen-home	27	add_homeimage
80	Can change imagen-home	27	change_homeimage
81	Can delete imagen-home	27	delete_homeimage
82	Can add configuracion-home	28	add_configuracionhome
83	Can change configuracion-home	28	change_configuracionhome
84	Can delete configuracion-home	28	delete_configuracionhome
85	Can add site	29	add_site
86	Can change site	29	change_site
87	Can delete site	29	delete_site
88	Can add comment	30	add_comment
89	Can change comment	30	change_comment
90	Can delete comment	30	delete_comment
91	Can moderate comments	30	can_moderate
92	Can add comment flag	31	add_commentflag
93	Can change comment flag	31	change_commentflag
94	Can delete comment flag	31	delete_commentflag
95	Can add tag	32	add_tag
96	Can change tag	32	change_tag
97	Can delete tag	32	delete_tag
98	Can add tagged item	33	add_taggeditem
99	Can change tagged item	33	change_taggeditem
100	Can delete tagged item	33	delete_taggeditem
101	Can add entry	34	add_entry
102	Can change entry	34	change_entry
103	Can delete entry	34	delete_entry
104	Can view all entries	34	can_view_all
105	Can change status	34	can_change_status
106	Can change author(s)	34	can_change_author
107	Can add category	35	add_category
108	Can change category	35	change_category
109	Can delete category	35	delete_category
110	Can add author	4	add_author
111	Can change author	4	change_author
112	Can delete author	4	delete_author
113	Can add bookmark	37	add_bookmark
114	Can change bookmark	37	change_bookmark
115	Can delete bookmark	37	delete_bookmark
116	Can add dashboard preferences	38	add_dashboardpreferences
117	Can change dashboard preferences	38	change_dashboardpreferences
118	Can delete dashboard preferences	38	delete_dashboardpreferences
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_permission_id_seq', 118, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
666	!tnGXLqkIXNXgOhgNjrq1ZeX1eD4qVrPQBAIloGyr	\N	f	AnonymousUser				f	t	2016-04-18 23:45:17.952065-03
3	12365478	\N	f	monogatari				f	t	2016-04-28 01:02:11-03
4	pbkdf2_sha256$24000$ppOoZoj13mgV$j8oijBQG8SOmCNF9USYISFLOu06S0pOABwcktuOhQv0=	\N	f	baloo				f	t	2016-04-28 01:11:06-03
5	pbkdf2_sha256$24000$R2FYyWnKUQws$vGWsS1jPRapqb1lLI7a7XWb0KEO7WmFebbNFe1jz8Os=	\N	f	baguera				f	t	2016-04-28 01:13:08-03
2	pbkdf2_sha256$24000$NzYehzzm6vs9$SAUDyeRKVlsVF7WSPoYyTGPBDz6H4dvZAZjvp0/417k=	2016-05-04 00:15:47.537629-03	f	oso_meloso				f	t	2016-04-19 00:20:18-03
1	pbkdf2_sha256$24000$wc2h87TfDXKR$oGiblmr1cp9rq/M2iHIbSn9KLbiSLabyUq+yAsd0A1Y=	2016-05-22 20:57:31.803806-03	t	admin			ad@lo.oo	t	t	2016-04-18 23:46:05.445962-03
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_user_id_seq', 5, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: contactform_contact; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY contactform_contact (id, nombre, pub_date, telefono, email, asunto, mensaje) FROM stdin;
\.


--
-- Name: contactform_contact_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('contactform_contact_id_seq', 1, false);


--
-- Data for Name: contactform_contact_servicio; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY contactform_contact_servicio (id, contact_id, pagina_id) FROM stdin;
\.


--
-- Name: contactform_contact_servicio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('contactform_contact_servicio_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-04-19 00:21:33.97213-03	2	oso_meloso	1	Added. Added user "oso_meloso".	4	1
2	2016-04-19 00:21:51.885052-03	2	oso_meloso	2	Changed password.	4	1
3	2016-04-19 13:52:49.572443-03	2	oso_meloso	2	Changed imagen for user "oso_meloso".	4	1
4	2016-04-28 01:04:32.512557-03	3	monogatari	1	Added. Added user "monogatari".	4	1
5	2016-04-28 01:12:39.067027-03	4	baloo	1	Added. Added user "baloo".	4	1
6	2016-04-28 01:12:52.158349-03	4	baloo	2	Changed password.	4	1
7	2016-04-28 01:13:39.629194-03	5	baguera	1	Added. Added user "baguera".	4	1
8	2016-04-28 01:13:50.159032-03	5	baguera	2	Changed password.	4	1
9	2016-04-28 18:21:08.893694-03	4	Daniel Ortiz	3		19	1
10	2016-04-28 18:21:08.936907-03	7	Francisca Olzman	3		19	1
11	2016-04-28 18:21:08.947954-03	5	Ismael Vergara	3		19	1
12	2016-04-28 18:21:08.959059-03	3	José Martinez	3		19	1
13	2016-04-28 18:21:08.970296-03	6	Pedro Western	3		19	1
14	2016-04-28 18:22:44.065541-03	9	Daniel Ortiz	3		19	1
15	2016-04-28 18:22:44.082264-03	12	Francisca Olzman	3		19	1
16	2016-04-28 18:22:44.093354-03	10	Ismael Vergara	3		19	1
17	2016-04-28 18:22:44.104387-03	8	José Martinez	3		19	1
18	2016-04-28 18:22:44.115631-03	11	Pedro Western	3		19	1
19	2016-04-28 18:37:15.868224-03	19	Daniel Ortiz	3		19	1
20	2016-04-28 18:37:15.87903-03	14	Daniel Ortiz	3		19	1
21	2016-04-28 18:37:15.89031-03	22	Francisca Olzman	3		19	1
22	2016-04-28 18:37:15.901352-03	17	Francisca Olzman	3		19	1
23	2016-04-28 18:37:15.912498-03	20	Ismael Vergara	3		19	1
24	2016-04-28 18:37:15.923635-03	15	Ismael Vergara	3		19	1
25	2016-04-28 18:37:15.934728-03	18	José Martinez	3		19	1
26	2016-04-28 18:37:15.945819-03	13	José Martinez	3		19	1
27	2016-04-28 18:37:15.95694-03	21	Pedro Western	3		19	1
28	2016-04-28 18:37:15.968111-03	16	Pedro Western	3		19	1
29	2016-04-28 18:37:22.479407-03	4	Esteban Figueroa	3		20	1
30	2016-04-28 18:37:22.490284-03	6	José Dono	3		20	1
31	2016-04-28 18:37:22.501457-03	1	Juan Estoril	3		20	1
32	2016-04-28 18:37:22.512565-03	3	Lina Dona	3		20	1
33	2016-04-28 18:37:22.523683-03	7	Luna Dominguez	3		20	1
34	2016-04-28 18:37:22.534786-03	2	Marina Estevez	3		20	1
35	2016-04-28 18:37:22.546311-03	5	Martin Cárcamo	3		20	1
36	2016-04-28 18:49:21.901776-03	24	Freire 512, Concepción	3		18	1
37	2016-04-28 18:49:21.934218-03	23	Colón 615, Los Ángeles	3		18	1
38	2016-04-28 18:49:21.954347-03	22	1 Sur 1623, Talca	3		18	1
39	2016-04-28 18:49:21.963885-03	21	Arturo Prat 733, Curicó	3		18	1
40	2016-04-28 18:49:21.975004-03	20	Brasil 1082, Rancagua	3		18	1
41	2016-04-28 18:49:21.986248-03	19	Los Carrera 705, La Calera	3		18	1
42	2016-04-28 18:49:21.997345-03	18	Santiago 598, Viña del Mar	3		18	1
43	2016-04-28 18:49:22.008456-03	17	Teatinos 425, Santiago	3		18	1
44	2016-04-28 18:49:22.019552-03	16	Artesanos 657, Recoleta	3		18	1
45	2016-04-28 18:49:22.030614-03	15	Freire 399, San Bernardo	3		18	1
46	2016-04-28 18:49:22.041754-03	14	Av. Santa Rosa  12786, La Pintana	3		18	1
47	2016-04-28 18:49:22.052921-03	13	Serrano 144, Melipilla	3		18	1
48	2016-04-28 18:49:22.063956-03	12	Vicuña Mackena 1918, Peñaflor	3		18	1
49	2016-04-28 18:49:22.075091-03	11	Av. Independencia 3517, Conchalí	3		18	1
50	2016-04-28 18:49:22.086202-03	10	Henriquez 483, Villarrica	3		18	1
51	2016-04-28 18:49:22.097371-03	9	Independencia 267, Ovalle	3		18	1
52	2016-04-28 18:49:22.108427-03	8	Tarapacá 723, Iquique	3		18	1
53	2016-04-28 18:49:22.119534-03	7	Nancy 725, Ñuñoa	3		18	1
54	2016-04-28 18:49:22.130711-03	6	José Miguel de la Barra  460, Santiago	3		18	1
55	2016-04-28 18:49:22.141807-03	5	Av. Suecia 3304, Ñuñoa	3		18	1
56	2016-04-28 18:49:22.152886-03	4	Av. Príncipe de Gales 6465, La Reina	3		18	1
57	2016-04-28 18:49:22.164027-03	3	Los Duraznos  95, La Pintana	3		18	1
58	2016-04-28 18:49:22.181169-03	2	Arturo Prat 34, Santiago	3		18	1
59	2016-04-28 18:49:22.186186-03	1	Presidente Lincoln 1615, Recoleta	3		18	1
60	2016-04-28 18:49:35.168299-03	18	Esteban Figueroa	3		20	1
61	2016-04-28 18:49:35.186413-03	11	Esteban Figueroa	3		20	1
62	2016-04-28 18:49:35.197484-03	20	José Dono	3		20	1
63	2016-04-28 18:49:35.208864-03	13	José Dono	3		20	1
64	2016-04-28 18:49:35.219696-03	15	Juan Estoril	3		20	1
65	2016-04-28 18:49:35.230833-03	8	Juan Estoril	3		20	1
66	2016-04-28 18:49:35.242046-03	17	Lina Dona	3		20	1
67	2016-04-28 18:49:35.253203-03	10	Lina Dona	3		20	1
68	2016-04-28 18:49:35.264249-03	21	Luna Dominguez	3		20	1
69	2016-04-28 18:49:35.275374-03	14	Luna Dominguez	3		20	1
70	2016-04-28 18:49:35.286468-03	16	Marina Estevez	3		20	1
71	2016-04-28 18:49:35.297556-03	9	Marina Estevez	3		20	1
72	2016-04-28 18:49:35.308716-03	19	Martin Cárcamo	3		20	1
73	2016-04-28 18:49:35.319778-03	12	Martin Cárcamo	3		20	1
74	2016-04-28 18:49:45.606516-03	29	Daniel Ortiz	3		19	1
75	2016-04-28 18:49:45.619956-03	24	Daniel Ortiz	3		19	1
76	2016-04-28 18:49:45.631097-03	32	Francisca Olzman	3		19	1
77	2016-04-28 18:49:45.651421-03	27	Francisca Olzman	3		19	1
78	2016-04-28 18:49:45.664346-03	30	Ismael Vergara	3		19	1
79	2016-04-28 18:49:45.675573-03	25	Ismael Vergara	3		19	1
80	2016-04-28 18:49:45.686637-03	28	José Martinez	3		19	1
81	2016-04-28 18:49:45.697763-03	23	José Martinez	3		19	1
82	2016-04-28 18:49:45.708878-03	31	Pedro Western	3		19	1
83	2016-04-28 18:49:45.719976-03	26	Pedro Western	3		19	1
84	2016-04-28 18:56:06.284749-03	25	Esteban Figueroa	3		20	1
85	2016-04-28 18:56:06.302471-03	27	José Dono	3		20	1
86	2016-04-28 18:56:06.313553-03	22	Juan Estoril	3		20	1
87	2016-04-28 18:56:06.325004-03	24	Lina Dona	3		20	1
88	2016-04-28 18:56:06.335929-03	28	Luna Dominguez	3		20	1
89	2016-04-28 18:56:06.346992-03	23	Marina Estevez	3		20	1
90	2016-04-28 18:56:06.358134-03	26	Martin Cárcamo	3		20	1
91	2016-04-28 18:56:16.959262-03	34	Daniel Ortiz	3		19	1
92	2016-04-28 18:56:16.969424-03	37	Francisca Olzman	3		19	1
93	2016-04-28 18:56:16.980399-03	35	Ismael Vergara	3		19	1
94	2016-04-28 18:56:16.991559-03	33	José Martinez	3		19	1
95	2016-04-28 18:56:17.00263-03	36	Pedro Western	3		19	1
96	2016-04-28 18:56:32.653734-03	48	Freire 512, Concepción	3		18	1
97	2016-04-28 18:56:32.669551-03	47	Colón 615, Los Ángeles	3		18	1
98	2016-04-28 18:56:32.681216-03	46	1 Sur 1623, Talca	3		18	1
99	2016-04-28 18:56:32.691757-03	45	Arturo Prat 733, Curicó	3		18	1
100	2016-04-28 18:56:32.702795-03	44	Brasil 1082, Rancagua	3		18	1
101	2016-04-28 18:56:32.713969-03	43	Los Carrera 705, La Calera	3		18	1
102	2016-04-28 18:56:32.725051-03	42	Santiago 598, Viña del Mar	3		18	1
103	2016-04-28 18:56:32.736214-03	41	Teatinos 425, Santiago	3		18	1
104	2016-04-28 18:56:32.747368-03	40	Artesanos 657, Recoleta	3		18	1
105	2016-04-28 18:56:32.758469-03	39	Freire 399, San Bernardo	3		18	1
106	2016-04-28 18:56:32.76958-03	38	Av. Santa Rosa  12786, La Pintana	3		18	1
107	2016-04-28 18:56:32.781855-03	37	Serrano 144, Melipilla	3		18	1
108	2016-04-28 18:56:32.79183-03	36	Vicuña Mackena 1918, Peñaflor	3		18	1
109	2016-04-28 18:56:32.802969-03	35	Av. Independencia 3517, Conchalí	3		18	1
110	2016-04-28 18:56:32.814025-03	34	Henriquez 483, Villarrica	3		18	1
111	2016-04-28 18:56:32.825165-03	33	Independencia 267, Ovalle	3		18	1
112	2016-04-28 18:56:32.836289-03	32	Tarapacá 723, Iquique	3		18	1
113	2016-04-28 18:56:32.847348-03	31	Nancy 725, Ñuñoa	3		18	1
114	2016-04-28 18:56:32.858502-03	30	José Miguel de la Barra  460, Santiago	3		18	1
115	2016-04-28 18:56:32.869643-03	29	Av. Suecia 3304, Ñuñoa	3		18	1
116	2016-04-28 18:56:32.881172-03	28	Av. Príncipe de Gales 6465, La Reina	3		18	1
117	2016-04-28 18:56:32.891786-03	27	Los Duraznos  95, La Pintana	3		18	1
118	2016-04-28 18:56:32.90295-03	26	Arturo Prat 34, Santiago	3		18	1
119	2016-04-28 18:56:32.914068-03	25	Presidente Lincoln 1615, Recoleta	3		18	1
120	2016-04-28 19:09:16.111804-03	72	Freire 512, Concepción	3		18	1
121	2016-04-28 19:09:16.138231-03	71	Colón 615, Los Ángeles	3		18	1
122	2016-04-28 19:09:16.166063-03	70	1 Sur 1623, Talca	3		18	1
123	2016-04-28 19:09:16.177144-03	69	Arturo Prat 733, Curicó	3		18	1
124	2016-04-28 19:09:16.188229-03	68	Brasil 1082, Rancagua	3		18	1
125	2016-04-28 19:09:16.199376-03	67	Los Carrera 705, La Calera	3		18	1
126	2016-04-28 19:09:16.210492-03	66	Santiago 598, Viña del Mar	3		18	1
127	2016-04-28 19:09:16.221647-03	65	Teatinos 425, Santiago	3		18	1
128	2016-04-28 19:09:16.232759-03	64	Artesanos 657, Recoleta	3		18	1
129	2016-04-28 19:09:16.243816-03	63	Freire 399, San Bernardo	3		18	1
130	2016-04-28 19:09:16.255034-03	62	Av. Santa Rosa  12786, La Pintana	3		18	1
131	2016-04-28 19:09:16.266227-03	61	Serrano 144, Melipilla	3		18	1
132	2016-04-28 19:09:16.277284-03	60	Vicuña Mackena 1918, Peñaflor	3		18	1
133	2016-04-28 19:09:16.288282-03	59	Av. Independencia 3517, Conchalí	3		18	1
134	2016-04-28 19:09:16.299447-03	58	Henriquez 483, Villarrica	3		18	1
135	2016-04-28 19:09:16.310554-03	57	Independencia 267, Ovalle	3		18	1
136	2016-04-28 19:09:16.321785-03	56	Tarapacá 723, Iquique	3		18	1
137	2016-04-28 19:09:16.332747-03	55	Nancy 725, Ñuñoa	3		18	1
138	2016-04-28 19:09:16.34392-03	54	José Miguel de la Barra  460, Santiago	3		18	1
139	2016-04-28 19:09:16.35501-03	53	Av. Suecia 3304, Ñuñoa	3		18	1
140	2016-04-28 19:09:16.366205-03	52	Av. Príncipe de Gales 6465, La Reina	3		18	1
141	2016-04-28 19:09:16.377246-03	51	Los Duraznos  95, La Pintana	3		18	1
142	2016-04-28 19:09:16.388378-03	50	Arturo Prat 34, Santiago	3		18	1
143	2016-04-28 19:09:16.399488-03	49	Presidente Lincoln 1615, Recoleta	3		18	1
144	2016-04-28 19:09:29.926426-03	39	Daniel Ortiz	3		19	1
145	2016-04-28 19:09:29.944255-03	42	Francisca Olzman	3		19	1
146	2016-04-28 19:09:29.955338-03	40	Ismael Vergara	3		19	1
147	2016-04-28 19:09:29.966444-03	38	José Martinez	3		19	1
148	2016-04-28 19:09:29.977492-03	41	Pedro Western	3		19	1
149	2016-04-28 19:09:36.82954-03	32	Esteban Figueroa	3		20	1
150	2016-04-28 19:09:36.844379-03	34	José Dono	3		20	1
151	2016-04-28 19:09:36.855278-03	29	Juan Estoril	3		20	1
152	2016-04-28 19:09:36.866509-03	31	Lina Dona	3		20	1
153	2016-04-28 19:09:36.877629-03	35	Luna Dominguez	3		20	1
154	2016-04-28 19:09:36.888792-03	30	Marina Estevez	3		20	1
155	2016-04-28 19:09:36.899847-03	33	Martin Cárcamo	3		20	1
156	2016-04-28 19:13:19.314347-03	96	Freire 512, Concepción	3		18	1
157	2016-04-28 19:13:19.326235-03	95	Colón 615, Los Ángeles	3		18	1
158	2016-04-28 19:13:19.33728-03	94	1 Sur 1623, Talca	3		18	1
159	2016-04-28 19:13:19.348548-03	93	Arturo Prat 733, Curicó	3		18	1
160	2016-04-28 19:13:19.36115-03	92	Brasil 1082, Rancagua	3		18	1
161	2016-04-28 19:13:19.370735-03	91	Los Carrera 705, La Calera	3		18	1
162	2016-04-28 19:13:19.381821-03	90	Santiago 598, Viña del Mar	3		18	1
163	2016-04-28 19:13:19.392915-03	89	Teatinos 425, Santiago	3		18	1
164	2016-04-28 19:13:19.404019-03	88	Artesanos 657, Recoleta	3		18	1
165	2016-04-28 19:13:19.41514-03	87	Freire 399, San Bernardo	3		18	1
166	2016-04-28 19:13:19.42622-03	86	Av. Santa Rosa  12786, La Pintana	3		18	1
167	2016-04-28 19:13:19.437318-03	85	Serrano 144, Melipilla	3		18	1
168	2016-04-28 19:13:19.448497-03	84	Vicuña Mackena 1918, Peñaflor	3		18	1
169	2016-04-28 19:13:19.459615-03	83	Av. Independencia 3517, Conchalí	3		18	1
170	2016-04-28 19:13:19.470725-03	82	Henriquez 483, Villarrica	3		18	1
171	2016-04-28 19:13:19.481802-03	81	Independencia 267, Ovalle	3		18	1
172	2016-04-28 19:13:19.49298-03	80	Tarapacá 723, Iquique	3		18	1
173	2016-04-28 19:13:19.504072-03	79	Nancy 725, Ñuñoa	3		18	1
174	2016-04-28 19:13:19.515396-03	78	José Miguel de la Barra  460, Santiago	3		18	1
175	2016-04-28 19:13:19.526319-03	77	Av. Suecia 3304, Ñuñoa	3		18	1
176	2016-04-28 19:13:19.537395-03	76	Av. Príncipe de Gales 6465, La Reina	3		18	1
177	2016-04-28 19:13:19.548533-03	75	Los Duraznos  95, La Pintana	3		18	1
178	2016-04-28 19:13:19.559677-03	74	Arturo Prat 34, Santiago	3		18	1
179	2016-04-28 19:13:19.570913-03	73	Presidente Lincoln 1615, Recoleta	3		18	1
180	2016-04-28 19:13:34.017406-03	44	Daniel Ortiz	3		19	1
181	2016-04-28 19:13:34.0264-03	47	Francisca Olzman	3		19	1
182	2016-04-28 19:13:34.03758-03	45	Ismael Vergara	3		19	1
183	2016-04-28 19:13:34.048839-03	43	José Martinez	3		19	1
184	2016-04-28 19:13:34.059907-03	46	Pedro Western	3		19	1
185	2016-04-28 19:13:38.923444-03	39	Esteban Figueroa	3		20	1
186	2016-04-28 19:13:38.93767-03	41	José Dono	3		20	1
187	2016-04-28 19:13:38.94876-03	36	Juan Estoril	3		20	1
188	2016-04-28 19:13:38.959936-03	38	Lina Dona	3		20	1
189	2016-04-28 19:13:38.971082-03	42	Luna Dominguez	3		20	1
190	2016-04-28 19:13:38.98218-03	37	Marina Estevez	3		20	1
191	2016-04-28 19:13:38.993391-03	40	Martin Cárcamo	3		20	1
192	2016-04-28 19:20:00.61592-03	49	Daniel Ortiz	3		19	1
193	2016-04-28 19:20:00.631099-03	52	Francisca Olzman	3		19	1
194	2016-04-28 19:20:00.642263-03	50	Ismael Vergara	3		19	1
195	2016-04-28 19:20:00.653392-03	48	José Martinez	3		19	1
196	2016-04-28 19:20:00.664481-03	51	Pedro Western	3		19	1
197	2016-04-28 19:20:05.104292-03	46	Esteban Figueroa	3		20	1
198	2016-04-28 19:20:05.156451-03	48	José Dono	3		20	1
199	2016-04-28 19:20:05.175718-03	43	Juan Estoril	3		20	1
200	2016-04-28 19:20:05.186784-03	45	Lina Dona	3		20	1
201	2016-04-28 19:20:05.197886-03	49	Luna Dominguez	3		20	1
202	2016-04-28 19:20:05.209115-03	44	Marina Estevez	3		20	1
203	2016-04-28 19:20:05.220229-03	47	Martin Cárcamo	3		20	1
204	2016-04-28 19:20:17.452062-03	120	Freire 512, Concepción	3		18	1
205	2016-04-28 19:20:17.464729-03	119	Colón 615, Los Ángeles	3		18	1
206	2016-04-28 19:20:17.475864-03	118	1 Sur 1623, Talca	3		18	1
207	2016-04-28 19:20:17.487-03	117	Arturo Prat 733, Curicó	3		18	1
208	2016-04-28 19:20:17.49803-03	116	Brasil 1082, Rancagua	3		18	1
209	2016-04-28 19:20:17.509294-03	115	Los Carrera 705, La Calera	3		18	1
210	2016-04-28 19:20:17.520296-03	114	Santiago 598, Viña del Mar	3		18	1
211	2016-04-28 19:20:17.531466-03	113	Teatinos 425, Santiago	3		18	1
212	2016-04-28 19:20:17.542544-03	112	Artesanos 657, Recoleta	3		18	1
213	2016-04-28 19:20:17.553682-03	111	Freire 399, San Bernardo	3		18	1
214	2016-04-28 19:20:17.56479-03	110	Av. Santa Rosa  12786, La Pintana	3		18	1
215	2016-04-28 19:20:17.575992-03	109	Serrano 144, Melipilla	3		18	1
216	2016-04-28 19:20:17.587051-03	108	Vicuña Mackena 1918, Peñaflor	3		18	1
217	2016-04-28 19:20:17.598144-03	107	Av. Independencia 3517, Conchalí	3		18	1
218	2016-04-28 19:20:17.609209-03	106	Henriquez 483, Villarrica	3		18	1
219	2016-04-28 19:20:17.620321-03	105	Independencia 267, Ovalle	3		18	1
220	2016-04-28 19:20:17.63149-03	104	Tarapacá 723, Iquique	3		18	1
221	2016-04-28 19:20:17.642582-03	103	Nancy 725, Ñuñoa	3		18	1
222	2016-04-28 19:20:17.653672-03	102	José Miguel de la Barra  460, Santiago	3		18	1
223	2016-04-28 19:20:17.664823-03	101	Av. Suecia 3304, Ñuñoa	3		18	1
224	2016-04-28 19:20:17.675927-03	100	Av. Príncipe de Gales 6465, La Reina	3		18	1
225	2016-04-28 19:20:17.687238-03	99	Los Duraznos  95, La Pintana	3		18	1
226	2016-04-28 19:20:17.698178-03	98	Arturo Prat 34, Santiago	3		18	1
227	2016-04-28 19:20:17.70929-03	97	Presidente Lincoln 1615, Recoleta	3		18	1
228	2016-04-28 19:36:39.53908-03	408	Freire 512, Concepción	3		18	1
229	2016-04-28 19:36:39.552617-03	407	Colón 615, Los Ángeles	3		18	1
230	2016-04-28 19:36:39.563738-03	406	1 Sur 1623, Talca	3		18	1
231	2016-04-28 19:36:39.574806-03	405	Arturo Prat 733, Curicó	3		18	1
232	2016-04-28 19:36:39.585992-03	404	Brasil 1082, Rancagua	3		18	1
233	2016-04-28 19:36:39.597062-03	403	Los Carrera 705, La Calera	3		18	1
234	2016-04-28 19:36:39.608128-03	402	Santiago 598, Viña del Mar	3		18	1
235	2016-04-28 19:36:39.619325-03	401	Teatinos 425, Santiago	3		18	1
236	2016-04-28 19:36:39.63048-03	400	Artesanos 657, Recoleta	3		18	1
237	2016-04-28 19:36:39.64152-03	399	Freire 399, San Bernardo	3		18	1
238	2016-04-28 19:36:39.652653-03	398	Av. Santa Rosa  12786, La Pintana	3		18	1
239	2016-04-28 19:36:39.66374-03	397	Serrano 144, Melipilla	3		18	1
240	2016-04-28 19:36:39.674855-03	396	Vicuña Mackena 1918, Peñaflor	3		18	1
241	2016-04-28 19:36:39.686007-03	395	Av. Independencia 3517, Conchalí	3		18	1
242	2016-04-28 19:36:39.697182-03	394	Henriquez 483, Villarrica	3		18	1
243	2016-04-28 19:36:39.708207-03	393	Independencia 267, Ovalle	3		18	1
244	2016-04-28 19:36:39.719282-03	392	Tarapacá 723, Iquique	3		18	1
245	2016-04-28 19:36:39.730433-03	391	Nancy 725, Ñuñoa	3		18	1
246	2016-04-28 19:36:39.741605-03	390	José Miguel de la Barra  460, Santiago	3		18	1
247	2016-04-28 19:36:39.752844-03	389	Av. Suecia 3304, Ñuñoa	3		18	1
248	2016-04-28 19:36:39.763774-03	388	Av. Príncipe de Gales 6465, La Reina	3		18	1
249	2016-04-28 19:36:39.774895-03	387	Los Duraznos  95, La Pintana	3		18	1
250	2016-04-28 19:36:39.785988-03	386	Arturo Prat 34, Santiago	3		18	1
251	2016-04-28 19:36:39.797188-03	385	Presidente Lincoln 1615, Recoleta	3		18	1
252	2016-04-28 19:36:39.808293-03	384	Freire 512, Concepción	3		18	1
253	2016-04-28 19:36:39.819454-03	383	Colón 615, Los Ángeles	3		18	1
254	2016-04-28 19:36:39.830535-03	382	1 Sur 1623, Talca	3		18	1
255	2016-04-28 19:36:39.841644-03	381	Arturo Prat 733, Curicó	3		18	1
256	2016-04-28 19:36:39.852751-03	380	Brasil 1082, Rancagua	3		18	1
257	2016-04-28 19:36:39.863823-03	379	Los Carrera 705, La Calera	3		18	1
258	2016-04-28 19:36:39.87506-03	378	Santiago 598, Viña del Mar	3		18	1
259	2016-04-28 19:36:39.88606-03	377	Teatinos 425, Santiago	3		18	1
260	2016-04-28 19:36:39.897233-03	376	Artesanos 657, Recoleta	3		18	1
261	2016-04-28 19:36:39.908425-03	375	Freire 399, San Bernardo	3		18	1
262	2016-04-28 19:36:39.919374-03	374	Av. Santa Rosa  12786, La Pintana	3		18	1
263	2016-04-28 19:36:39.930535-03	373	Serrano 144, Melipilla	3		18	1
264	2016-04-28 19:36:39.959158-03	372	Vicuña Mackena 1918, Peñaflor	3		18	1
265	2016-04-28 19:36:39.963846-03	371	Av. Independencia 3517, Conchalí	3		18	1
266	2016-04-28 19:36:39.974932-03	370	Henriquez 483, Villarrica	3		18	1
267	2016-04-28 19:36:39.986198-03	369	Independencia 267, Ovalle	3		18	1
268	2016-04-28 19:36:39.997377-03	368	Tarapacá 723, Iquique	3		18	1
269	2016-04-28 19:36:40.008384-03	367	Nancy 725, Ñuñoa	3		18	1
270	2016-04-28 19:36:40.019537-03	366	José Miguel de la Barra  460, Santiago	3		18	1
271	2016-04-28 19:36:40.030636-03	365	Av. Suecia 3304, Ñuñoa	3		18	1
272	2016-04-28 19:36:40.041744-03	364	Av. Príncipe de Gales 6465, La Reina	3		18	1
273	2016-04-28 19:36:40.052829-03	363	Los Duraznos  95, La Pintana	3		18	1
274	2016-04-28 19:36:40.06398-03	362	Arturo Prat 34, Santiago	3		18	1
275	2016-04-28 19:36:40.075146-03	361	Presidente Lincoln 1615, Recoleta	3		18	1
276	2016-04-28 19:36:40.086184-03	360	Freire 512, Concepción	3		18	1
277	2016-04-28 19:36:40.097309-03	359	Colón 615, Los Ángeles	3		18	1
278	2016-04-28 19:36:40.108403-03	358	1 Sur 1623, Talca	3		18	1
279	2016-04-28 19:36:40.119786-03	357	Arturo Prat 733, Curicó	3		18	1
280	2016-04-28 19:36:40.130652-03	356	Brasil 1082, Rancagua	3		18	1
281	2016-04-28 19:36:40.141775-03	355	Los Carrera 705, La Calera	3		18	1
282	2016-04-28 19:36:40.152954-03	354	Santiago 598, Viña del Mar	3		18	1
283	2016-04-28 19:36:40.164057-03	353	Teatinos 425, Santiago	3		18	1
284	2016-04-28 19:36:40.17524-03	352	Artesanos 657, Recoleta	3		18	1
285	2016-04-28 19:36:40.186295-03	351	Freire 399, San Bernardo	3		18	1
286	2016-04-28 19:36:40.197389-03	350	Av. Santa Rosa  12786, La Pintana	3		18	1
287	2016-04-28 19:36:40.208481-03	349	Serrano 144, Melipilla	3		18	1
288	2016-04-28 19:36:40.219552-03	348	Vicuña Mackena 1918, Peñaflor	3		18	1
289	2016-04-28 19:36:40.230758-03	347	Av. Independencia 3517, Conchalí	3		18	1
290	2016-04-28 19:36:40.242326-03	346	Henriquez 483, Villarrica	3		18	1
291	2016-04-28 19:36:40.25289-03	345	Independencia 267, Ovalle	3		18	1
292	2016-04-28 19:36:40.264063-03	344	Tarapacá 723, Iquique	3		18	1
293	2016-04-28 19:36:40.27513-03	343	Nancy 725, Ñuñoa	3		18	1
294	2016-04-28 19:36:40.286237-03	342	José Miguel de la Barra  460, Santiago	3		18	1
295	2016-04-28 19:36:40.297302-03	341	Av. Suecia 3304, Ñuñoa	3		18	1
296	2016-04-28 19:36:40.308522-03	340	Av. Príncipe de Gales 6465, La Reina	3		18	1
297	2016-04-28 19:36:40.319633-03	339	Los Duraznos  95, La Pintana	3		18	1
298	2016-04-28 19:36:40.330811-03	338	Arturo Prat 34, Santiago	3		18	1
299	2016-04-28 19:36:40.341852-03	337	Presidente Lincoln 1615, Recoleta	3		18	1
300	2016-04-28 19:36:40.352989-03	336	Freire 512, Concepción	3		18	1
301	2016-04-28 19:36:40.36419-03	335	Colón 615, Los Ángeles	3		18	1
302	2016-04-28 19:36:40.375234-03	334	1 Sur 1623, Talca	3		18	1
303	2016-04-28 19:36:40.38637-03	333	Arturo Prat 733, Curicó	3		18	1
304	2016-04-28 19:36:40.397379-03	332	Brasil 1082, Rancagua	3		18	1
305	2016-04-28 19:36:40.408534-03	331	Los Carrera 705, La Calera	3		18	1
306	2016-04-28 19:36:40.41965-03	330	Santiago 598, Viña del Mar	3		18	1
307	2016-04-28 19:36:40.430775-03	329	Teatinos 425, Santiago	3		18	1
308	2016-04-28 19:36:40.441958-03	328	Artesanos 657, Recoleta	3		18	1
309	2016-04-28 19:36:40.45298-03	327	Freire 399, San Bernardo	3		18	1
310	2016-04-28 19:36:40.464083-03	326	Av. Santa Rosa  12786, La Pintana	3		18	1
311	2016-04-28 19:36:40.475231-03	325	Serrano 144, Melipilla	3		18	1
312	2016-04-28 19:36:40.486445-03	324	Vicuña Mackena 1918, Peñaflor	3		18	1
313	2016-04-28 19:36:40.497378-03	323	Av. Independencia 3517, Conchalí	3		18	1
314	2016-04-28 19:36:40.508585-03	322	Henriquez 483, Villarrica	3		18	1
315	2016-04-28 19:36:40.519744-03	321	Independencia 267, Ovalle	3		18	1
316	2016-04-28 19:36:40.530887-03	320	Tarapacá 723, Iquique	3		18	1
317	2016-04-28 19:36:40.542022-03	319	Nancy 725, Ñuñoa	3		18	1
318	2016-04-28 19:36:40.553123-03	318	José Miguel de la Barra  460, Santiago	3		18	1
319	2016-04-28 19:36:40.564229-03	317	Av. Suecia 3304, Ñuñoa	3		18	1
320	2016-04-28 19:36:40.575279-03	316	Av. Príncipe de Gales 6465, La Reina	3		18	1
321	2016-04-28 19:36:40.586389-03	315	Los Duraznos  95, La Pintana	3		18	1
322	2016-04-28 19:36:40.597552-03	314	Arturo Prat 34, Santiago	3		18	1
323	2016-04-28 19:36:40.608803-03	313	Presidente Lincoln 1615, Recoleta	3		18	1
324	2016-04-28 19:36:40.61985-03	312	Freire 512, Concepción	3		18	1
325	2016-04-28 19:36:40.630871-03	311	Colón 615, Los Ángeles	3		18	1
326	2016-04-28 19:36:40.642004-03	310	1 Sur 1623, Talca	3		18	1
327	2016-04-28 19:36:40.653154-03	309	Arturo Prat 733, Curicó	3		18	1
328	2016-04-28 19:36:48.891895-03	308	Brasil 1082, Rancagua	3		18	1
329	2016-04-28 19:36:48.909648-03	307	Los Carrera 705, La Calera	3		18	1
330	2016-04-28 19:36:48.920773-03	306	Santiago 598, Viña del Mar	3		18	1
331	2016-04-28 19:36:48.931819-03	305	Teatinos 425, Santiago	3		18	1
332	2016-04-28 19:36:48.943002-03	304	Artesanos 657, Recoleta	3		18	1
333	2016-04-28 19:36:48.954185-03	303	Freire 399, San Bernardo	3		18	1
334	2016-04-28 19:36:48.965218-03	302	Av. Santa Rosa  12786, La Pintana	3		18	1
335	2016-04-28 19:36:48.976271-03	301	Serrano 144, Melipilla	3		18	1
336	2016-04-28 19:36:48.987392-03	300	Vicuña Mackena 1918, Peñaflor	3		18	1
337	2016-04-28 19:36:48.998458-03	299	Av. Independencia 3517, Conchalí	3		18	1
338	2016-04-28 19:36:49.009628-03	298	Henriquez 483, Villarrica	3		18	1
339	2016-04-28 19:36:49.020883-03	297	Independencia 267, Ovalle	3		18	1
340	2016-04-28 19:36:49.031804-03	296	Tarapacá 723, Iquique	3		18	1
341	2016-04-28 19:36:49.043105-03	295	Nancy 725, Ñuñoa	3		18	1
342	2016-04-28 19:36:49.054325-03	294	José Miguel de la Barra  460, Santiago	3		18	1
343	2016-04-28 19:36:49.065305-03	293	Av. Suecia 3304, Ñuñoa	3		18	1
344	2016-04-28 19:36:49.076391-03	292	Av. Príncipe de Gales 6465, La Reina	3		18	1
345	2016-04-28 19:36:49.087451-03	291	Los Duraznos  95, La Pintana	3		18	1
346	2016-04-28 19:36:49.098562-03	290	Arturo Prat 34, Santiago	3		18	1
347	2016-04-28 19:36:49.109668-03	289	Presidente Lincoln 1615, Recoleta	3		18	1
348	2016-04-28 19:36:49.120806-03	288	Freire 512, Concepción	3		18	1
349	2016-04-28 19:36:49.137077-03	287	Colón 615, Los Ángeles	3		18	1
350	2016-04-28 19:36:49.157117-03	286	1 Sur 1623, Talca	3		18	1
351	2016-04-28 19:36:49.165267-03	285	Arturo Prat 733, Curicó	3		18	1
352	2016-04-28 19:36:49.176382-03	284	Brasil 1082, Rancagua	3		18	1
353	2016-04-28 19:36:49.187511-03	283	Los Carrera 705, La Calera	3		18	1
354	2016-04-28 19:36:49.198665-03	282	Santiago 598, Viña del Mar	3		18	1
355	2016-04-28 19:36:49.209944-03	281	Teatinos 425, Santiago	3		18	1
356	2016-04-28 19:36:49.22086-03	280	Artesanos 657, Recoleta	3		18	1
357	2016-04-28 19:36:49.23202-03	279	Freire 399, San Bernardo	3		18	1
358	2016-04-28 19:36:49.243146-03	278	Av. Santa Rosa  12786, La Pintana	3		18	1
359	2016-04-28 19:36:49.254384-03	277	Serrano 144, Melipilla	3		18	1
360	2016-04-28 19:36:49.26584-03	276	Vicuña Mackena 1918, Peñaflor	3		18	1
361	2016-04-28 19:36:49.276452-03	275	Av. Independencia 3517, Conchalí	3		18	1
362	2016-04-28 19:36:49.287596-03	274	Henriquez 483, Villarrica	3		18	1
363	2016-04-28 19:36:49.298653-03	273	Independencia 267, Ovalle	3		18	1
364	2016-04-28 19:36:49.309814-03	272	Tarapacá 723, Iquique	3		18	1
365	2016-04-28 19:36:49.320869-03	271	Nancy 725, Ñuñoa	3		18	1
366	2016-04-28 19:36:49.332015-03	270	José Miguel de la Barra  460, Santiago	3		18	1
367	2016-04-28 19:36:49.343125-03	269	Av. Suecia 3304, Ñuñoa	3		18	1
368	2016-04-28 19:36:49.354229-03	268	Av. Príncipe de Gales 6465, La Reina	3		18	1
369	2016-04-28 19:36:49.365339-03	267	Los Duraznos  95, La Pintana	3		18	1
370	2016-04-28 19:36:49.376435-03	266	Arturo Prat 34, Santiago	3		18	1
371	2016-04-28 19:36:49.387542-03	265	Presidente Lincoln 1615, Recoleta	3		18	1
372	2016-04-28 19:36:49.398731-03	264	Freire 512, Concepción	3		18	1
373	2016-04-28 19:36:49.409875-03	263	Colón 615, Los Ángeles	3		18	1
374	2016-04-28 19:36:49.420948-03	262	1 Sur 1623, Talca	3		18	1
375	2016-04-28 19:36:49.432079-03	261	Arturo Prat 733, Curicó	3		18	1
376	2016-04-28 19:36:49.443194-03	260	Brasil 1082, Rancagua	3		18	1
377	2016-04-28 19:36:49.454318-03	259	Los Carrera 705, La Calera	3		18	1
378	2016-04-28 19:36:49.465384-03	258	Santiago 598, Viña del Mar	3		18	1
379	2016-04-28 19:36:49.476518-03	257	Teatinos 425, Santiago	3		18	1
380	2016-04-28 19:36:49.487712-03	256	Artesanos 657, Recoleta	3		18	1
381	2016-04-28 19:36:49.498709-03	255	Freire 399, San Bernardo	3		18	1
382	2016-04-28 19:36:49.509857-03	254	Av. Santa Rosa  12786, La Pintana	3		18	1
383	2016-04-28 19:36:49.520966-03	253	Serrano 144, Melipilla	3		18	1
384	2016-04-28 19:36:49.532036-03	252	Vicuña Mackena 1918, Peñaflor	3		18	1
385	2016-04-28 19:36:49.543208-03	251	Av. Independencia 3517, Conchalí	3		18	1
386	2016-04-28 19:36:49.554405-03	250	Henriquez 483, Villarrica	3		18	1
387	2016-04-28 19:36:49.565477-03	249	Independencia 267, Ovalle	3		18	1
388	2016-04-28 19:36:49.576695-03	248	Tarapacá 723, Iquique	3		18	1
389	2016-04-28 19:36:49.587693-03	247	Nancy 725, Ñuñoa	3		18	1
390	2016-04-28 19:36:49.598832-03	246	José Miguel de la Barra  460, Santiago	3		18	1
391	2016-04-28 19:36:49.609932-03	245	Av. Suecia 3304, Ñuñoa	3		18	1
392	2016-04-28 19:36:49.621071-03	244	Av. Príncipe de Gales 6465, La Reina	3		18	1
393	2016-04-28 19:36:49.63224-03	243	Los Duraznos  95, La Pintana	3		18	1
394	2016-04-28 19:36:49.643322-03	242	Arturo Prat 34, Santiago	3		18	1
395	2016-04-28 19:36:49.654416-03	241	Presidente Lincoln 1615, Recoleta	3		18	1
396	2016-04-28 19:36:49.665511-03	240	Freire 512, Concepción	3		18	1
397	2016-04-28 19:36:49.676644-03	239	Colón 615, Los Ángeles	3		18	1
398	2016-04-28 19:36:49.687788-03	238	1 Sur 1623, Talca	3		18	1
399	2016-04-28 19:36:49.698844-03	237	Arturo Prat 733, Curicó	3		18	1
400	2016-04-28 19:36:49.709987-03	236	Brasil 1082, Rancagua	3		18	1
401	2016-04-28 19:36:49.721069-03	235	Los Carrera 705, La Calera	3		18	1
402	2016-04-28 19:36:49.732164-03	234	Santiago 598, Viña del Mar	3		18	1
403	2016-04-28 19:36:49.743377-03	233	Teatinos 425, Santiago	3		18	1
404	2016-04-28 19:36:49.754468-03	232	Artesanos 657, Recoleta	3		18	1
405	2016-04-28 19:36:49.765976-03	231	Freire 399, San Bernardo	3		18	1
406	2016-04-28 19:36:49.776688-03	230	Av. Santa Rosa  12786, La Pintana	3		18	1
407	2016-04-28 19:36:49.787817-03	229	Serrano 144, Melipilla	3		18	1
408	2016-04-28 19:36:49.798959-03	228	Vicuña Mackena 1918, Peñaflor	3		18	1
409	2016-04-28 19:36:49.810053-03	227	Av. Independencia 3517, Conchalí	3		18	1
410	2016-04-28 19:36:49.821194-03	226	Henriquez 483, Villarrica	3		18	1
411	2016-04-28 19:36:49.832284-03	225	Independencia 267, Ovalle	3		18	1
412	2016-04-28 19:36:49.843397-03	224	Tarapacá 723, Iquique	3		18	1
413	2016-04-28 19:36:49.854544-03	223	Nancy 725, Ñuñoa	3		18	1
414	2016-04-28 19:36:49.86557-03	222	José Miguel de la Barra  460, Santiago	3		18	1
415	2016-04-28 19:36:49.877032-03	221	Av. Suecia 3304, Ñuñoa	3		18	1
416	2016-04-28 19:36:49.887862-03	220	Av. Príncipe de Gales 6465, La Reina	3		18	1
417	2016-04-28 19:36:49.899006-03	219	Los Duraznos  95, La Pintana	3		18	1
418	2016-04-28 19:36:49.91017-03	218	Arturo Prat 34, Santiago	3		18	1
419	2016-04-28 19:36:49.92127-03	217	Presidente Lincoln 1615, Recoleta	3		18	1
420	2016-04-28 19:36:49.93243-03	216	Freire 512, Concepción	3		18	1
421	2016-04-28 19:36:49.94346-03	215	Colón 615, Los Ángeles	3		18	1
422	2016-04-28 19:36:49.954564-03	214	1 Sur 1623, Talca	3		18	1
423	2016-04-28 19:36:49.965821-03	213	Arturo Prat 733, Curicó	3		18	1
424	2016-04-28 19:36:49.976791-03	212	Brasil 1082, Rancagua	3		18	1
425	2016-04-28 19:36:49.987896-03	211	Los Carrera 705, La Calera	3		18	1
426	2016-04-28 19:36:49.999193-03	210	Santiago 598, Viña del Mar	3		18	1
427	2016-04-28 19:36:50.010075-03	209	Teatinos 425, Santiago	3		18	1
428	2016-04-28 19:36:57.976829-03	208	Artesanos 657, Recoleta	3		18	1
429	2016-04-28 19:36:57.988286-03	207	Freire 399, San Bernardo	3		18	1
430	2016-04-28 19:36:57.999442-03	206	Av. Santa Rosa  12786, La Pintana	3		18	1
431	2016-04-28 19:36:58.010573-03	205	Serrano 144, Melipilla	3		18	1
432	2016-04-28 19:36:58.021756-03	204	Vicuña Mackena 1918, Peñaflor	3		18	1
433	2016-04-28 19:36:58.032831-03	203	Av. Independencia 3517, Conchalí	3		18	1
434	2016-04-28 19:36:58.044189-03	202	Henriquez 483, Villarrica	3		18	1
435	2016-04-28 19:36:58.055082-03	201	Independencia 267, Ovalle	3		18	1
436	2016-04-28 19:36:58.066167-03	200	Tarapacá 723, Iquique	3		18	1
437	2016-04-28 19:36:58.077238-03	199	Nancy 725, Ñuñoa	3		18	1
438	2016-04-28 19:36:58.088345-03	198	José Miguel de la Barra  460, Santiago	3		18	1
439	2016-04-28 19:36:58.099481-03	197	Av. Suecia 3304, Ñuñoa	3		18	1
440	2016-04-28 19:36:58.110633-03	196	Av. Príncipe de Gales 6465, La Reina	3		18	1
441	2016-04-28 19:36:58.12172-03	195	Los Duraznos  95, La Pintana	3		18	1
442	2016-04-28 19:36:58.132795-03	194	Arturo Prat 34, Santiago	3		18	1
443	2016-04-28 19:36:58.143952-03	193	Presidente Lincoln 1615, Recoleta	3		18	1
444	2016-04-28 19:36:58.15503-03	192	Freire 512, Concepción	3		18	1
445	2016-04-28 19:36:58.16617-03	191	Colón 615, Los Ángeles	3		18	1
446	2016-04-28 19:36:58.177311-03	190	1 Sur 1623, Talca	3		18	1
447	2016-04-28 19:36:58.188337-03	189	Arturo Prat 733, Curicó	3		18	1
448	2016-04-28 19:36:58.199548-03	188	Brasil 1082, Rancagua	3		18	1
449	2016-04-28 19:36:58.210656-03	187	Los Carrera 705, La Calera	3		18	1
450	2016-04-28 19:36:58.221877-03	186	Santiago 598, Viña del Mar	3		18	1
451	2016-04-28 19:36:58.232923-03	185	Teatinos 425, Santiago	3		18	1
452	2016-04-28 19:36:58.243943-03	184	Artesanos 657, Recoleta	3		18	1
453	2016-04-28 19:36:58.255181-03	183	Freire 399, San Bernardo	3		18	1
454	2016-04-28 19:36:58.266247-03	182	Av. Santa Rosa  12786, La Pintana	3		18	1
455	2016-04-28 19:36:58.277365-03	181	Serrano 144, Melipilla	3		18	1
456	2016-04-28 19:36:58.288442-03	180	Vicuña Mackena 1918, Peñaflor	3		18	1
457	2016-04-28 19:36:58.299623-03	179	Av. Independencia 3517, Conchalí	3		18	1
458	2016-04-28 19:36:58.310882-03	178	Henriquez 483, Villarrica	3		18	1
459	2016-04-28 19:36:58.321847-03	177	Independencia 267, Ovalle	3		18	1
460	2016-04-28 19:36:58.340664-03	176	Tarapacá 723, Iquique	3		18	1
461	2016-04-28 19:36:58.344075-03	175	Nancy 725, Ñuñoa	3		18	1
462	2016-04-28 19:36:58.355121-03	174	José Miguel de la Barra  460, Santiago	3		18	1
463	2016-04-28 19:36:58.366349-03	173	Av. Suecia 3304, Ñuñoa	3		18	1
464	2016-04-28 19:36:58.377492-03	172	Av. Príncipe de Gales 6465, La Reina	3		18	1
465	2016-04-28 19:36:58.388486-03	171	Los Duraznos  95, La Pintana	3		18	1
466	2016-04-28 19:36:58.39963-03	170	Arturo Prat 34, Santiago	3		18	1
467	2016-04-28 19:36:58.41073-03	169	Presidente Lincoln 1615, Recoleta	3		18	1
468	2016-04-28 19:36:58.421933-03	168	Freire 512, Concepción	3		18	1
469	2016-04-28 19:36:58.433074-03	167	Colón 615, Los Ángeles	3		18	1
470	2016-04-28 19:36:58.444138-03	166	1 Sur 1623, Talca	3		18	1
471	2016-04-28 19:36:58.455306-03	165	Arturo Prat 733, Curicó	3		18	1
472	2016-04-28 19:36:58.466409-03	164	Brasil 1082, Rancagua	3		18	1
473	2016-04-28 19:36:58.477513-03	163	Los Carrera 705, La Calera	3		18	1
474	2016-04-28 19:36:58.488495-03	162	Santiago 598, Viña del Mar	3		18	1
475	2016-04-28 19:36:58.499726-03	161	Teatinos 425, Santiago	3		18	1
476	2016-04-28 19:36:58.510878-03	160	Artesanos 657, Recoleta	3		18	1
477	2016-04-28 19:36:58.52192-03	159	Freire 399, San Bernardo	3		18	1
478	2016-04-28 19:36:58.533018-03	158	Av. Santa Rosa  12786, La Pintana	3		18	1
479	2016-04-28 19:36:58.544132-03	157	Serrano 144, Melipilla	3		18	1
480	2016-04-28 19:36:58.555243-03	156	Vicuña Mackena 1918, Peñaflor	3		18	1
481	2016-04-28 19:36:58.56649-03	155	Av. Independencia 3517, Conchalí	3		18	1
482	2016-04-28 19:36:58.577482-03	154	Henriquez 483, Villarrica	3		18	1
483	2016-04-28 19:36:58.588581-03	153	Independencia 267, Ovalle	3		18	1
484	2016-04-28 19:36:58.599692-03	152	Tarapacá 723, Iquique	3		18	1
485	2016-04-28 19:36:58.610787-03	151	Nancy 725, Ñuñoa	3		18	1
486	2016-04-28 19:36:58.622063-03	150	José Miguel de la Barra  460, Santiago	3		18	1
487	2016-04-28 19:36:58.633077-03	149	Av. Suecia 3304, Ñuñoa	3		18	1
488	2016-04-28 19:36:58.645932-03	148	Av. Príncipe de Gales 6465, La Reina	3		18	1
489	2016-04-28 19:36:58.657044-03	147	Los Duraznos  95, La Pintana	3		18	1
490	2016-04-28 19:36:58.668217-03	146	Arturo Prat 34, Santiago	3		18	1
491	2016-04-28 19:36:58.679272-03	145	Presidente Lincoln 1615, Recoleta	3		18	1
492	2016-04-28 19:36:58.690433-03	144	Freire 512, Concepción	3		18	1
493	2016-04-28 19:36:58.701436-03	143	Colón 615, Los Ángeles	3		18	1
494	2016-04-28 19:36:58.712606-03	142	1 Sur 1623, Talca	3		18	1
495	2016-04-28 19:36:58.723708-03	141	Arturo Prat 733, Curicó	3		18	1
496	2016-04-28 19:36:58.734758-03	140	Brasil 1082, Rancagua	3		18	1
497	2016-04-28 19:36:58.745948-03	139	Los Carrera 705, La Calera	3		18	1
498	2016-04-28 19:36:58.757075-03	138	Santiago 598, Viña del Mar	3		18	1
499	2016-04-28 19:36:58.768277-03	137	Teatinos 425, Santiago	3		18	1
500	2016-04-28 19:36:58.779396-03	136	Artesanos 657, Recoleta	3		18	1
501	2016-04-28 19:36:58.790458-03	135	Freire 399, San Bernardo	3		18	1
502	2016-04-28 19:36:58.802037-03	134	Av. Santa Rosa  12786, La Pintana	3		18	1
503	2016-04-28 19:36:58.812614-03	133	Serrano 144, Melipilla	3		18	1
504	2016-04-28 19:36:58.82382-03	132	Vicuña Mackena 1918, Peñaflor	3		18	1
505	2016-04-28 19:36:58.834938-03	131	Av. Independencia 3517, Conchalí	3		18	1
506	2016-04-28 19:36:58.846025-03	130	Henriquez 483, Villarrica	3		18	1
507	2016-04-28 19:36:58.857209-03	129	Independencia 267, Ovalle	3		18	1
508	2016-04-28 19:36:58.869405-03	128	Tarapacá 723, Iquique	3		18	1
509	2016-04-28 19:36:58.879389-03	127	Nancy 725, Ñuñoa	3		18	1
510	2016-04-28 19:36:58.890452-03	126	José Miguel de la Barra  460, Santiago	3		18	1
511	2016-04-28 19:36:58.901562-03	125	Av. Suecia 3304, Ñuñoa	3		18	1
512	2016-04-28 19:36:58.912695-03	124	Av. Príncipe de Gales 6465, La Reina	3		18	1
513	2016-04-28 19:36:58.923926-03	123	Los Duraznos  95, La Pintana	3		18	1
514	2016-04-28 19:36:58.935018-03	122	Arturo Prat 34, Santiago	3		18	1
515	2016-04-28 19:36:58.94599-03	121	Presidente Lincoln 1615, Recoleta	3		18	1
516	2016-04-28 19:37:25.952604-03	130	Esteban Figueroa	3		20	1
517	2016-04-28 19:37:25.974715-03	123	Esteban Figueroa	3		20	1
518	2016-04-28 19:37:25.996993-03	116	Esteban Figueroa	3		20	1
519	2016-04-28 19:37:26.003026-03	109	Esteban Figueroa	3		20	1
520	2016-04-28 19:37:26.014151-03	102	Esteban Figueroa	3		20	1
521	2016-04-28 19:37:26.025336-03	95	Esteban Figueroa	3		20	1
522	2016-04-28 19:37:26.036421-03	88	Esteban Figueroa	3		20	1
523	2016-04-28 19:37:26.047499-03	81	Esteban Figueroa	3		20	1
524	2016-04-28 19:37:26.058637-03	74	Esteban Figueroa	3		20	1
525	2016-04-28 19:37:26.069685-03	67	Esteban Figueroa	3		20	1
526	2016-04-28 19:37:26.080854-03	60	Esteban Figueroa	3		20	1
527	2016-04-28 19:37:26.091922-03	53	Esteban Figueroa	3		20	1
528	2016-04-28 19:37:26.103023-03	132	José Dono	3		20	1
529	2016-04-28 19:37:26.114272-03	125	José Dono	3		20	1
530	2016-04-28 19:37:26.125348-03	118	José Dono	3		20	1
531	2016-04-28 19:37:26.158629-03	111	José Dono	3		20	1
532	2016-04-28 19:37:26.192118-03	104	José Dono	3		20	1
533	2016-04-28 19:37:26.203081-03	97	José Dono	3		20	1
534	2016-04-28 19:37:26.214314-03	90	José Dono	3		20	1
535	2016-04-28 19:37:26.225277-03	83	José Dono	3		20	1
536	2016-04-28 19:37:26.236581-03	76	José Dono	3		20	1
537	2016-04-28 19:37:26.247622-03	69	José Dono	3		20	1
538	2016-04-28 19:37:26.258673-03	62	José Dono	3		20	1
539	2016-04-28 19:37:26.26975-03	55	José Dono	3		20	1
540	2016-04-28 19:37:26.280971-03	127	Juan Estoril	3		20	1
541	2016-04-28 19:37:26.292077-03	120	Juan Estoril	3		20	1
542	2016-04-28 19:37:26.303165-03	113	Juan Estoril	3		20	1
543	2016-04-28 19:37:26.314338-03	106	Juan Estoril	3		20	1
544	2016-04-28 19:37:26.325366-03	99	Juan Estoril	3		20	1
545	2016-04-28 19:37:26.336361-03	92	Juan Estoril	3		20	1
546	2016-04-28 19:37:26.347574-03	85	Juan Estoril	3		20	1
547	2016-04-28 19:37:26.358758-03	78	Juan Estoril	3		20	1
548	2016-04-28 19:37:26.369995-03	71	Juan Estoril	3		20	1
549	2016-04-28 19:37:26.381117-03	64	Juan Estoril	3		20	1
550	2016-04-28 19:37:26.392145-03	57	Juan Estoril	3		20	1
551	2016-04-28 19:37:26.40333-03	50	Juan Estoril	3		20	1
552	2016-04-28 19:37:26.414499-03	129	Lina Dona	3		20	1
553	2016-04-28 19:37:26.425477-03	122	Lina Dona	3		20	1
554	2016-04-28 19:37:26.436573-03	115	Lina Dona	3		20	1
555	2016-04-28 19:37:26.44761-03	108	Lina Dona	3		20	1
556	2016-04-28 19:37:26.458776-03	101	Lina Dona	3		20	1
557	2016-04-28 19:37:26.469936-03	94	Lina Dona	3		20	1
558	2016-04-28 19:37:26.48097-03	87	Lina Dona	3		20	1
559	2016-04-28 19:37:26.492138-03	80	Lina Dona	3		20	1
560	2016-04-28 19:37:26.503261-03	73	Lina Dona	3		20	1
561	2016-04-28 19:37:26.514409-03	66	Lina Dona	3		20	1
562	2016-04-28 19:37:26.525504-03	59	Lina Dona	3		20	1
563	2016-04-28 19:37:26.536568-03	52	Lina Dona	3		20	1
564	2016-04-28 19:37:26.547642-03	133	Luna Dominguez	3		20	1
565	2016-04-28 19:37:26.558909-03	126	Luna Dominguez	3		20	1
566	2016-04-28 19:37:26.569977-03	119	Luna Dominguez	3		20	1
567	2016-04-28 19:37:26.581085-03	112	Luna Dominguez	3		20	1
568	2016-04-28 19:37:26.592196-03	105	Luna Dominguez	3		20	1
569	2016-04-28 19:37:26.603335-03	98	Luna Dominguez	3		20	1
570	2016-04-28 19:37:26.614467-03	91	Luna Dominguez	3		20	1
571	2016-04-28 19:37:26.625481-03	84	Luna Dominguez	3		20	1
572	2016-04-28 19:37:26.636648-03	77	Luna Dominguez	3		20	1
573	2016-04-28 19:37:26.647765-03	70	Luna Dominguez	3		20	1
574	2016-04-28 19:37:26.658906-03	63	Luna Dominguez	3		20	1
575	2016-04-28 19:37:26.670022-03	56	Luna Dominguez	3		20	1
576	2016-04-28 19:37:26.681099-03	128	Marina Estevez	3		20	1
577	2016-04-28 19:37:26.692234-03	121	Marina Estevez	3		20	1
578	2016-04-28 19:37:26.703405-03	114	Marina Estevez	3		20	1
579	2016-04-28 19:37:26.714536-03	107	Marina Estevez	3		20	1
580	2016-04-28 19:37:26.725641-03	100	Marina Estevez	3		20	1
581	2016-04-28 19:37:26.736697-03	93	Marina Estevez	3		20	1
582	2016-04-28 19:37:26.747802-03	86	Marina Estevez	3		20	1
583	2016-04-28 19:37:26.758925-03	79	Marina Estevez	3		20	1
584	2016-04-28 19:37:26.770079-03	72	Marina Estevez	3		20	1
585	2016-04-28 19:37:26.781149-03	65	Marina Estevez	3		20	1
586	2016-04-28 19:37:26.79233-03	58	Marina Estevez	3		20	1
587	2016-04-28 19:37:26.803404-03	51	Marina Estevez	3		20	1
588	2016-04-28 19:37:26.814547-03	131	Martin Cárcamo	3		20	1
589	2016-04-28 19:37:26.825683-03	124	Martin Cárcamo	3		20	1
590	2016-04-28 19:37:26.836769-03	117	Martin Cárcamo	3		20	1
591	2016-04-28 19:37:26.84784-03	110	Martin Cárcamo	3		20	1
592	2016-04-28 19:37:26.858939-03	103	Martin Cárcamo	3		20	1
593	2016-04-28 19:37:26.870056-03	96	Martin Cárcamo	3		20	1
594	2016-04-28 19:37:26.881305-03	89	Martin Cárcamo	3		20	1
595	2016-04-28 19:37:26.892352-03	82	Martin Cárcamo	3		20	1
596	2016-04-28 19:37:26.903617-03	75	Martin Cárcamo	3		20	1
597	2016-04-28 19:37:26.91464-03	68	Martin Cárcamo	3		20	1
598	2016-04-28 19:37:26.925922-03	61	Martin Cárcamo	3		20	1
599	2016-04-28 19:37:26.936869-03	54	Martin Cárcamo	3		20	1
600	2016-04-28 19:37:52.683609-03	109	Daniel Ortiz	3		19	1
601	2016-04-28 19:37:52.703879-03	104	Daniel Ortiz	3		19	1
602	2016-04-28 19:37:52.714966-03	99	Daniel Ortiz	3		19	1
603	2016-04-28 19:37:52.726122-03	94	Daniel Ortiz	3		19	1
604	2016-04-28 19:37:52.737265-03	89	Daniel Ortiz	3		19	1
605	2016-04-28 19:37:52.748305-03	84	Daniel Ortiz	3		19	1
606	2016-04-28 19:37:52.759458-03	79	Daniel Ortiz	3		19	1
607	2016-04-28 19:37:52.770687-03	74	Daniel Ortiz	3		19	1
608	2016-04-28 19:37:52.781696-03	69	Daniel Ortiz	3		19	1
609	2016-04-28 19:37:52.792762-03	64	Daniel Ortiz	3		19	1
610	2016-04-28 19:37:52.803822-03	59	Daniel Ortiz	3		19	1
611	2016-04-28 19:37:52.81497-03	54	Daniel Ortiz	3		19	1
612	2016-04-28 19:37:52.826116-03	112	Francisca Olzman	3		19	1
613	2016-04-28 19:37:52.837253-03	107	Francisca Olzman	3		19	1
614	2016-04-28 19:37:52.848367-03	102	Francisca Olzman	3		19	1
615	2016-04-28 19:37:52.859399-03	97	Francisca Olzman	3		19	1
616	2016-04-28 19:37:52.870512-03	92	Francisca Olzman	3		19	1
617	2016-04-28 19:37:52.881697-03	87	Francisca Olzman	3		19	1
618	2016-04-28 19:37:52.893063-03	82	Francisca Olzman	3		19	1
619	2016-04-28 19:37:52.903998-03	77	Francisca Olzman	3		19	1
620	2016-04-28 19:37:52.915138-03	72	Francisca Olzman	3		19	1
621	2016-04-28 19:37:52.926214-03	67	Francisca Olzman	3		19	1
622	2016-04-28 19:37:52.937406-03	62	Francisca Olzman	3		19	1
623	2016-04-28 19:37:52.948406-03	57	Francisca Olzman	3		19	1
624	2016-04-28 19:37:52.972664-03	110	Ismael Vergara	3		19	1
625	2016-04-28 19:37:52.9817-03	105	Ismael Vergara	3		19	1
626	2016-04-28 19:37:52.992838-03	100	Ismael Vergara	3		19	1
627	2016-04-28 19:37:53.003944-03	95	Ismael Vergara	3		19	1
628	2016-04-28 19:37:53.015173-03	90	Ismael Vergara	3		19	1
629	2016-04-28 19:37:53.026209-03	85	Ismael Vergara	3		19	1
630	2016-04-28 19:37:53.037467-03	80	Ismael Vergara	3		19	1
631	2016-04-28 19:37:53.048413-03	75	Ismael Vergara	3		19	1
632	2016-04-28 19:37:53.059525-03	70	Ismael Vergara	3		19	1
633	2016-04-28 19:37:53.070648-03	65	Ismael Vergara	3		19	1
634	2016-04-28 19:37:53.081843-03	60	Ismael Vergara	3		19	1
635	2016-04-28 19:37:53.092927-03	55	Ismael Vergara	3		19	1
636	2016-04-28 19:37:53.10402-03	108	José Martinez	3		19	1
637	2016-04-28 19:37:53.115188-03	103	José Martinez	3		19	1
638	2016-04-28 19:37:53.12629-03	98	José Martinez	3		19	1
639	2016-04-28 19:37:53.137436-03	93	José Martinez	3		19	1
640	2016-04-28 19:37:53.148512-03	88	José Martinez	3		19	1
641	2016-04-28 19:37:53.159581-03	83	José Martinez	3		19	1
642	2016-04-28 19:37:53.170715-03	78	José Martinez	3		19	1
643	2016-04-28 19:37:53.181773-03	73	José Martinez	3		19	1
644	2016-04-28 19:37:53.192895-03	68	José Martinez	3		19	1
645	2016-04-28 19:37:53.204066-03	63	José Martinez	3		19	1
646	2016-04-28 19:37:53.215135-03	58	José Martinez	3		19	1
647	2016-04-28 19:37:53.226252-03	53	José Martinez	3		19	1
648	2016-04-28 19:37:53.238188-03	111	Pedro Western	3		19	1
649	2016-04-28 19:37:53.248484-03	106	Pedro Western	3		19	1
650	2016-04-28 19:37:53.259661-03	101	Pedro Western	3		19	1
651	2016-04-28 19:37:53.270821-03	96	Pedro Western	3		19	1
652	2016-04-28 19:37:53.281935-03	91	Pedro Western	3		19	1
653	2016-04-28 19:37:53.293037-03	86	Pedro Western	3		19	1
654	2016-04-28 19:37:53.304123-03	81	Pedro Western	3		19	1
655	2016-04-28 19:37:53.315333-03	76	Pedro Western	3		19	1
656	2016-04-28 19:37:53.326355-03	71	Pedro Western	3		19	1
657	2016-04-28 19:37:53.337975-03	66	Pedro Western	3		19	1
658	2016-04-28 19:37:53.34859-03	61	Pedro Western	3		19	1
659	2016-04-28 19:37:53.359726-03	56	Pedro Western	3		19	1
660	2016-04-28 19:53:03.861313-03	260	Agrícola BUENA UBICACION	2	Changed descripcion. Changed moneda for Precio Propiedad "Unidad de Fomento 611.0".	21	1
661	2016-04-28 20:04:03.953447-03	137	Esteban Figueroa	3		20	1
662	2016-04-28 20:04:03.989472-03	139	José Dono	3		20	1
663	2016-04-28 20:04:04.000325-03	134	Juan Estoril	3		20	1
664	2016-04-28 20:04:04.011406-03	136	Lina Dona	3		20	1
665	2016-04-28 20:04:04.022567-03	140	Luna Dominguez	3		20	1
666	2016-04-28 20:04:04.033678-03	135	Marina Estevez	3		20	1
667	2016-04-28 20:04:04.044746-03	138	Martin Cárcamo	3		20	1
668	2016-04-28 20:04:15.708773-03	308	Agrícola BUENA UBICACION	3		21	1
669	2016-04-28 20:04:15.736321-03	295	Agrícola BUENA UBICACION	3		21	1
670	2016-04-28 20:04:15.745302-03	284	Agrícola BUENA UBICACION	3		21	1
671	2016-04-28 20:04:15.756439-03	271	Agrícola BUENA UBICACION	3		21	1
672	2016-04-28 20:04:15.767552-03	303	Bodega BUENA UBICACION	3		21	1
673	2016-04-28 20:04:15.778622-03	279	Bodega BUENA UBICACION	3		21	1
674	2016-04-28 20:04:15.790294-03	311	Bodega BUENA UBICACION	3		21	1
675	2016-04-28 20:04:15.800796-03	301	Bodega BUENA UBICACION	3		21	1
676	2016-04-28 20:04:15.812018-03	300	Bodega BUENA UBICACION	3		21	1
677	2016-04-28 20:04:15.823036-03	287	Bodega BUENA UBICACION	3		21	1
678	2016-04-28 20:04:15.834214-03	277	Bodega BUENA UBICACION	3		21	1
679	2016-04-28 20:04:15.845408-03	276	Bodega BUENA UBICACION	3		21	1
680	2016-04-28 20:04:15.856496-03	299	Casa BUENA UBICACION	3		21	1
681	2016-04-28 20:04:15.867631-03	298	Casa BUENA UBICACION	3		21	1
682	2016-04-28 20:04:15.878685-03	294	Casa BUENA UBICACION	3		21	1
683	2016-04-28 20:04:15.889824-03	275	Casa BUENA UBICACION	3		21	1
684	2016-04-28 20:04:15.900933-03	274	Casa BUENA UBICACION	3		21	1
685	2016-04-28 20:04:15.912053-03	270	Casa BUENA UBICACION	3		21	1
686	2016-04-28 20:04:15.923135-03	305	Casa BUENA UBICACION	3		21	1
687	2016-04-28 20:04:15.934275-03	296	Casa BUENA UBICACION	3		21	1
688	2016-04-28 20:04:15.945354-03	293	Casa BUENA UBICACION	3		21	1
689	2016-04-28 20:04:15.956534-03	281	Casa BUENA UBICACION	3		21	1
690	2016-04-28 20:04:15.967702-03	272	Casa BUENA UBICACION	3		21	1
691	2016-04-28 20:04:15.978736-03	269	Casa BUENA UBICACION	3		21	1
692	2016-04-28 20:04:15.989768-03	310	Comercial BUENA UBICACION	3		21	1
693	2016-04-28 20:04:16.000883-03	286	Comercial BUENA UBICACION	3		21	1
694	2016-04-28 20:04:16.012104-03	291	Departamento BUENA UBICACION	3		21	1
695	2016-04-28 20:04:16.023219-03	289	Departamento BUENA UBICACION	3		21	1
696	2016-04-28 20:04:16.034368-03	267	Departamento BUENA UBICACION	3		21	1
697	2016-04-28 20:04:16.045496-03	265	Departamento BUENA UBICACION	3		21	1
698	2016-04-28 20:04:16.056549-03	312	Loteo BUENA UBICACION	3		21	1
699	2016-04-28 20:04:16.067706-03	288	Loteo BUENA UBICACION	3		21	1
700	2016-04-28 20:04:16.078766-03	306	Parcela BUENA UBICACION	3		21	1
701	2016-04-28 20:04:16.089963-03	292	Parcela BUENA UBICACION	3		21	1
702	2016-04-28 20:04:16.101065-03	282	Parcela BUENA UBICACION	3		21	1
703	2016-04-28 20:04:16.112137-03	268	Parcela BUENA UBICACION	3		21	1
704	2016-04-28 20:04:16.123195-03	309	Parcela BUENA UBICACION	3		21	1
705	2016-04-28 20:04:16.13434-03	302	Parcela BUENA UBICACION	3		21	1
706	2016-04-28 20:04:16.150524-03	285	Parcela BUENA UBICACION	3		21	1
707	2016-04-28 20:04:16.167766-03	278	Parcela BUENA UBICACION	3		21	1
708	2016-04-28 20:04:16.17888-03	304	Sitio BUENA UBICACION	3		21	1
709	2016-04-28 20:04:16.189923-03	280	Sitio BUENA UBICACION	3		21	1
710	2016-04-28 20:04:16.201065-03	290	Terreno en Construcción BUENA UBICACION	3		21	1
711	2016-04-28 20:04:16.21221-03	266	Terreno en Construcción BUENA UBICACION	3		21	1
712	2016-04-28 20:04:16.223259-03	307	Terreno en Construcción BUENA UBICACION	3		21	1
713	2016-04-28 20:04:16.234403-03	297	Terreno en Construcción BUENA UBICACION	3		21	1
714	2016-04-28 20:04:16.245548-03	283	Terreno en Construcción BUENA UBICACION	3		21	1
715	2016-04-28 20:04:16.265937-03	273	Terreno en Construcción BUENA UBICACION	3		21	1
716	2016-04-28 20:04:28.526443-03	124	Daniel Ortiz	3		19	1
717	2016-04-28 20:04:28.553297-03	119	Daniel Ortiz	3		19	1
718	2016-04-28 20:04:28.568027-03	114	Daniel Ortiz	3		19	1
719	2016-04-28 20:04:28.579108-03	127	Francisca Olzman	3		19	1
720	2016-04-28 20:04:28.590664-03	122	Francisca Olzman	3		19	1
721	2016-04-28 20:04:28.601584-03	117	Francisca Olzman	3		19	1
722	2016-04-28 20:04:28.612948-03	125	Ismael Vergara	3		19	1
723	2016-04-28 20:04:28.623598-03	120	Ismael Vergara	3		19	1
724	2016-04-28 20:04:28.634678-03	115	Ismael Vergara	3		19	1
725	2016-04-28 20:04:28.646461-03	123	José Martinez	3		19	1
726	2016-04-28 20:04:28.656935-03	118	José Martinez	3		19	1
727	2016-04-28 20:04:28.668031-03	113	José Martinez	3		19	1
728	2016-04-28 20:04:28.67919-03	126	Pedro Western	3		19	1
729	2016-04-28 20:04:28.690381-03	121	Pedro Western	3		19	1
730	2016-04-28 20:04:28.701411-03	116	Pedro Western	3		19	1
731	2016-04-28 20:04:35.932976-03	151	Esteban Figueroa	3		20	1
732	2016-04-28 20:04:35.950168-03	144	Esteban Figueroa	3		20	1
733	2016-04-28 20:04:35.95944-03	153	José Dono	3		20	1
734	2016-04-28 20:04:35.968094-03	146	José Dono	3		20	1
735	2016-04-28 20:04:35.979737-03	148	Juan Estoril	3		20	1
736	2016-04-28 20:04:35.990489-03	141	Juan Estoril	3		20	1
737	2016-04-28 20:04:36.001607-03	150	Lina Dona	3		20	1
738	2016-04-28 20:04:36.013812-03	143	Lina Dona	3		20	1
739	2016-04-28 20:04:36.023699-03	154	Luna Dominguez	3		20	1
740	2016-04-28 20:04:36.034946-03	147	Luna Dominguez	3		20	1
741	2016-04-28 20:04:36.045935-03	149	Marina Estevez	3		20	1
742	2016-04-28 20:04:36.05702-03	142	Marina Estevez	3		20	1
743	2016-04-28 20:04:36.068353-03	152	Martin Cárcamo	3		20	1
744	2016-04-28 20:04:36.079627-03	145	Martin Cárcamo	3		20	1
745	2016-04-28 20:05:01.242647-03	480	Freire 512, Concepción	3		18	1
746	2016-04-28 20:05:01.257594-03	479	Colón 615, Los Ángeles	3		18	1
747	2016-04-28 20:05:01.268555-03	478	1 Sur 1623, Talca	3		18	1
748	2016-04-28 20:05:01.279599-03	477	Arturo Prat 733, Curicó	3		18	1
749	2016-04-28 20:05:01.290716-03	476	Brasil 1082, Rancagua	3		18	1
750	2016-04-28 20:05:01.301826-03	475	Los Carrera 705, La Calera	3		18	1
751	2016-04-28 20:05:01.31293-03	474	Santiago 598, Viña del Mar	3		18	1
752	2016-04-28 20:05:01.324174-03	473	Teatinos 425, Santiago	3		18	1
753	2016-04-28 20:05:01.33514-03	472	Artesanos 657, Recoleta	3		18	1
754	2016-04-28 20:05:01.346357-03	471	Freire 399, San Bernardo	3		18	1
755	2016-04-28 20:05:01.357444-03	470	Av. Santa Rosa  12786, La Pintana	3		18	1
756	2016-04-28 20:05:01.368694-03	469	Serrano 144, Melipilla	3		18	1
757	2016-04-28 20:05:01.379729-03	468	Vicuña Mackena 1918, Peñaflor	3		18	1
758	2016-04-28 20:05:01.390874-03	467	Av. Independencia 3517, Conchalí	3		18	1
759	2016-04-28 20:05:01.401905-03	466	Henriquez 483, Villarrica	3		18	1
760	2016-04-28 20:05:01.413134-03	465	Independencia 267, Ovalle	3		18	1
761	2016-04-28 20:05:01.424218-03	464	Tarapacá 723, Iquique	3		18	1
762	2016-04-28 20:05:01.435276-03	463	Nancy 725, Ñuñoa	3		18	1
763	2016-04-28 20:05:01.446393-03	462	José Miguel de la Barra  460, Santiago	3		18	1
764	2016-04-28 20:05:01.457588-03	461	Av. Suecia 3304, Ñuñoa	3		18	1
765	2016-04-28 20:05:01.468636-03	460	Av. Príncipe de Gales 6465, La Reina	3		18	1
766	2016-04-28 20:05:01.479841-03	459	Los Duraznos  95, La Pintana	3		18	1
767	2016-04-28 20:05:01.491251-03	458	Arturo Prat 34, Santiago	3		18	1
768	2016-04-28 20:05:01.502054-03	457	Presidente Lincoln 1615, Recoleta	3		18	1
769	2016-04-28 20:05:01.513161-03	456	Freire 512, Concepción	3		18	1
770	2016-04-28 20:05:01.524319-03	455	Colón 615, Los Ángeles	3		18	1
771	2016-04-28 20:05:01.535301-03	454	1 Sur 1623, Talca	3		18	1
772	2016-04-28 20:05:01.546477-03	453	Arturo Prat 733, Curicó	3		18	1
773	2016-04-28 20:05:01.557886-03	452	Brasil 1082, Rancagua	3		18	1
774	2016-04-28 20:05:01.568618-03	451	Los Carrera 705, La Calera	3		18	1
775	2016-04-28 20:05:01.579785-03	450	Santiago 598, Viña del Mar	3		18	1
776	2016-04-28 20:05:01.590964-03	449	Teatinos 425, Santiago	3		18	1
777	2016-04-28 20:05:01.602126-03	448	Artesanos 657, Recoleta	3		18	1
778	2016-04-28 20:05:01.613228-03	447	Freire 399, San Bernardo	3		18	1
779	2016-04-28 20:05:01.624426-03	446	Av. Santa Rosa  12786, La Pintana	3		18	1
780	2016-04-28 20:05:01.63543-03	445	Serrano 144, Melipilla	3		18	1
781	2016-04-28 20:05:01.646588-03	444	Vicuña Mackena 1918, Peñaflor	3		18	1
782	2016-04-28 20:05:01.657603-03	443	Av. Independencia 3517, Conchalí	3		18	1
783	2016-04-28 20:05:01.668746-03	442	Henriquez 483, Villarrica	3		18	1
784	2016-04-28 20:05:01.679925-03	441	Independencia 267, Ovalle	3		18	1
785	2016-04-28 20:05:01.691077-03	440	Tarapacá 723, Iquique	3		18	1
786	2016-04-28 20:05:01.702155-03	439	Nancy 725, Ñuñoa	3		18	1
787	2016-04-28 20:05:01.713323-03	438	José Miguel de la Barra  460, Santiago	3		18	1
788	2016-04-28 20:05:01.724309-03	437	Av. Suecia 3304, Ñuñoa	3		18	1
789	2016-04-28 20:05:01.735433-03	436	Av. Príncipe de Gales 6465, La Reina	3		18	1
790	2016-04-28 20:05:01.746539-03	435	Los Duraznos  95, La Pintana	3		18	1
791	2016-04-28 20:05:01.757614-03	434	Arturo Prat 34, Santiago	3		18	1
792	2016-04-28 20:05:01.768906-03	433	Presidente Lincoln 1615, Recoleta	3		18	1
793	2016-04-28 20:05:01.780012-03	432	Freire 512, Concepción	3		18	1
794	2016-04-28 20:05:01.791111-03	431	Colón 615, Los Ángeles	3		18	1
795	2016-04-28 20:05:01.802237-03	430	1 Sur 1623, Talca	3		18	1
796	2016-04-28 20:05:01.81336-03	429	Arturo Prat 733, Curicó	3		18	1
797	2016-04-28 20:05:01.824428-03	428	Brasil 1082, Rancagua	3		18	1
798	2016-04-28 20:05:01.835524-03	427	Los Carrera 705, La Calera	3		18	1
799	2016-04-28 20:05:01.846604-03	426	Santiago 598, Viña del Mar	3		18	1
800	2016-04-28 20:05:01.857664-03	425	Teatinos 425, Santiago	3		18	1
801	2016-04-28 20:05:01.868829-03	424	Artesanos 657, Recoleta	3		18	1
802	2016-04-28 20:05:01.879952-03	423	Freire 399, San Bernardo	3		18	1
803	2016-04-28 20:05:01.891081-03	422	Av. Santa Rosa  12786, La Pintana	3		18	1
804	2016-04-28 20:05:01.902273-03	421	Serrano 144, Melipilla	3		18	1
805	2016-04-28 20:05:01.913341-03	420	Vicuña Mackena 1918, Peñaflor	3		18	1
806	2016-04-28 20:05:01.924425-03	419	Av. Independencia 3517, Conchalí	3		18	1
807	2016-04-28 20:05:01.935535-03	418	Henriquez 483, Villarrica	3		18	1
808	2016-04-28 20:05:01.946681-03	417	Independencia 267, Ovalle	3		18	1
809	2016-04-28 20:05:01.957849-03	416	Tarapacá 723, Iquique	3		18	1
810	2016-04-28 20:05:01.968972-03	415	Nancy 725, Ñuñoa	3		18	1
811	2016-04-28 20:05:01.980133-03	414	José Miguel de la Barra  460, Santiago	3		18	1
812	2016-04-28 20:05:01.991158-03	413	Av. Suecia 3304, Ñuñoa	3		18	1
813	2016-04-28 20:05:02.00228-03	412	Av. Príncipe de Gales 6465, La Reina	3		18	1
814	2016-04-28 20:05:02.013432-03	411	Los Duraznos  95, La Pintana	3		18	1
815	2016-04-28 20:05:02.025419-03	410	Arturo Prat 34, Santiago	3		18	1
816	2016-04-28 20:05:02.035624-03	409	Presidente Lincoln 1615, Recoleta	3		18	1
817	2016-04-28 20:07:28.240242-03	332	Agrícola BUENA UBICACION	3		21	1
818	2016-04-28 20:07:28.249505-03	319	Agrícola BUENA UBICACION	3		21	1
819	2016-04-28 20:07:28.260541-03	327	Bodega BUENA UBICACION	3		21	1
820	2016-04-28 20:07:28.271672-03	335	Bodega BUENA UBICACION	3		21	1
821	2016-04-28 20:07:28.283058-03	325	Bodega BUENA UBICACION	3		21	1
822	2016-04-28 20:07:28.293902-03	324	Bodega BUENA UBICACION	3		21	1
823	2016-04-28 20:07:28.305413-03	323	Casa BUENA UBICACION	3		21	1
824	2016-04-28 20:07:28.380425-03	322	Casa BUENA UBICACION	3		21	1
825	2016-04-28 20:07:28.393966-03	318	Casa BUENA UBICACION	3		21	1
826	2016-04-28 20:07:28.404994-03	329	Casa BUENA UBICACION	3		21	1
827	2016-04-28 20:07:28.416176-03	320	Casa BUENA UBICACION	3		21	1
828	2016-04-28 20:07:28.427283-03	317	Casa BUENA UBICACION	3		21	1
829	2016-04-28 20:07:28.438334-03	334	Comercial BUENA UBICACION	3		21	1
830	2016-04-28 20:07:28.449513-03	315	Departamento BUENA UBICACION	3		21	1
831	2016-04-28 20:07:28.460565-03	313	Departamento BUENA UBICACION	3		21	1
832	2016-04-28 20:07:28.471685-03	336	Loteo BUENA UBICACION	3		21	1
833	2016-04-28 20:07:28.482795-03	330	Parcela BUENA UBICACION	3		21	1
834	2016-04-28 20:07:28.494052-03	316	Parcela BUENA UBICACION	3		21	1
835	2016-04-28 20:07:28.505053-03	333	Parcela BUENA UBICACION	3		21	1
836	2016-04-28 20:07:28.516684-03	326	Parcela BUENA UBICACION	3		21	1
837	2016-04-28 20:07:28.527391-03	328	Sitio BUENA UBICACION	3		21	1
838	2016-04-28 20:07:28.538365-03	314	Terreno en Construcción BUENA UBICACION	3		21	1
839	2016-04-28 20:07:28.549502-03	331	Terreno en Construcción BUENA UBICACION	3		21	1
840	2016-04-28 20:07:28.56079-03	321	Terreno en Construcción BUENA UBICACION	3		21	1
841	2016-04-28 20:07:36.927399-03	158	Esteban Figueroa	3		20	1
842	2016-04-28 20:07:36.972147-03	160	José Dono	3		20	1
843	2016-04-28 20:07:36.983529-03	155	Juan Estoril	3		20	1
844	2016-04-28 20:07:36.994679-03	157	Lina Dona	3		20	1
845	2016-04-28 20:07:37.008441-03	161	Luna Dominguez	3		20	1
846	2016-04-28 20:07:37.016581-03	156	Marina Estevez	3		20	1
847	2016-04-28 20:07:37.02771-03	159	Martin Cárcamo	3		20	1
848	2016-04-28 20:07:44.857182-03	129	Daniel Ortiz	3		19	1
849	2016-04-28 20:07:44.875063-03	132	Francisca Olzman	3		19	1
850	2016-04-28 20:07:44.883316-03	130	Ismael Vergara	3		19	1
851	2016-04-28 20:07:44.894467-03	128	José Martinez	3		19	1
852	2016-04-28 20:07:44.905691-03	131	Pedro Western	3		19	1
853	2016-04-28 20:07:59.641002-03	504	Freire 512, Concepción	3		18	1
854	2016-04-28 20:07:59.661436-03	503	Colón 615, Los Ángeles	3		18	1
855	2016-04-28 20:07:59.6725-03	502	1 Sur 1623, Talca	3		18	1
856	2016-04-28 20:07:59.683698-03	501	Arturo Prat 733, Curicó	3		18	1
857	2016-04-28 20:07:59.694769-03	500	Brasil 1082, Rancagua	3		18	1
858	2016-04-28 20:07:59.706021-03	499	Los Carrera 705, La Calera	3		18	1
859	2016-04-28 20:07:59.716931-03	498	Santiago 598, Viña del Mar	3		18	1
860	2016-04-28 20:07:59.728002-03	497	Teatinos 425, Santiago	3		18	1
861	2016-04-28 20:07:59.739694-03	496	Artesanos 657, Recoleta	3		18	1
862	2016-04-28 20:07:59.750496-03	495	Freire 399, San Bernardo	3		18	1
863	2016-04-28 20:07:59.761457-03	494	Av. Santa Rosa  12786, La Pintana	3		18	1
864	2016-04-28 20:07:59.77252-03	493	Serrano 144, Melipilla	3		18	1
865	2016-04-28 20:07:59.783668-03	492	Vicuña Mackena 1918, Peñaflor	3		18	1
866	2016-04-28 20:07:59.794745-03	491	Av. Independencia 3517, Conchalí	3		18	1
867	2016-04-28 20:07:59.806556-03	490	Henriquez 483, Villarrica	3		18	1
868	2016-04-28 20:07:59.816978-03	489	Independencia 267, Ovalle	3		18	1
869	2016-04-28 20:07:59.828253-03	488	Tarapacá 723, Iquique	3		18	1
870	2016-04-28 20:07:59.839335-03	487	Nancy 725, Ñuñoa	3		18	1
871	2016-04-28 20:07:59.85036-03	486	José Miguel de la Barra  460, Santiago	3		18	1
872	2016-04-28 20:07:59.861545-03	485	Av. Suecia 3304, Ñuñoa	3		18	1
873	2016-04-28 20:07:59.872638-03	484	Av. Príncipe de Gales 6465, La Reina	3		18	1
874	2016-04-28 20:07:59.883679-03	483	Los Duraznos  95, La Pintana	3		18	1
875	2016-04-28 20:07:59.894795-03	482	Arturo Prat 34, Santiago	3		18	1
876	2016-04-28 20:07:59.905953-03	481	Presidente Lincoln 1615, Recoleta	3		18	1
877	2016-05-08 00:51:16.499601-03	3	Bello	1	Added. Added imagen-home "flisol".	26	1
878	2016-05-10 20:09:15.237541-03	21	Prueba conf	1	Added. Added imagen-home "fam". Added imagen-home "flisol".	26	1
879	2016-05-10 20:11:38.961268-03	1	Primera Configuracion	1	Added.	28	1
880	2016-05-22 21:04:23.520486-03	22	Prima Santiago	1	Added. Added imagen-home "Santiago3". Added imagen-home "Santiago2". Added imagen-home "Santiago1".	26	1
881	2016-05-22 21:04:43.580265-03	1	Primera Configuracion	2	Changed set.	28	1
882	2016-05-30 00:55:50.565056-03	1	Primera Configuracion	2	Changed cantidad_propiedades.	28	1
883	2016-05-30 01:14:08.599567-03	1	Casas en Remate, una excelente oportunidad: published	1	Added.	34	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 883, true);


--
-- Data for Name: django_comment_flags; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_comment_flags (id, flag, flag_date, comment_id, user_id) FROM stdin;
\.


--
-- Name: django_comment_flags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_comment_flags_id_seq', 1, false);


--
-- Data for Name: django_comments; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_comments (id, object_pk, user_name, user_email, user_url, comment, submit_date, ip_address, is_public, is_removed, content_type_id, site_id, user_id) FROM stdin;
\.


--
-- Name: django_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_comments_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	easy_thumbnails	source
8	easy_thumbnails	thumbnail
9	easy_thumbnails	thumbnaildimensions
10	guardian	userobjectpermission
11	guardian	groupobjectpermission
12	pagina	clasificacion
13	pagina	pagina
14	principal	busqueda
15	contactform	contact
16	localizacion	region
17	localizacion	comuna
18	localizacion	localizacion
19	propiedad	propietario
20	propiedad	arquitecto
21	propiedad	propiedad
22	usuario	usuariomjd
23	usuario	profileusermjd
24	propiedad	imagenpropiedad
25	propiedad	preciopropiedad
26	principal	sethomeimage
27	principal	homeimage
28	principal	configuracionhome
29	sites	site
30	django_comments	comment
31	django_comments	commentflag
32	tagging	tag
33	tagging	taggeditem
34	zinnia	entry
35	zinnia	category
36	zinnia	author
37	menu	bookmark
38	dashboard	dashboardpreferences
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_content_type_id_seq', 38, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-04-18 23:45:12.527254-03
2	auth	0001_initial	2016-04-18 23:45:13.295692-03
3	admin	0001_initial	2016-04-18 23:45:13.473815-03
4	admin	0002_logentry_remove_auto_add	2016-04-18 23:45:13.584659-03
5	contenttypes	0002_remove_content_type_name	2016-04-18 23:45:13.629396-03
6	auth	0002_alter_permission_name_max_length	2016-04-18 23:45:13.662833-03
7	auth	0003_alter_user_email_max_length	2016-04-18 23:45:13.684926-03
8	auth	0004_alter_user_username_opts	2016-04-18 23:45:13.703892-03
9	auth	0005_alter_user_last_login_null	2016-04-18 23:45:13.718257-03
10	auth	0006_require_contenttypes_0002	2016-04-18 23:45:13.729453-03
11	auth	0007_alter_validators_add_error_messages	2016-04-18 23:45:13.753771-03
12	pagina	0001_initial	2016-04-18 23:45:14.252981-03
13	contactform	0001_initial	2016-04-18 23:45:14.341952-03
14	contactform	0002_contact_servicio	2016-04-18 23:45:14.530908-03
15	easy_thumbnails	0001_initial	2016-04-18 23:45:15.06505-03
16	easy_thumbnails	0002_thumbnaildimensions	2016-04-18 23:45:15.165175-03
17	guardian	0001_initial	2016-04-18 23:45:15.634418-03
18	localizacion	0001_initial	2016-04-18 23:45:16.247003-03
19	propiedad	0001_initial	2016-04-18 23:45:16.83697-03
20	principal	0001_initial	2016-04-18 23:45:16.959599-03
21	principal	0002_auto_20160418_2344	2016-04-18 23:45:17.361064-03
22	sessions	0001_initial	2016-04-18 23:45:17.516546-03
23	usuario	0001_initial	2016-04-18 23:45:17.752956-03
24	propiedad	0002_imagenpropiedad_preciopropiedad	2016-04-18 23:47:41.233337-03
25	usuario	0002_auto_20160419_1324	2016-04-19 13:24:29.21617-03
26	propiedad	0003_propiedad_usuario	2016-04-22 04:51:32.251465-03
27	propiedad	0004_auto_20160422_0515	2016-04-22 05:15:16.203859-03
28	propiedad	0005_auto_20160422_0805	2016-04-22 08:06:43.470276-03
29	propiedad	0006_auto_20160428_1911	2016-04-28 19:12:35.326475-03
30	principal	0003_auto_20160503_0018	2016-05-03 00:18:11.826643-03
31	sites	0001_initial	2016-05-03 00:21:16.484122-03
32	sites	0002_alter_domain_unique	2016-05-03 00:21:16.584282-03
33	django_comments	0001_initial	2016-05-03 00:21:31.630202-03
34	django_comments	0002_update_user_email_field_length	2016-05-03 00:21:31.674569-03
35	django_comments	0003_add_submit_date_index	2016-05-03 00:21:31.741359-03
36	principal	0004_auto_20160503_0021	2016-05-03 00:21:51.044085-03
37	principal	0005_auto_20160503_0021	2016-05-03 00:22:00.20868-03
38	tagging	0001_initial	2016-05-03 00:22:00.955341-03
39	tagging	0002_auto_20160503_0021	2016-05-03 00:22:01.344328-03
40	principal	0006_auto_20160503_0022	2016-05-03 00:22:12.050116-03
41	zinnia	0001_initial	2016-05-03 00:22:14.492152-03
42	zinnia	0002_lead_paragraph_and_image_caption	2016-05-03 00:22:15.428373-03
43	zinnia	0003_publication_date	2016-05-03 00:22:16.307255-03
44	principal	0007_auto_20160503_0024	2016-05-03 00:24:39.386895-03
45	principal	0008_auto_20160503_0033	2016-05-03 00:33:14.457392-03
46	dashboard	0001_initial	2016-05-03 22:10:12.49668-03
47	menu	0001_initial	2016-05-03 22:10:12.928726-03
48	principal	0009_auto_20160505_0008	2016-05-05 00:08:30.072355-03
49	principal	0010_auto_20160510_2204	2016-05-10 22:05:01.042044-03
50	propiedad	0007_auto_20160510_2204	2016-05-10 22:05:01.922307-03
51	propiedad	0008_auto_20160510_2358	2016-05-10 23:58:30.194528-03
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_migrations_id_seq', 51, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
e0v6alkl2qyw6lq7masorxlpifedt1vc	OTgxZDY5NmYyMWViNzcyNGJlZDlmMThlZTBjZjMzMTI2NjA3MTg2NTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI4MjIyODFjMzczNDc3NDMwYzYwM2VkNTMxNDQ2YWNjN2ExYWUxNjg5In0=	2016-05-22 00:42:54.154291-03
bbeiwfo7ukpsd1239jva4u7l429b61yz	NWZmYjJmZjAyYTUzOTllZTc1MGZhMDE2MmM4ZGIyMGM0OWVlYmU3Mzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4MjIyODFjMzczNDc3NDMwYzYwM2VkNTMxNDQ2YWNjN2ExYWUxNjg5In0=	2016-06-05 20:57:31.893154-03
suqn0935zzw4qe3ke3ogzal2xvufnph0	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:21:59.001703-03
6w1ii5r3g380889qca3ui87lku61d2tl	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:22:31.374283-03
xyjkcm8gqmenobzl3r3f67sgi6p6s86h	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:23:47.575344-03
zrur6scyo915vpqnfi9dna6q4ywx6euf	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:27:09.595975-03
c2lk2unn3r3p27dknyu6uoxt62899bsf	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:28:03.276696-03
m1ynxe2iw4sm47h67t8pawyatygmeqs9	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:28:59.092198-03
m63fx8zy6ri7meo9sr6t2h8ltlehae37	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:30:43.862562-03
swwgsuuiqlco7sd1cdlga3rrw06f4dhm	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-03 14:44:35.324176-03
x9ziizgq3qmo0vi07ekxyrszj0yr1twn	MTZlZmI3MTMxNWFjZmJiOTEzMThhMDRiZjUxZjgxYmQ4YmJmNGI5Njp7fQ==	2016-05-05 21:42:50.848196-03
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY easy_thumbnails_source (id, storage_hash, name, modified) FROM stdin;
\.


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('easy_thumbnails_source_id_seq', 1, false);


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY easy_thumbnails_thumbnail (id, storage_hash, name, modified, source_id) FROM stdin;
\.


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnail_id_seq', 1, false);


--
-- Data for Name: easy_thumbnails_thumbnaildimensions; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY easy_thumbnails_thumbnaildimensions (id, thumbnail_id, width, height) FROM stdin;
\.


--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('easy_thumbnails_thumbnaildimensions_id_seq', 1, false);


--
-- Data for Name: guardian_groupobjectpermission; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY guardian_groupobjectpermission (id, object_pk, content_type_id, group_id, permission_id) FROM stdin;
\.


--
-- Name: guardian_groupobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('guardian_groupobjectpermission_id_seq', 1, false);


--
-- Data for Name: guardian_userobjectpermission; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY guardian_userobjectpermission (id, object_pk, content_type_id, permission_id, user_id) FROM stdin;
\.


--
-- Name: guardian_userobjectpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('guardian_userobjectpermission_id_seq', 1, false);


--
-- Data for Name: localizacion_comuna; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY localizacion_comuna (id, nombre, object_id, content_type_id, region_id) FROM stdin;
1	Iquique	\N	\N	1
2	Alto Hospicio	\N	\N	1
3	Pozo Almonte	\N	\N	1
4	Camiña	\N	\N	1
5	Colchane	\N	\N	1
6	Huara	\N	\N	1
7	Pica	\N	\N	1
8	Antofagasta	\N	\N	2
9	Mejillones	\N	\N	2
10	Sierra Gorda	\N	\N	2
11	Taltal	\N	\N	2
12	Calama	\N	\N	2
13	Ollagüe	\N	\N	2
14	San Pedro de Atacama	\N	\N	2
15	Tocopilla	\N	\N	2
16	María Elena	\N	\N	2
17	Copiapó	\N	\N	3
18	Caldera	\N	\N	3
19	Tierra Amarilla	\N	\N	3
20	Chañaral	\N	\N	3
21	Diego de Almagro	\N	\N	3
22	Vallenar	\N	\N	3
23	Alto del Carmen	\N	\N	3
24	Freirina	\N	\N	3
25	Huasco	\N	\N	3
26	La Serena	\N	\N	4
27	Coquimbo	\N	\N	4
28	Andacollo	\N	\N	4
29	La Higuera	\N	\N	4
30	Paihuano	\N	\N	4
31	Vicuña	\N	\N	4
32	Illapel	\N	\N	4
33	Canela	\N	\N	4
34	Los Vilos	\N	\N	4
35	Salamanca	\N	\N	4
36	Ovalle	\N	\N	4
37	Combarbalá	\N	\N	4
38	Monte Patria	\N	\N	4
39	Punitaqui	\N	\N	4
40	Río Hurtado	\N	\N	4
41	Valparaíso	\N	\N	5
42	Casablanca	\N	\N	5
43	Concón	\N	\N	5
44	Juan Fernández	\N	\N	5
45	Puchuncaví	\N	\N	5
46	Quintero	\N	\N	5
47	Viña del Mar	\N	\N	5
48	Isla de Pascua	\N	\N	5
49	Los Andes	\N	\N	5
50	Calle Larga	\N	\N	5
51	Rinconada	\N	\N	5
52	San Esteban	\N	\N	5
53	La Ligua	\N	\N	5
54	Cabildo	\N	\N	5
55	Papudo	\N	\N	5
56	Petorca	\N	\N	5
57	Zapallar	\N	\N	5
58	Quillota	\N	\N	5
59	La Calera	\N	\N	5
60	Hijuelas	\N	\N	5
61	La Cruz	\N	\N	5
62	Nogales	\N	\N	5
63	San Antonio	\N	\N	5
64	Algarrobo	\N	\N	5
65	Cartagena	\N	\N	5
66	El Quisco	\N	\N	5
67	El Tabo	\N	\N	5
68	Santo Domingo	\N	\N	5
69	San Felipe	\N	\N	5
70	Catemu	\N	\N	5
71	Llay Llay	\N	\N	5
72	Panquehue	\N	\N	5
73	Putaendo	\N	\N	5
74	Santa María	\N	\N	5
75	Quilpué	\N	\N	5
76	Limache	\N	\N	5
77	Olmué	\N	\N	5
78	Villa Alemana	\N	\N	5
79	Rancagua	\N	\N	6
80	Codegua	\N	\N	6
81	Coinco	\N	\N	6
82	Coltauco	\N	\N	6
83	Doñihue	\N	\N	6
84	Graneros	\N	\N	6
85	Las Cabras	\N	\N	6
86	Machalí	\N	\N	6
87	Malloa	\N	\N	6
88	Mostazal	\N	\N	6
89	Olivar	\N	\N	6
90	Peumo	\N	\N	6
91	Pichidegua	\N	\N	6
92	Quinta de Tilcoco	\N	\N	6
93	Rengo	\N	\N	6
94	Requínoa	\N	\N	6
95	San Vicente	\N	\N	6
96	Pichilemu	\N	\N	6
97	La Estrella	\N	\N	6
98	Litueche	\N	\N	6
99	Marchihue	\N	\N	6
100	Navidad	\N	\N	6
101	Paredones	\N	\N	6
102	San Fernando	\N	\N	6
103	Chépica	\N	\N	6
104	Chimbarongo	\N	\N	6
105	Lolol	\N	\N	6
106	Nancagua	\N	\N	6
107	Palmilla	\N	\N	6
108	Peralillo	\N	\N	6
109	Placilla	\N	\N	6
110	Pumanque	\N	\N	6
111	Santa Cruz	\N	\N	6
112	Talca	\N	\N	7
113	Constitución	\N	\N	7
114	Curepto	\N	\N	7
115	Empedrado	\N	\N	7
116	Maule	\N	\N	7
117	Pelarco	\N	\N	7
118	Pencahue	\N	\N	7
119	Río Claro	\N	\N	7
120	San Clemente	\N	\N	7
121	San Rafael	\N	\N	7
122	Cauquenes	\N	\N	7
123	Chanco	\N	\N	7
124	Pelluhue	\N	\N	7
125	Curicó	\N	\N	7
126	Hualañé	\N	\N	7
127	Licantén	\N	\N	7
128	Molina	\N	\N	7
129	Rauco	\N	\N	7
130	Romeral	\N	\N	7
131	Sagrada Familia	\N	\N	7
132	Teno	\N	\N	7
133	Vichuquén	\N	\N	7
134	Linares	\N	\N	7
135	Colbún	\N	\N	7
136	Longaví	\N	\N	7
137	Parral	\N	\N	7
138	Retiro	\N	\N	7
139	San Javier	\N	\N	7
140	Villa Alegre	\N	\N	7
141	Yerbas Buenas	\N	\N	7
142	Concepción	\N	\N	8
143	Coronel	\N	\N	8
144	Chiguayante	\N	\N	8
145	Florida	\N	\N	8
146	Hualqui	\N	\N	8
147	Lota	\N	\N	8
148	Penco	\N	\N	8
149	San Pedro de la Paz	\N	\N	8
150	Santa Juana	\N	\N	8
151	Talcahuano	\N	\N	8
152	Tomé	\N	\N	8
153	Hualpén	\N	\N	8
154	Lebu	\N	\N	8
155	Arauco	\N	\N	8
156	Cañete	\N	\N	8
157	Contulmo	\N	\N	8
158	Curanilahue	\N	\N	8
159	Los Álamos	\N	\N	8
160	Tirúa	\N	\N	8
161	Los Ángeles	\N	\N	8
162	Antuco	\N	\N	8
163	Cabrero	\N	\N	8
164	Laja	\N	\N	8
165	Mulchén	\N	\N	8
166	Nacimiento	\N	\N	8
167	Negrete	\N	\N	8
168	Quilaco	\N	\N	8
169	Quilleco	\N	\N	8
170	San Rosendo	\N	\N	8
171	Santa Bárbara	\N	\N	8
172	Tucapel	\N	\N	8
173	Yumbel	\N	\N	8
174	Alto Biobío	\N	\N	8
175	Chillán	\N	\N	8
176	Bulnes	\N	\N	8
177	Cobquecura	\N	\N	8
178	Coelemu	\N	\N	8
179	Coihueco	\N	\N	8
180	Chillán Viejo	\N	\N	8
181	El Carmen	\N	\N	8
182	Ninhue	\N	\N	8
183	Ñiquén	\N	\N	8
184	Pemuco	\N	\N	8
185	Pinto	\N	\N	8
186	Portezuelo	\N	\N	8
187	Quillón	\N	\N	8
188	Quirihue	\N	\N	8
189	Ránquil	\N	\N	8
190	San Carlos	\N	\N	8
191	San Fabián	\N	\N	8
192	San Ignacio	\N	\N	8
193	San Nicolás	\N	\N	8
194	Treguaco	\N	\N	8
195	Yungay	\N	\N	8
196	Temuco	\N	\N	9
197	Carahue	\N	\N	9
198	Cunco	\N	\N	9
199	Curarrehue	\N	\N	9
200	Freire	\N	\N	9
201	Galvarino	\N	\N	9
202	Gorbea	\N	\N	9
203	Lautaro	\N	\N	9
204	Loncoche	\N	\N	9
205	Melipeuco	\N	\N	9
206	Nueva Imperial	\N	\N	9
207	Padre las Casas	\N	\N	9
208	Perquenco	\N	\N	9
209	Pitrufquén	\N	\N	9
210	Pucón	\N	\N	9
211	Saavedra	\N	\N	9
212	Teodoro Schmidt	\N	\N	9
213	Toltén	\N	\N	9
214	Vilcún	\N	\N	9
215	Villarrica	\N	\N	9
216	Cholchol	\N	\N	9
217	Angol	\N	\N	9
218	Collipulli	\N	\N	9
219	Curacautín	\N	\N	9
220	Ercilla	\N	\N	9
221	Lonquimay	\N	\N	9
222	Los Sauces	\N	\N	9
223	Lumaco	\N	\N	9
224	Purén	\N	\N	9
225	Renaico	\N	\N	9
226	Traiguén	\N	\N	9
227	Victoria	\N	\N	9
228	Puerto Montt	\N	\N	10
229	Calbuco	\N	\N	10
230	Cochamó	\N	\N	10
231	Fresia	\N	\N	10
232	Frutillar	\N	\N	10
233	Los Muermos	\N	\N	10
234	Llanquihue	\N	\N	10
235	Maullín	\N	\N	10
236	Puerto Varas	\N	\N	10
237	Castro	\N	\N	10
238	Ancud	\N	\N	10
239	Chonchi	\N	\N	10
240	Curaco de Vélez	\N	\N	10
241	Dalcahue	\N	\N	10
242	Puqueldón	\N	\N	10
243	Queilén	\N	\N	10
244	Quellón	\N	\N	10
245	Quemchi	\N	\N	10
246	Quinchao	\N	\N	10
247	Osorno	\N	\N	10
248	Puerto Octay	\N	\N	10
249	Purranque	\N	\N	10
250	Puyehue	\N	\N	10
251	Río Negro	\N	\N	10
252	San Juan de la Costa	\N	\N	10
253	San Pablo	\N	\N	10
254	Chaitén	\N	\N	10
255	Futaleufú	\N	\N	10
256	Hualaihué	\N	\N	10
257	Palena	\N	\N	10
258	Coyhaique	\N	\N	11
259	Lago Verde	\N	\N	11
260	Aysén	\N	\N	11
261	Cisnes	\N	\N	11
262	Guaitecas	\N	\N	11
263	Cochrane	\N	\N	11
264	O'Higgins	\N	\N	11
265	Tortel	\N	\N	11
266	Chile Chico	\N	\N	11
267	Río Ibáñez	\N	\N	11
268	Punta Arenas	\N	\N	12
269	Laguna Blanca	\N	\N	12
270	Río Verde	\N	\N	12
271	San Gregorio	\N	\N	12
272	Cabo de Hornos	\N	\N	12
273	Antártica	\N	\N	12
274	Porvenir	\N	\N	12
275	Primavera	\N	\N	12
276	Timaukel	\N	\N	12
277	Natales	\N	\N	12
278	Torres del Paine	\N	\N	12
279	Santiago	\N	\N	13
280	Cerrillos	\N	\N	13
281	Cerro Navia	\N	\N	13
282	Conchalí	\N	\N	13
283	El Bosque	\N	\N	13
284	Estación Central	\N	\N	13
285	Huechuraba	\N	\N	13
286	Independencia	\N	\N	13
287	La Cisterna	\N	\N	13
288	La Florida	\N	\N	13
289	La Granja	\N	\N	13
290	La Pintana	\N	\N	13
291	La Reina	\N	\N	13
292	Las Condes	\N	\N	13
293	Lo Barnechea	\N	\N	13
294	Lo Espejo	\N	\N	13
295	Lo Prado	\N	\N	13
296	Macul	\N	\N	13
297	Maipú	\N	\N	13
298	Ñuñoa	\N	\N	13
299	Pedro Aguirre Cerda	\N	\N	13
300	Peñalolén	\N	\N	13
301	Providencia	\N	\N	13
302	Pudahuel	\N	\N	13
303	Quilicura	\N	\N	13
304	Quinta Normal	\N	\N	13
305	Recoleta	\N	\N	13
306	Renca	\N	\N	13
307	San Joaquín	\N	\N	13
308	San Miguel	\N	\N	13
309	San Ramón	\N	\N	13
310	Vitacura	\N	\N	13
311	Puente Alto	\N	\N	13
312	Pirque	\N	\N	13
313	San José de Maipo	\N	\N	13
314	Colina	\N	\N	13
315	Lampa	\N	\N	13
316	Tiltil	\N	\N	13
317	San Bernardo	\N	\N	13
318	Buin	\N	\N	13
319	Calera de Tango	\N	\N	13
320	Paine	\N	\N	13
321	Melipilla	\N	\N	13
322	Alhué	\N	\N	13
323	Curacaví	\N	\N	13
324	María Pinto	\N	\N	13
325	San Pedro	\N	\N	13
326	Talagante	\N	\N	13
327	El Monte	\N	\N	13
328	Isla de Maipo	\N	\N	13
329	Padre Hurtado	\N	\N	13
330	Peñaflor	\N	\N	13
331	Valdivia	\N	\N	14
332	Corral	\N	\N	14
333	Lanco	\N	\N	14
334	Los Lagos	\N	\N	14
335	Máfil	\N	\N	14
336	Mariquina	\N	\N	14
337	Paillaco	\N	\N	14
338	Panguipulli	\N	\N	14
339	La Unión	\N	\N	14
340	Futrono	\N	\N	14
341	Lago Ranco	\N	\N	14
342	Río Bueno	\N	\N	14
343	Arica	\N	\N	15
344	Camarones	\N	\N	15
345	Putre	\N	\N	15
346	General Lagos	\N	\N	15
\.


--
-- Name: localizacion_comuna_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('localizacion_comuna_id_seq', 346, true);


--
-- Data for Name: localizacion_localizacion; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY localizacion_localizacion (id, ubicacion, numero, geo_point, object_id, comuna_id, content_type_id) FROM stdin;
505	Presidente Lincoln	1615	0101000020E61000004714A46318A851C0D7828362D0B440C0	\N	305	\N
506	Arturo Prat	34	0101000020E61000000FA782409DA951C09BD2B0CEE7B840C0	\N	279	\N
507	Los Duraznos 	95	0101000020E61000003A56293DD3A851C0CC738E4B66CE40C0	\N	290	\N
508	Av. Príncipe de Gales	6465	0101000020E6100000F6D786E52AA451C091099D7C1FB840C0	\N	291	\N
509	Av. Suecia	3304	0101000020E61000007B9054956CA651C0F0CFFBB5ABB940C0	\N	298	\N
510	José Miguel de la Barra 	460	0101000020E6100000BFFD6FDB2DA951C03DED951EF2B740C0	\N	279	\N
511	Nancy	725	0101000020E610000031EE06D1DAA751C09AEE75525FBA40C0	\N	298	\N
512	Tarapacá	723	0101000020E61000007EB78A0F7D8951C031A6AA1ACE3634C0	\N	1	\N
513	Independencia	267	0101000020E6100000E84CDA54DDCC51C042131736B99A3EC0	\N	36	\N
514	Henriquez	483	0101000020E6100000C6B4801A880E52C0FEC8BE750FA443C0	\N	215	\N
515	Av. Independencia	3517	0101000020E6100000569DD502FBAA51C0AA0A0DC4B2B240C0	\N	282	\N
516	Vicuña Mackena	1918	0101000020E610000081C8C79349B851C0B2EABD4230CE40C0	\N	330	\N
517	Serrano	144	0101000020E610000000016BD5AECD51C04F6095664ED740C0	\N	321	\N
518	Av. Santa Rosa 	12786	0101000020E61000001252126D22A851C08A8F4FC8CECC40C0	\N	290	\N
519	Freire	399	0101000020E610000066097C8FC4AC51C002542756FCCA40C0	\N	317	\N
520	Artesanos	657	0101000020E610000067DA594F88A951C07F29A84D41B740C0	\N	305	\N
521	Teatinos	425	0101000020E6100000BAF770C9F1A951C0C84BEDFB26B840C0	\N	279	\N
522	Santiago	598	0101000020E610000015359886E1AD51C0344289E0C9AD40C0	\N	47	\N
523	Los Carrera	705	0101000020E6100000C1ABE5CE4CCC51C063DA92B0CA6440C0	\N	59	\N
524	Brasil	1082	0101000020E61000008209922D01B051C0AED7F4A0A01541C0	\N	79	\N
525	Arturo Prat	733	0101000020E6100000D78A9180ACCF51C0AB1ACE46F87D41C0	\N	125	\N
526	1 Sur	1623	0101000020E610000044D4E9E59CE851C0D9AA6E3F2AB741C0	\N	112	\N
527	Colón	615	0101000020E610000013E74DEA701652C0D5C276418EBB42C0	\N	161	\N
528	Freire	512	0101000020E61000009D8598A6634352C042D4D8B9C46942C0	\N	142	\N
\.


--
-- Name: localizacion_localizacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('localizacion_localizacion_id_seq', 528, true);


--
-- Data for Name: localizacion_region; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY localizacion_region (id, nombre, numero_decimal, numero_romano, pais, object_id, content_type_id) FROM stdin;
1	Tarapacá	1	I	CL	\N	\N
2	Antofagasta	2	II	CL	\N	\N
3	Atacama	3	III	CL	\N	\N
4	Coquimbo	4	IV	CL	\N	\N
5	Valparaíso	5	V	CL	\N	\N
6	Región del Libertador Gral. Bernardo O'Higgins	6	VI	CL	\N	\N
7	Región del Maule	7	VII	CL	\N	\N
8	Región del Biobío	8	VIII	CL	\N	\N
9	Región de la Araucanía	9	IX	CL	\N	\N
10	Región de Los Lagos	10	X	CL	\N	\N
11	Región Aisén del Gral. Carlos Ibáñez del Campo	11	XI	CL	\N	\N
12	Región de Magallanes y de la Antártica Chilena	12	XII	CL	\N	\N
13	Región Metropolitana de Santiago	0	RM	CL	\N	\N
14	Región de Los Ríos	14	XIV	CL	\N	\N
15	Arica y Parinacota	15	XV	CL	\N	\N
\.


--
-- Name: localizacion_region_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('localizacion_region_id_seq', 15, true);


--
-- Data for Name: pagina_clasificacion; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY pagina_clasificacion (id, tipo, slug_tipo, descripccion, fecha_publicacion, object_id, content_type_id) FROM stdin;
\.


--
-- Name: pagina_clasificacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('pagina_clasificacion_id_seq', 1, false);


--
-- Data for Name: pagina_pagina; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY pagina_pagina (id, titulo, slug_titulo, imagen, fecha_publicacion, resumen, cuerpo, object_id, clasificacion_id, content_type_id) FROM stdin;
\.


--
-- Name: pagina_pagina_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('pagina_pagina_id_seq', 1, false);


--
-- Data for Name: principal_busqueda; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_busqueda (id, tipo_adquisicion, estado, tipo_propiedad, tiempo_publicacion, fecha_busqueda, object_id, content_type_id) FROM stdin;
\.


--
-- Name: principal_busqueda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_busqueda_id_seq', 1, false);


--
-- Data for Name: principal_busqueda_set_resultados; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_busqueda_set_resultados (id, busqueda_id, propiedad_id) FROM stdin;
\.


--
-- Name: principal_busqueda_set_resultados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_busqueda_set_resultados_id_seq', 1, false);


--
-- Data for Name: principal_busqueda_ubicacion; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_busqueda_ubicacion (id, gm2m_src_id, gm2m_ct_id, gm2m_pk) FROM stdin;
\.


--
-- Name: principal_busqueda_ubicacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_busqueda_ubicacion_id_seq', 1, false);


--
-- Data for Name: principal_configuracionhome; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_configuracionhome (id, nombre, mostrar_ultimas_propiedades, cantidad_propiedades, orden_propiedades, clase_css_propiedades_home, mostrar_destacados, cantidad_destacados, aleatorio_destacados, primera_fecha_destacados, clase_css_propiedades_destacadas_home, activar_blog, set_id, slug_nombre) FROM stdin;
1	Primera Configuracion	t	8	t	propiedades-primera	f	0	f	2016-05-10	0	t	22	primera-configuracion
\.


--
-- Name: principal_configuracionhome_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_configuracionhome_id_seq', 1, true);


--
-- Data for Name: principal_homeimage; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_homeimage (id, imagen, etiqueta, set_id) FROM stdin;
2	home/imagen/pineda.jpeg	sf	12
3	home/imagen/pineda_sxZ6ROT.jpeg	sf	13
4	home/imagen/pineda_HDTlfJI.jpeg	sf	14
5	home/imagen/pineda_XxAt4Uo.jpeg	sf	15
6	home/imagen/pineda_wo52o1V.jpeg	sf	16
7	home/imagen/pineda_zGEQOmH.jpeg	sf	17
8	home/imagen/pineda_gDXIlCl.jpeg	sf	18
9	home/imagen/pineda_mDl1IiH.jpeg	sf	19
10	home/imagen/pineda_UBLhUpQ.jpeg	sf	20
11	home/imagen/2016-02-27-09-09-23-992.jpg	fam	21
12	home/imagen/DSCF1176_vjDFiqw.JPG	flisol	21
13	home/imagen/stgo3.jpg	Santiago3	22
14	home/imagen/stgo2.jpg	Santiago2	22
15	home/imagen/stgo1.jpg	Santiago1	22
\.


--
-- Name: principal_homeimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_homeimage_id_seq', 15, true);


--
-- Data for Name: principal_sethomeimage; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY principal_sethomeimage (id, nombre, fecha) FROM stdin;
4	Bello	2016-05-08
6	David Alejandro	2016-05-09
7	David Alejandro	2016-05-09
8	David Alejandro	2016-05-09
9	David Alejandro	2016-05-09
10	Perico Propiedades	2016-05-09
11	Perico Propiedades	2016-05-09
12	Perico Propiedasdsaddes	2016-05-09
13	Perico Propiedasdsaddes	2016-05-09
14	Perico Propiedasdsaddes	2016-05-09
15	Perico Propiedasdsaddes	2016-05-09
16	Perico Propiedasdsaddes	2016-05-09
17	Perico Propiedasdsaddes	2016-05-09
18	Perico Propiedasdsaddes	2016-05-09
19	Perico Propiedasdsaddes	2016-05-09
20	Perico Propiedasdsaddes	2016-05-09
21	Prueba conf	2016-05-10
22	Prima Santiago	2016-05-23
\.


--
-- Name: principal_sethomeimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('principal_sethomeimage_id_seq', 22, true);


--
-- Data for Name: propiedad_arquitecto; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY propiedad_arquitecto (id, nombre, website, email, fono, object_id, content_type_id) FROM stdin;
162	Juan Estoril	www.jjee.cl	arqu1@kilo.cl	5451121	\N	\N
163	Marina Estevez	ww.mmee.cl	arqu2@kilo.cl	5451122	\N	\N
164	Lina Dona	www.lldd.cl	arqu3@kilo.cl	5451123	\N	\N
165	Esteban Figueroa	www.eeff.cl	arqu4@kilo.cl	5451124	\N	\N
166	Martin Cárcamo	www.mmcc.cl	arqu5@kilo.cl	5451125	\N	\N
167	José Dono	www.jjdd.cl	arqu6@kilo.cl	5451126	\N	\N
168	Luna Dominguez	www.lldd.cl	arqu7@kilo.cl	5451127	\N	\N
\.


--
-- Name: propiedad_arquitecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('propiedad_arquitecto_id_seq', 168, true);


--
-- Data for Name: propiedad_imagenpropiedad; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY propiedad_imagenpropiedad (id, imagen, etiqueta, principal, propiedad_id) FROM stdin;
380	propiedad/imagen/propiedad1_7sLZmoq.jpg	Propiedad 1	t	337
381	propiedad/imagen/propiedad2_Rzyt7PN.jpg	Propiedad 2	t	338
382	propiedad/imagen/propiedad3_lFP0E9t.jpg	Propiedad 3	t	339
383	propiedad/imagen/propiedad4_7fKbo9M.jpg	Propiedad 4	t	340
384	propiedad/imagen/propiedad5_LqeninK.jpg	Propiedad 5	t	341
385	propiedad/imagen/propiedad6_77iR4s5.jpg	Propiedad 6	t	342
386	propiedad/imagen/propiedad7_cb0oOFC.jpg	Propiedad 7	t	343
387	propiedad/imagen/propiedad8_MA11Gzx.jpg	Propiedad 8	t	344
388	propiedad/imagen/propiedad9_nSotW4y.jpg	Propiedad 9	t	345
389	propiedad/imagen/propiedad10_VYGa5jK.jpg	Propiedad 10	t	346
390	propiedad/imagen/propiedad11_ERkw29c.jpg	Propiedad 11	t	347
391	propiedad/imagen/propiedad12_w7bRbE8.jpg	Propiedad 12	t	348
392	propiedad/imagen/propiedad13_QD7jEDV.jpg	Propiedad 13	t	349
393	propiedad/imagen/propiedad14_H2S0q1n.jpg	Propiedad 14	t	350
394	propiedad/imagen/propiedad15_VCKLRcR.jpg	Propiedad 15	t	351
395	propiedad/imagen/propiedad16_zSFoon0.jpg	Propiedad 16	t	352
396	propiedad/imagen/propiedad17_YCwTgBp.jpg	Propiedad 17	t	353
397	propiedad/imagen/propiedad18_Ov2ydsF.jpg	Propiedad 18	t	354
398	propiedad/imagen/propiedad19_plUywnq.jpg	Propiedad 19	t	355
399	propiedad/imagen/propiedad20_Dj7Fa4o.jpg	Propiedad 20	t	356
400	propiedad/imagen/propiedad21_Pb4Or6h.jpg	Propiedad 21	t	357
401	propiedad/imagen/propiedad22_Niw09Zj.jpg	Propiedad 22	t	358
402	propiedad/imagen/propiedad23_7iwigjq.jpg	Propiedad 23	t	359
403	propiedad/imagen/propiedad24_2F4GJJ7.jpg	Propiedad 24	t	360
404	propiedad/imagen/propiedad25_qoFVag2.jpg	Propiedad 25	f	337
405	propiedad/imagen/propiedad26_Rwcp0t8.jpg	Propiedad 26	f	338
406	propiedad/imagen/propiedad27_3VngFGi.jpg	Propiedad 27	f	339
407	propiedad/imagen/propiedad28_StFJiSy.jpg	Propiedad 28	f	340
408	propiedad/imagen/propiedad29_LjuoK6N.jpg	Propiedad 29	f	341
409	propiedad/imagen/propiedad30_GOKwM3l.jpg	Propiedad 30	f	342
410	propiedad/imagen/propiedad31_cvmKaxS.jpg	Propiedad 31	f	343
411	propiedad/imagen/propiedad32_6lijo0W.jpg	Propiedad 32	f	344
412	propiedad/imagen/propiedad33_47X2Zvd.jpg	Propiedad 33	f	345
413	propiedad/imagen/propiedad34_mytK3rd.jpg	Propiedad 34	f	346
414	propiedad/imagen/propiedad35_osOkF5X.jpg	Propiedad 35	f	347
415	propiedad/imagen/propiedad36_1qWDMpY.jpg	Propiedad 36	f	348
416	propiedad/imagen/propiedad37_dRt30dX.jpg	Propiedad 37	f	349
417	propiedad/imagen/propiedad38_494hm10.jpg	Propiedad 38	f	350
418	propiedad/imagen/propiedad39_jSnhHAc.jpg	Propiedad 39	f	351
419	propiedad/imagen/propiedad40_bTrgk5V.jpg	Propiedad 40	f	352
420	propiedad/imagen/propiedad41_0GnU3DI.jpg	Propiedad 41	f	353
421	propiedad/imagen/propiedad42_2SdV0Gm.jpg	Propiedad 42	f	354
\.


--
-- Name: propiedad_imagenpropiedad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('propiedad_imagenpropiedad_id_seq', 421, true);


--
-- Data for Name: propiedad_preciopropiedad; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY propiedad_preciopropiedad (id, moneda, cantidad, propiedad_id) FROM stdin;
121	Dólar Americano	5577	337
122	Dólar Americano	1176	338
123	Dólar Americano	3004	339
124	Peso	3202	340
125	Peso	4925	341
126	Peso	2776	342
127	Unidad de Fomento	1664	343
128	Unidad de Fomento	640	344
129	Peso	2733	345
130	Unidad de Fomento	5298	346
131	Peso	5527	347
132	Peso	2564	348
133	Unidad de Fomento	2236	349
134	Peso	4715	350
135	Peso	4067	351
136	Unidad de Fomento	3601	352
137	Unidad de Fomento	2204	353
138	Dólar Americano	3163	354
139	Peso	3758	355
140	Peso	611	356
141	Unidad de Fomento	977	357
142	Dólar Americano	5410	358
143	Unidad de Fomento	2556	359
144	Peso	821	360
\.


--
-- Name: propiedad_preciopropiedad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('propiedad_preciopropiedad_id_seq', 144, true);


--
-- Data for Name: propiedad_propiedad; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY propiedad_propiedad (id, nombre, estado, caracter, tipo_propiedad, fecha_registro, cantidad_dormitorios, "cantidad_baños", area, descripcion, fecha_entrega, en_oferta, object_id, arquitecto_id, content_type_id, localizacion_id, propietario_id, usuario_id) FROM stdin;
337	Departamento BUENA UBICACION	used	Buy	Depto	2016-04-28	5	1	111	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-09	f	\N	162	\N	528	136	5
338	Terreno en Construcción BUENA UBICACION	used	Buy	Cons	2016-04-28	3	1	78	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-06	t	\N	168	\N	527	137	4
339	Departamento BUENA UBICACION	new	Buy	Depto	2016-04-28	3	3	77	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-06	f	\N	162	\N	526	133	2
340	Parcela BUENA UBICACION	new	Buy	Parc	2016-04-28	4	2	69	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-29	t	\N	165	\N	525	134	1
341	Casa BUENA UBICACION	new	Lease	Casa	2016-04-28	5	2	100	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-02	f	\N	167	\N	524	133	3
342	Casa BUENA UBICACION	used	Buy	Casa	2016-04-28	4	4	25	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-28	t	\N	167	\N	523	137	5
343	Agrícola BUENA UBICACION	new	Buy	Agr	2016-04-28	3	1	101	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-04-29	t	\N	167	\N	522	134	5
344	Casa BUENA UBICACION	used	Lease	Casa	2016-04-28	5	2	41	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-17	t	\N	162	\N	521	137	4
345	Terreno en Construcción BUENA UBICACION	used	Lease	Cons	2016-04-28	4	4	34	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-22	f	\N	166	\N	520	135	5
346	Casa BUENA UBICACION	new	Buy	Casa	2016-04-28	2	4	109	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-07	f	\N	162	\N	519	135	4
347	Casa BUENA UBICACION	used	Buy	Casa	2016-04-28	3	4	83	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-06	f	\N	166	\N	518	136	2
348	Bodega BUENA UBICACION	new	Lease	Bod	2016-04-28	3	2	46	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-03	f	\N	162	\N	517	134	4
349	Bodega BUENA UBICACION	used	Lease	Bod	2016-04-28	4	4	28	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-22	f	\N	163	\N	516	134	1
350	Parcela BUENA UBICACION	new	Lease	Parc	2016-04-28	2	4	72	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-29	f	\N	165	\N	515	136	2
351	Bodega BUENA UBICACION	used	Buy	Bod	2016-04-28	5	4	11	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-04-30	t	\N	163	\N	514	134	4
352	Sitio BUENA UBICACION	new	Buy	Sitio	2016-04-28	2	2	54	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-02	t	\N	163	\N	513	136	5
353	Casa BUENA UBICACION	new	Lease	Casa	2016-04-28	5	2	69	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-06	t	\N	162	\N	512	133	3
354	Parcela BUENA UBICACION	new	Buy	Parc	2016-04-28	4	1	57	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-05	t	\N	168	\N	511	134	3
355	Terreno en Construcción BUENA UBICACION	new	Lease	Cons	2016-04-28	1	1	12	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-13	f	\N	165	\N	510	136	3
356	Agrícola BUENA UBICACION	used	Buy	Agr	2016-04-28	1	4	75	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-19	t	\N	165	\N	509	133	3
357	Parcela BUENA UBICACION	used	Lease	Parc	2016-04-28	4	3	43	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-16	f	\N	165	\N	508	135	1
358	Comercial BUENA UBICACION	used	Buy	Comerc	2016-04-28	4	1	114	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-05-01	t	\N	166	\N	507	136	2
359	Bodega BUENA UBICACION	new	Lease	Bod	2016-04-28	4	3	41	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed luctus est, vitae finibus odio. Proin lacinia molestie mollis. Integer aliquam mattis eros sit amet dignissim. Curabitur sed porttitor felis, vitae aliquam est. Nullam aliquet scelerisque diam sit amet elementum. Integer a placerat purus, vel volutpat est. Mauris porttitor rhoncus sem, et rutrum ex imperdiet vel. Donec pharetra pulvinar ligula a tristique. Ut id ipsum velit. Donec rhoncus dolor odio, eget posuere enim posuere vitae. Donec vel ullamcorper enim, eu tristique nunc. Etiam maximus interdum ex, imperdiet luctus mi maximus ullamcorper. </p>	2016-05-25	t	\N	164	\N	506	134	4
360	Loteo BUENA UBICACION	used	Lease	Lot	2016-04-28	1	3	78	<p>Morbi at mauris nec risus pretium finibus et ut leo. Vestibulum placerat ligula purus, non semper mauris ornare non. Integer hendrerit eleifend urna, ac faucibus nisl faucibus eu. Nulla tortor dui, consectetur sed volutpat ut, aliquet sit amet turpis. Integer cursus nulla a laoreet gravida. Aliquam a luctus velit, at scelerisque ex. Ut sed condimentum velit. </p>	2016-04-29	t	\N	168	\N	505	135	3
\.


--
-- Name: propiedad_propiedad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('propiedad_propiedad_id_seq', 360, true);


--
-- Data for Name: propiedad_propietario; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY propiedad_propietario (id, nombre, website, email, fono, object_id, content_type_id) FROM stdin;
133	José Martinez	www.jjmm.cl	jmarti@lopo.cl	456289423	\N	\N
134	Daniel Ortiz	www.ddoo.cl	dortiz@lopo.cl	456289424	\N	\N
135	Ismael Vergara	www.iivv.cl	iverga@lopo.cl	456289425	\N	\N
136	Pedro Western	www.pppww.cl	pwest@lopo.cl	456289426	\N	\N
137	Francisca Olzman	www.ffoo.cl	kikaolz@lopo.cl	456289427	\N	\N
\.


--
-- Name: propiedad_propietario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('propiedad_propietario_id_seq', 137, true);


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spatial_ref_sys  FROM stdin;
\.


--
-- Data for Name: tagging_tag; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY tagging_tag (id, name) FROM stdin;
\.


--
-- Name: tagging_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('tagging_tag_id_seq', 1, false);


--
-- Data for Name: tagging_taggeditem; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY tagging_taggeditem (id, object_id, content_type_id, tag_id) FROM stdin;
\.


--
-- Name: tagging_taggeditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('tagging_taggeditem_id_seq', 1, false);


--
-- Data for Name: us_gaz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_gaz  FROM stdin;
\.


--
-- Data for Name: us_lex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_lex  FROM stdin;
\.


--
-- Data for Name: us_rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY us_rules  FROM stdin;
\.


--
-- Data for Name: usuario_profileusermjd; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY usuario_profileusermjd (id, interaction, user_id) FROM stdin;
1	0	2
2	0	3
3	0	4
4	0	5
\.


--
-- Name: usuario_profileusermjd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('usuario_profileusermjd_id_seq', 4, true);


--
-- Data for Name: usuario_usuariomjd; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY usuario_usuariomjd (user_ptr_id, telefono, imagen, grupo) FROM stdin;
2	595665	imagen_perfil/paty.jpg	administrador
3	8465512	imagen_perfil/802332385_82278.jpg	vendedor
4	3432523	imagen_perfil/pineda.jpeg	vendedor
5	3423423	imagen_perfil/gato.jpg	vendedor
\.


--
-- Data for Name: zinnia_category; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_category (id, title, slug, description, lft, rght, tree_id, level, parent_id) FROM stdin;
\.


--
-- Name: zinnia_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_category_id_seq', 1, false);


--
-- Data for Name: zinnia_entry; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_entry (id, title, slug, status, start_publication, end_publication, creation_date, last_update, content, comment_enabled, pingback_enabled, trackback_enabled, comment_count, pingback_count, trackback_count, excerpt, image, featured, tags, login_required, password, content_template, detail_template, image_caption, lead, publication_date) FROM stdin;
1	Casas en Remate, una excelente oportunidad	casas-en-remate-una-excelente-oportunidad	2	\N	\N	2016-05-30 01:14:08.12942-03	2016-05-30 01:14:08.162123-03	l remate de propiedades es una buena opción a la hora de comprar una casa, pues te traerá, sobre todo, un importante beneficio económico. En ese sentido, considerar la compra de una casa en remate puede ser una buena idea para ahorrar mucho dinero. ¿En qué consiste un remate de propiedad? Existen casas que deben ser vendidas rápidamente por diversas cuestiones, como contribuciones impagas, disputas por herencias, deudas en un Crédito Hipotecario, divorcios o simplemente la necesidad de dinero de manera urgente. Y la mejor manera para hacerlo de manera ágil y absolutamente legal es mediante un remate. Las casas que se rematan pueden ser antiguas o nuevas y en cualquier zona, pues no hay ningún tipo de distinción. ¿Cuáles son los beneficios de comprar una casa en un remate? El beneficio económico encabeza el listado de provechos, pues puedes conseguir una casa o departamento hasta a un 40% menos que el valor real. Esto es más que importante si es que buscas una casa para vivienda familiar e incluso como inversión. El otro beneficio es la rapidez con que puedes obtener tu nueva vivienda, ya que en tan solo una semana puedes ser el nuevo dueño de la propiedad. Es que detrás de un remate hay una necesidad imperiosa de realizar la venta que puede jugar en tu favor. ¿Cómo es el mecanismo del remate? Antes que nada, tienes que saber que es necesario que participes de un remate sólo con el asesoramiento de un abogado, que pueda verificar que el expediente de la propiedad esté en orden. Las subastas son difundidas en los diarios un mes antes de que se realicen, y en ese tiempo puedes comunicarte para que te cuenten detalles del inmueble e incluso ir a visitarlo. Para poder participar del remate deberás adquirir una boleta de garantía, que equivale al mínimo que publica el aviso. Esa boleta es un documento que te entrega el banco, que certifica que cuentas con el valor mínimo para poder participar del proceso, que puede ser que provenga de ahorros o de un Crédito Hipotecario. Cuando comienza el remate, cada participante pone en un sobre su oferta, y la más elevada se convierte en la base de la puja. Ya ves que comprar una casa en un remate es una buena opción para ahorrar y para ser dueño en muy poco tiempo. Solo debes tener el cuidado de asesorarte para que sea una buena experiencia y obtener así tu tan anhelada vivienda. No debes olvidar que la mejor forma de obtener un Crédito Hipotecario a tu medida es Simular sus características, como intereses, plazos y gastos operacionales. En ComparaOnline podrás Simular gratis tu Crédito Hipotecario, para realizar una compra informada e inteligente de tu financiamiento ¡Qué esperas! Ahorra tiempo y dinero con nosotros. - See more at: https://www.comparaonline.cl/blog/finanzas/credito-hipotecario/2014/05/remate-de-propiedades-una-excelente-alternativa-para-comprar-una-casa/#sthash.hFxzhlqX.dpuf	t	t	t	0	0	0	l remate de propiedades es una buena opción a la hora de comprar una casa, pues te traerá, sobre todo, un importante beneficio económico. En ese sentido, considerar la compra de una casa en remate puede ser una buena idea para ahorrar mucho dinero. ¿En qué consiste un remate de...	uploads/zinnia/2016/05/30/remate.jpg	f		f		zinnia/_entry_detail.html	entry_detail.html	cas remate	Lo bueno de comprar una casa en remate	2016-05-30 01:12:39-03
\.


--
-- Data for Name: zinnia_entry_authors; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_entry_authors (id, entry_id, author_id) FROM stdin;
1	1	1
\.


--
-- Name: zinnia_entry_authors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_entry_authors_id_seq', 1, true);


--
-- Data for Name: zinnia_entry_categories; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_entry_categories (id, entry_id, category_id) FROM stdin;
\.


--
-- Name: zinnia_entry_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_entry_categories_id_seq', 1, false);


--
-- Name: zinnia_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_entry_id_seq', 1, true);


--
-- Data for Name: zinnia_entry_related; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_entry_related (id, from_entry_id, to_entry_id) FROM stdin;
\.


--
-- Name: zinnia_entry_related_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_entry_related_id_seq', 1, false);


--
-- Data for Name: zinnia_entry_sites; Type: TABLE DATA; Schema: public; Owner: mjd_db
--

COPY zinnia_entry_sites (id, entry_id, site_id) FROM stdin;
1	1	1
\.


--
-- Name: zinnia_entry_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: mjd_db
--

SELECT pg_catalog.setval('zinnia_entry_sites_id_seq', 1, true);


SET search_path = tiger, pg_catalog;

--
-- Data for Name: geocode_settings; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY geocode_settings  FROM stdin;
\.


--
-- Data for Name: pagc_gaz; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_gaz  FROM stdin;
\.


--
-- Data for Name: pagc_lex; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_lex  FROM stdin;
\.


--
-- Data for Name: pagc_rules; Type: TABLE DATA; Schema: tiger; Owner: postgres
--

COPY pagc_rules  FROM stdin;
\.


SET search_path = topology, pg_catalog;

--
-- Data for Name: topology; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology  FROM stdin;
\.


--
-- Data for Name: layer; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY layer  FROM stdin;
\.


SET search_path = public, pg_catalog;

--
-- Name: admin_tools_dashboard_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY admin_tools_dashboard_preferences
    ADD CONSTRAINT admin_tools_dashboard_preferences_pkey PRIMARY KEY (id);


--
-- Name: admin_tools_dashboard_preferences_user_id_74da8e56_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY admin_tools_dashboard_preferences
    ADD CONSTRAINT admin_tools_dashboard_preferences_user_id_74da8e56_uniq UNIQUE (user_id, dashboard_id);


--
-- Name: admin_tools_menu_bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY admin_tools_menu_bookmark
    ADD CONSTRAINT admin_tools_menu_bookmark_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contactform_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY contactform_contact
    ADD CONSTRAINT contactform_contact_pkey PRIMARY KEY (id);


--
-- Name: contactform_contact_servicio_contact_id_718064e7_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contact_servicio_contact_id_718064e7_uniq UNIQUE (contact_id, pagina_id);


--
-- Name: contactform_contact_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contact_servicio_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_comment_flags_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_pkey PRIMARY KEY (id);


--
-- Name: django_comment_flags_user_id_537f77a7_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_user_id_537f77a7_uniq UNIQUE (user_id, comment_id, flag);


--
-- Name: django_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source_storage_hash_481ce32d_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_storage_hash_481ce32d_uniq UNIQUE (storage_hash, name);


--
-- Name: easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_fb375270_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_storage_hash_fb375270_uniq UNIQUE (storage_hash, name, source_id);


--
-- Name: easy_thumbnails_thumbnaildimensions_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions_thumbnail_id_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_thumbnail_id_key UNIQUE (thumbnail_id);


--
-- Name: guardian_groupobjectpermission_group_id_3f189f7c_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermission_group_id_3f189f7c_uniq UNIQUE (group_id, permission_id, object_pk);


--
-- Name: guardian_groupobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: guardian_userobjectpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_pkey PRIMARY KEY (id);


--
-- Name: guardian_userobjectpermission_user_id_b0b3d2fc_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_user_id_b0b3d2fc_uniq UNIQUE (user_id, permission_id, object_pk);


--
-- Name: localizacion_comuna_nombre_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY localizacion_comuna
    ADD CONSTRAINT localizacion_comuna_nombre_key UNIQUE (nombre);


--
-- Name: localizacion_comuna_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY localizacion_comuna
    ADD CONSTRAINT localizacion_comuna_pkey PRIMARY KEY (id);


--
-- Name: localizacion_localizacion_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY localizacion_localizacion
    ADD CONSTRAINT localizacion_localizacion_pkey PRIMARY KEY (id);


--
-- Name: localizacion_region_numero_decimal_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY localizacion_region
    ADD CONSTRAINT localizacion_region_numero_decimal_key UNIQUE (numero_decimal);


--
-- Name: localizacion_region_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY localizacion_region
    ADD CONSTRAINT localizacion_region_pkey PRIMARY KEY (id);


--
-- Name: pagina_clasificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY pagina_clasificacion
    ADD CONSTRAINT pagina_clasificacion_pkey PRIMARY KEY (id);


--
-- Name: pagina_clasificacion_tipo_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY pagina_clasificacion
    ADD CONSTRAINT pagina_clasificacion_tipo_key UNIQUE (tipo);


--
-- Name: pagina_pagina_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pagina_pkey PRIMARY KEY (id);


--
-- Name: pagina_pagina_slug_titulo_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pagina_slug_titulo_key UNIQUE (slug_titulo);


--
-- Name: principal_busqueda_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_busqueda
    ADD CONSTRAINT principal_busqueda_pkey PRIMARY KEY (id);


--
-- Name: principal_busqueda_set_resultados_busqueda_id_5f05c45c_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_busqueda_set_resultados
    ADD CONSTRAINT principal_busqueda_set_resultados_busqueda_id_5f05c45c_uniq UNIQUE (busqueda_id, propiedad_id);


--
-- Name: principal_busqueda_set_resultados_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_busqueda_set_resultados
    ADD CONSTRAINT principal_busqueda_set_resultados_pkey PRIMARY KEY (id);


--
-- Name: principal_busqueda_ubicacion_gm2m_src_id_53316320_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_busqueda_ubicacion
    ADD CONSTRAINT principal_busqueda_ubicacion_gm2m_src_id_53316320_uniq UNIQUE (gm2m_src_id, gm2m_ct_id, gm2m_pk);


--
-- Name: principal_busqueda_ubicacion_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_busqueda_ubicacion
    ADD CONSTRAINT principal_busqueda_ubicacion_pkey PRIMARY KEY (id);


--
-- Name: principal_configuracionhome_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_configuracionhome
    ADD CONSTRAINT principal_configuracionhome_pkey PRIMARY KEY (id);


--
-- Name: principal_homeimage_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_homeimage
    ADD CONSTRAINT principal_homeimage_pkey PRIMARY KEY (id);


--
-- Name: principal_sethomeimage_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY principal_sethomeimage
    ADD CONSTRAINT principal_sethomeimage_pkey PRIMARY KEY (id);


--
-- Name: propiedad_arquitecto_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY propiedad_arquitecto
    ADD CONSTRAINT propiedad_arquitecto_pkey PRIMARY KEY (id);


--
-- Name: propiedad_imagenpropiedad_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY propiedad_imagenpropiedad
    ADD CONSTRAINT propiedad_imagenpropiedad_pkey PRIMARY KEY (id);


--
-- Name: propiedad_preciopropiedad_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY propiedad_preciopropiedad
    ADD CONSTRAINT propiedad_preciopropiedad_pkey PRIMARY KEY (id);


--
-- Name: propiedad_propiedad_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propiedad_propiedad_pkey PRIMARY KEY (id);


--
-- Name: propiedad_propietario_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY propiedad_propietario
    ADD CONSTRAINT propiedad_propietario_pkey PRIMARY KEY (id);


--
-- Name: tagging_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY tagging_tag
    ADD CONSTRAINT tagging_tag_name_key UNIQUE (name);


--
-- Name: tagging_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY tagging_tag
    ADD CONSTRAINT tagging_tag_pkey PRIMARY KEY (id);


--
-- Name: tagging_taggeditem_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY tagging_taggeditem
    ADD CONSTRAINT tagging_taggeditem_pkey PRIMARY KEY (id);


--
-- Name: tagging_taggeditem_tag_id_3d53f09d_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY tagging_taggeditem
    ADD CONSTRAINT tagging_taggeditem_tag_id_3d53f09d_uniq UNIQUE (tag_id, content_type_id, object_id);


--
-- Name: usuario_profileusermjd_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY usuario_profileusermjd
    ADD CONSTRAINT usuario_profileusermjd_pkey PRIMARY KEY (id);


--
-- Name: usuario_profileusermjd_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY usuario_profileusermjd
    ADD CONSTRAINT usuario_profileusermjd_user_id_key UNIQUE (user_id);


--
-- Name: usuario_usuariomjd_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY usuario_usuariomjd
    ADD CONSTRAINT usuario_usuariomjd_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: zinnia_category_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_category
    ADD CONSTRAINT zinnia_category_pkey PRIMARY KEY (id);


--
-- Name: zinnia_category_slug_key; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_category
    ADD CONSTRAINT zinnia_category_slug_key UNIQUE (slug);


--
-- Name: zinnia_entry_authors_entry_id_15a74736_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_authors
    ADD CONSTRAINT zinnia_entry_authors_entry_id_15a74736_uniq UNIQUE (entry_id, author_id);


--
-- Name: zinnia_entry_authors_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_authors
    ADD CONSTRAINT zinnia_entry_authors_pkey PRIMARY KEY (id);


--
-- Name: zinnia_entry_categories_entry_id_06e8a1ea_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_categories
    ADD CONSTRAINT zinnia_entry_categories_entry_id_06e8a1ea_uniq UNIQUE (entry_id, category_id);


--
-- Name: zinnia_entry_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_categories
    ADD CONSTRAINT zinnia_entry_categories_pkey PRIMARY KEY (id);


--
-- Name: zinnia_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry
    ADD CONSTRAINT zinnia_entry_pkey PRIMARY KEY (id);


--
-- Name: zinnia_entry_related_from_entry_id_f5c132fc_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_related
    ADD CONSTRAINT zinnia_entry_related_from_entry_id_f5c132fc_uniq UNIQUE (from_entry_id, to_entry_id);


--
-- Name: zinnia_entry_related_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_related
    ADD CONSTRAINT zinnia_entry_related_pkey PRIMARY KEY (id);


--
-- Name: zinnia_entry_sites_entry_id_f65c3bb6_uniq; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_sites
    ADD CONSTRAINT zinnia_entry_sites_entry_id_f65c3bb6_uniq UNIQUE (entry_id, site_id);


--
-- Name: zinnia_entry_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: mjd_db; Tablespace: 
--

ALTER TABLE ONLY zinnia_entry_sites
    ADD CONSTRAINT zinnia_entry_sites_pkey PRIMARY KEY (id);


--
-- Name: admin_tools_dashboard_preferences_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX admin_tools_dashboard_preferences_e8701ad4 ON admin_tools_dashboard_preferences USING btree (user_id);


--
-- Name: admin_tools_menu_bookmark_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX admin_tools_menu_bookmark_e8701ad4 ON admin_tools_menu_bookmark USING btree (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: contactform_contact_servicio_02f97d6d; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX contactform_contact_servicio_02f97d6d ON contactform_contact_servicio USING btree (pagina_id);


--
-- Name: contactform_contact_servicio_6d82f13d; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX contactform_contact_servicio_6d82f13d ON contactform_contact_servicio USING btree (contact_id);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_comment_flags_327a6c43; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comment_flags_327a6c43 ON django_comment_flags USING btree (flag);


--
-- Name: django_comment_flags_69b97d17; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comment_flags_69b97d17 ON django_comment_flags USING btree (comment_id);


--
-- Name: django_comment_flags_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comment_flags_e8701ad4 ON django_comment_flags USING btree (user_id);


--
-- Name: django_comment_flags_flag_8b141fcb_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comment_flags_flag_8b141fcb_like ON django_comment_flags USING btree (flag varchar_pattern_ops);


--
-- Name: django_comments_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comments_417f1b1c ON django_comments USING btree (content_type_id);


--
-- Name: django_comments_9365d6e7; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comments_9365d6e7 ON django_comments USING btree (site_id);


--
-- Name: django_comments_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comments_e8701ad4 ON django_comments USING btree (user_id);


--
-- Name: django_comments_submit_date_514ed2d9_uniq; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_comments_submit_date_514ed2d9_uniq ON django_comments USING btree (submit_date);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX django_site_domain_a2e37b91_like ON django_site USING btree (domain varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_b068931c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_b068931c ON easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_b454e115; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_b454e115 ON easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_source_name_5fe0edc6_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6_like ON easy_thumbnails_source USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9_like ON easy_thumbnails_source USING btree (storage_hash varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_0afd9202; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_0afd9202 ON easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_b068931c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_b068931c ON easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_b454e115; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_b454e115 ON easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31_like ON easy_thumbnails_thumbnail USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49_like ON easy_thumbnails_thumbnail USING btree (storage_hash varchar_pattern_ops);


--
-- Name: guardian_groupobjectpermission_0e939a4f; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_groupobjectpermission_0e939a4f ON guardian_groupobjectpermission USING btree (group_id);


--
-- Name: guardian_groupobjectpermission_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_groupobjectpermission_417f1b1c ON guardian_groupobjectpermission USING btree (content_type_id);


--
-- Name: guardian_groupobjectpermission_8373b171; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_groupobjectpermission_8373b171 ON guardian_groupobjectpermission USING btree (permission_id);


--
-- Name: guardian_userobjectpermission_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_userobjectpermission_417f1b1c ON guardian_userobjectpermission USING btree (content_type_id);


--
-- Name: guardian_userobjectpermission_8373b171; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_userobjectpermission_8373b171 ON guardian_userobjectpermission USING btree (permission_id);


--
-- Name: guardian_userobjectpermission_e8701ad4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX guardian_userobjectpermission_e8701ad4 ON guardian_userobjectpermission USING btree (user_id);


--
-- Name: localizacion_comuna_0f442f96; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_comuna_0f442f96 ON localizacion_comuna USING btree (region_id);


--
-- Name: localizacion_comuna_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_comuna_417f1b1c ON localizacion_comuna USING btree (content_type_id);


--
-- Name: localizacion_comuna_nombre_fde4ba89_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_comuna_nombre_fde4ba89_like ON localizacion_comuna USING btree (nombre varchar_pattern_ops);


--
-- Name: localizacion_localizacion_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_localizacion_417f1b1c ON localizacion_localizacion USING btree (content_type_id);


--
-- Name: localizacion_localizacion_5f006471; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_localizacion_5f006471 ON localizacion_localizacion USING btree (comuna_id);


--
-- Name: localizacion_localizacion_geo_point_id; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_localizacion_geo_point_id ON localizacion_localizacion USING gist (geo_point);


--
-- Name: localizacion_region_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_region_417f1b1c ON localizacion_region USING btree (content_type_id);


--
-- Name: localizacion_region_numero_decimal_0c32aeeb_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX localizacion_region_numero_decimal_0c32aeeb_like ON localizacion_region USING btree (numero_decimal varchar_pattern_ops);


--
-- Name: pagina_clasificacion_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_clasificacion_417f1b1c ON pagina_clasificacion USING btree (content_type_id);


--
-- Name: pagina_clasificacion_5fe7cbf2; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_clasificacion_5fe7cbf2 ON pagina_clasificacion USING btree (slug_tipo);


--
-- Name: pagina_clasificacion_slug_tipo_b1102d5f_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_clasificacion_slug_tipo_b1102d5f_like ON pagina_clasificacion USING btree (slug_tipo varchar_pattern_ops);


--
-- Name: pagina_clasificacion_tipo_8364cf48_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_clasificacion_tipo_8364cf48_like ON pagina_clasificacion USING btree (tipo varchar_pattern_ops);


--
-- Name: pagina_pagina_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_pagina_417f1b1c ON pagina_pagina USING btree (content_type_id);


--
-- Name: pagina_pagina_e2b7bb94; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_pagina_e2b7bb94 ON pagina_pagina USING btree (clasificacion_id);


--
-- Name: pagina_pagina_slug_titulo_ff1b2c47_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX pagina_pagina_slug_titulo_ff1b2c47_like ON pagina_pagina USING btree (slug_titulo varchar_pattern_ops);


--
-- Name: principal_busqueda_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_busqueda_417f1b1c ON principal_busqueda USING btree (content_type_id);


--
-- Name: principal_busqueda_set_resultados_069c44a6; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_busqueda_set_resultados_069c44a6 ON principal_busqueda_set_resultados USING btree (busqueda_id);


--
-- Name: principal_busqueda_set_resultados_3f43f707; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_busqueda_set_resultados_3f43f707 ON principal_busqueda_set_resultados USING btree (propiedad_id);


--
-- Name: principal_busqueda_ubicacion_b66ec49d; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_busqueda_ubicacion_b66ec49d ON principal_busqueda_ubicacion USING btree (gm2m_src_id);


--
-- Name: principal_busqueda_ubicacion_fbeaf1ea; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_busqueda_ubicacion_fbeaf1ea ON principal_busqueda_ubicacion USING btree (gm2m_ct_id);


--
-- Name: principal_configuracionhome_149f5074; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_configuracionhome_149f5074 ON principal_configuracionhome USING btree (slug_nombre);


--
-- Name: principal_configuracionhome_40ace839; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_configuracionhome_40ace839 ON principal_configuracionhome USING btree (set_id);


--
-- Name: principal_homeimage_40ace839; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX principal_homeimage_40ace839 ON principal_homeimage USING btree (set_id);


--
-- Name: propiedad_arquitecto_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_arquitecto_417f1b1c ON propiedad_arquitecto USING btree (content_type_id);


--
-- Name: propiedad_imagenpropiedad_3f43f707; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_imagenpropiedad_3f43f707 ON propiedad_imagenpropiedad USING btree (propiedad_id);


--
-- Name: propiedad_preciopropiedad_3f43f707; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_preciopropiedad_3f43f707 ON propiedad_preciopropiedad USING btree (propiedad_id);


--
-- Name: propiedad_propiedad_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propiedad_417f1b1c ON propiedad_propiedad USING btree (content_type_id);


--
-- Name: propiedad_propiedad_6f44f644; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propiedad_6f44f644 ON propiedad_propiedad USING btree (localizacion_id);


--
-- Name: propiedad_propiedad_abfe0f96; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propiedad_abfe0f96 ON propiedad_propiedad USING btree (usuario_id);


--
-- Name: propiedad_propiedad_af6180d4; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propiedad_af6180d4 ON propiedad_propiedad USING btree (arquitecto_id);


--
-- Name: propiedad_propiedad_e24036a9; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propiedad_e24036a9 ON propiedad_propiedad USING btree (propietario_id);


--
-- Name: propiedad_propietario_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX propiedad_propietario_417f1b1c ON propiedad_propietario USING btree (content_type_id);


--
-- Name: tagging_tag_name_a1527681_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX tagging_tag_name_a1527681_like ON tagging_tag USING btree (name varchar_pattern_ops);


--
-- Name: tagging_taggeditem_417f1b1c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX tagging_taggeditem_417f1b1c ON tagging_taggeditem USING btree (content_type_id);


--
-- Name: tagging_taggeditem_76f094bc; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX tagging_taggeditem_76f094bc ON tagging_taggeditem USING btree (tag_id);


--
-- Name: tagging_taggeditem_af31437c; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX tagging_taggeditem_af31437c ON tagging_taggeditem USING btree (object_id);


--
-- Name: zinnia_category_3cfbd988; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_3cfbd988 ON zinnia_category USING btree (rght);


--
-- Name: zinnia_category_656442a0; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_656442a0 ON zinnia_category USING btree (tree_id);


--
-- Name: zinnia_category_6be37982; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_6be37982 ON zinnia_category USING btree (parent_id);


--
-- Name: zinnia_category_c9e9a848; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_c9e9a848 ON zinnia_category USING btree (level);


--
-- Name: zinnia_category_caf7cc51; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_caf7cc51 ON zinnia_category USING btree (lft);


--
-- Name: zinnia_category_slug_deaaf526_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_category_slug_deaaf526_like ON zinnia_category USING btree (slug varchar_pattern_ops);


--
-- Name: zinnia_entry_2dbcba41; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_2dbcba41 ON zinnia_entry USING btree (slug);


--
-- Name: zinnia_entry_6dc6f11d; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_6dc6f11d ON zinnia_entry USING btree (start_publication);


--
-- Name: zinnia_entry_93b83098; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_93b83098 ON zinnia_entry USING btree (publication_date);


--
-- Name: zinnia_entry_9acb4454; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_9acb4454 ON zinnia_entry USING btree (status);


--
-- Name: zinnia_entry_authors_4f331e2f; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_authors_4f331e2f ON zinnia_entry_authors USING btree (author_id);


--
-- Name: zinnia_entry_authors_b64a62ea; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_authors_b64a62ea ON zinnia_entry_authors USING btree (entry_id);


--
-- Name: zinnia_entry_b803a79a; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_b803a79a ON zinnia_entry USING btree (end_publication);


--
-- Name: zinnia_entry_categories_b583a629; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_categories_b583a629 ON zinnia_entry_categories USING btree (category_id);


--
-- Name: zinnia_entry_categories_b64a62ea; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_categories_b64a62ea ON zinnia_entry_categories USING btree (entry_id);


--
-- Name: zinnia_entry_related_76fb690f; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_related_76fb690f ON zinnia_entry_related USING btree (to_entry_id);


--
-- Name: zinnia_entry_related_da15a9a7; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_related_da15a9a7 ON zinnia_entry_related USING btree (from_entry_id);


--
-- Name: zinnia_entry_sites_9365d6e7; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_sites_9365d6e7 ON zinnia_entry_sites USING btree (site_id);


--
-- Name: zinnia_entry_sites_b64a62ea; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_sites_b64a62ea ON zinnia_entry_sites USING btree (entry_id);


--
-- Name: zinnia_entry_slug_55a4551e_like; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_slug_55a4551e_like ON zinnia_entry USING btree (slug varchar_pattern_ops);


--
-- Name: zinnia_entry_slug_8de07f28_idx; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_slug_8de07f28_idx ON zinnia_entry USING btree (slug, publication_date);


--
-- Name: zinnia_entry_status_b9d97cbc_idx; Type: INDEX; Schema: public; Owner: mjd_db; Tablespace: 
--

CREATE INDEX zinnia_entry_status_b9d97cbc_idx ON zinnia_entry USING btree (status, publication_date, start_publication, end_publication);


--
-- Name: admin_tools_dashboard_preferen_user_id_8f768e6c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY admin_tools_dashboard_preferences
    ADD CONSTRAINT admin_tools_dashboard_preferen_user_id_8f768e6c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: admin_tools_menu_bookmark_user_id_0382e410_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY admin_tools_menu_bookmark
    ADD CONSTRAINT admin_tools_menu_bookmark_user_id_0382e410_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactform_conta_contact_id_ae984b54_fk_contactform_contact_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_conta_contact_id_ae984b54_fk_contactform_contact_id FOREIGN KEY (contact_id) REFERENCES contactform_contact(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: contactform_contact_serv_pagina_id_647a8de2_fk_pagina_pagina_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY contactform_contact_servicio
    ADD CONSTRAINT contactform_contact_serv_pagina_id_647a8de2_fk_pagina_pagina_id FOREIGN KEY (pagina_id) REFERENCES pagina_pagina(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_comme_content_type_id_c4afe962_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comme_content_type_id_c4afe962_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_comment_flags_comment_id_d8054933_fk_django_comments_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_comment_id_d8054933_fk_django_comments_id FOREIGN KEY (comment_id) REFERENCES django_comments(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_comment_flags_user_id_f3f81f0a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_user_id_f3f81f0a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_comments_site_id_9dcf666e_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_site_id_9dcf666e_fk_django_site_id FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_comments_user_id_a0a440a1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_user_id_a0a440a1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thum_thumbnail_id_c3a0c549_fk_easy_thumbnails_thumbnail_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thum_thumbnail_id_c3a0c549_fk_easy_thumbnails_thumbnail_id FOREIGN KEY (thumbnail_id) REFERENCES easy_thumbnails_thumbnail(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_source_id_5b57bc77_fk_easy_thumbnails_source_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_source_id_5b57bc77_fk_easy_thumbnails_source_id FOREIGN KEY (source_id) REFERENCES easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_gro_content_type_id_7ade36b8_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_groupobjectpermission
    ADD CONSTRAINT guardian_gro_content_type_id_7ade36b8_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobje_permission_id_36572738_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobje_permission_id_36572738_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_groupobjectpermissi_group_id_4bbbfb62_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_groupobjectpermission
    ADD CONSTRAINT guardian_groupobjectpermissi_group_id_4bbbfb62_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_use_content_type_id_2e892405_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_userobjectpermission
    ADD CONSTRAINT guardian_use_content_type_id_2e892405_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjec_permission_id_71807bfc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjec_permission_id_71807bfc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY guardian_userobjectpermission
    ADD CONSTRAINT guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: localizacion_comun_region_id_50dfd877_fk_localizacion_region_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_comuna
    ADD CONSTRAINT localizacion_comun_region_id_50dfd877_fk_localizacion_region_id FOREIGN KEY (region_id) REFERENCES localizacion_region(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: localizacion_content_type_id_2a91e879_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_comuna
    ADD CONSTRAINT localizacion_content_type_id_2a91e879_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: localizacion_content_type_id_ad57cd28_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_region
    ADD CONSTRAINT localizacion_content_type_id_ad57cd28_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: localizacion_content_type_id_c32ebb89_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_localizacion
    ADD CONSTRAINT localizacion_content_type_id_c32ebb89_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: localizacion_local_comuna_id_35a30f37_fk_localizacion_comuna_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY localizacion_localizacion
    ADD CONSTRAINT localizacion_local_comuna_id_35a30f37_fk_localizacion_comuna_id FOREIGN KEY (comuna_id) REFERENCES localizacion_comuna(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pagina_clasi_content_type_id_dab8707d_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY pagina_clasificacion
    ADD CONSTRAINT pagina_clasi_content_type_id_dab8707d_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pagina_pag_clasificacion_id_8daed73f_fk_pagina_clasificacion_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pag_clasificacion_id_8daed73f_fk_pagina_clasificacion_id FOREIGN KEY (clasificacion_id) REFERENCES pagina_clasificacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pagina_pagin_content_type_id_aefe2982_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY pagina_pagina
    ADD CONSTRAINT pagina_pagin_content_type_id_aefe2982_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_bu_content_type_id_84173e1c_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda
    ADD CONSTRAINT principal_bu_content_type_id_84173e1c_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_busqu_propiedad_id_2f39b4b3_fk_propiedad_propiedad_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_set_resultados
    ADD CONSTRAINT principal_busqu_propiedad_id_2f39b4b3_fk_propiedad_propiedad_id FOREIGN KEY (propiedad_id) REFERENCES propiedad_propiedad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_busqued_busqueda_id_717d3970_fk_principal_busqueda_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_set_resultados
    ADD CONSTRAINT principal_busqued_busqueda_id_717d3970_fk_principal_busqueda_id FOREIGN KEY (busqueda_id) REFERENCES principal_busqueda(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_busqued_gm2m_ct_id_ab757cb4_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_ubicacion
    ADD CONSTRAINT principal_busqued_gm2m_ct_id_ab757cb4_fk_django_content_type_id FOREIGN KEY (gm2m_ct_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_busqued_gm2m_src_id_149cf06b_fk_principal_busqueda_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_busqueda_ubicacion
    ADD CONSTRAINT principal_busqued_gm2m_src_id_149cf06b_fk_principal_busqueda_id FOREIGN KEY (gm2m_src_id) REFERENCES principal_busqueda(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_configur_set_id_52addc42_fk_principal_sethomeimage_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_configuracionhome
    ADD CONSTRAINT principal_configur_set_id_52addc42_fk_principal_sethomeimage_id FOREIGN KEY (set_id) REFERENCES principal_sethomeimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: principal_homeimag_set_id_153d13e9_fk_principal_sethomeimage_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY principal_homeimage
    ADD CONSTRAINT principal_homeimag_set_id_153d13e9_fk_principal_sethomeimage_id FOREIGN KEY (set_id) REFERENCES principal_sethomeimage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propie_localizacion_id_266bf040_fk_localizacion_localizacion_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propie_localizacion_id_266bf040_fk_localizacion_localizacion_id FOREIGN KEY (localizacion_id) REFERENCES localizacion_localizacion(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_ar_content_type_id_96ab0aee_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_arquitecto
    ADD CONSTRAINT propiedad_ar_content_type_id_96ab0aee_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_image_propiedad_id_b425b4be_fk_propiedad_propiedad_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_imagenpropiedad
    ADD CONSTRAINT propiedad_image_propiedad_id_b425b4be_fk_propiedad_propiedad_id FOREIGN KEY (propiedad_id) REFERENCES propiedad_propiedad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_p_propietario_id_ccdb016c_fk_propiedad_propietario_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propiedad_p_propietario_id_ccdb016c_fk_propiedad_propietario_id FOREIGN KEY (propietario_id) REFERENCES propiedad_propietario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_pr_content_type_id_0cec8175_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propiedad_pr_content_type_id_0cec8175_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_pr_content_type_id_d3247cc6_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propietario
    ADD CONSTRAINT propiedad_pr_content_type_id_d3247cc6_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_preci_propiedad_id_c5534bf4_fk_propiedad_propiedad_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_preciopropiedad
    ADD CONSTRAINT propiedad_preci_propiedad_id_c5534bf4_fk_propiedad_propiedad_id FOREIGN KEY (propiedad_id) REFERENCES propiedad_propiedad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_pro_arquitecto_id_220e2c8b_fk_propiedad_arquitecto_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propiedad_pro_arquitecto_id_220e2c8b_fk_propiedad_arquitecto_id FOREIGN KEY (arquitecto_id) REFERENCES propiedad_arquitecto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: propiedad_propiedad_usuario_id_ede550c5_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY propiedad_propiedad
    ADD CONSTRAINT propiedad_propiedad_usuario_id_ede550c5_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tagging_tagg_content_type_id_ede1c265_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY tagging_taggeditem
    ADD CONSTRAINT tagging_tagg_content_type_id_ede1c265_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: tagging_taggeditem_tag_id_f008ca79_fk_tagging_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY tagging_taggeditem
    ADD CONSTRAINT tagging_taggeditem_tag_id_f008ca79_fk_tagging_tag_id FOREIGN KEY (tag_id) REFERENCES tagging_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuario_prof_user_id_74666e96_fk_usuario_usuariomjd_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY usuario_profileusermjd
    ADD CONSTRAINT usuario_prof_user_id_74666e96_fk_usuario_usuariomjd_user_ptr_id FOREIGN KEY (user_id) REFERENCES usuario_usuariomjd(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: usuario_usuariomjd_user_ptr_id_24a3eadc_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY usuario_usuariomjd
    ADD CONSTRAINT usuario_usuariomjd_user_ptr_id_24a3eadc_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_category_parent_id_9957d607_fk_zinnia_category_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_category
    ADD CONSTRAINT zinnia_category_parent_id_9957d607_fk_zinnia_category_id FOREIGN KEY (parent_id) REFERENCES zinnia_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_authors_author_id_c95ec445_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_authors
    ADD CONSTRAINT zinnia_entry_authors_author_id_c95ec445_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_authors_entry_id_7e8cdb2d_fk_zinnia_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_authors
    ADD CONSTRAINT zinnia_entry_authors_entry_id_7e8cdb2d_fk_zinnia_entry_id FOREIGN KEY (entry_id) REFERENCES zinnia_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_categor_category_id_1af4a3b8_fk_zinnia_category_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_categories
    ADD CONSTRAINT zinnia_entry_categor_category_id_1af4a3b8_fk_zinnia_category_id FOREIGN KEY (category_id) REFERENCES zinnia_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_categories_entry_id_2245c94e_fk_zinnia_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_categories
    ADD CONSTRAINT zinnia_entry_categories_entry_id_2245c94e_fk_zinnia_entry_id FOREIGN KEY (entry_id) REFERENCES zinnia_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_related_from_entry_id_ef0e020e_fk_zinnia_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_related
    ADD CONSTRAINT zinnia_entry_related_from_entry_id_ef0e020e_fk_zinnia_entry_id FOREIGN KEY (from_entry_id) REFERENCES zinnia_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_related_to_entry_id_f3ace6a2_fk_zinnia_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_related
    ADD CONSTRAINT zinnia_entry_related_to_entry_id_f3ace6a2_fk_zinnia_entry_id FOREIGN KEY (to_entry_id) REFERENCES zinnia_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_sites_entry_id_b62220dc_fk_zinnia_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_sites
    ADD CONSTRAINT zinnia_entry_sites_entry_id_b62220dc_fk_zinnia_entry_id FOREIGN KEY (entry_id) REFERENCES zinnia_entry(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: zinnia_entry_sites_site_id_3b37fedc_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: mjd_db
--

ALTER TABLE ONLY zinnia_entry_sites
    ADD CONSTRAINT zinnia_entry_sites_site_id_3b37fedc_fk_django_site_id FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

