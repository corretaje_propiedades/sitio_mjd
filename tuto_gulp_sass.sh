Tutorial gestor de Tareas GULP
==============================

Este tutorial es un paso a paso para instalar, configurar y correr gulp
uno de los gestores de multitareas para el desarrollo web.

LINK:http://gulpjs.com/

Primero, es necesario tener instalado NODE JS

LINK: https://nodejs.org/dist/v6.2.1/node-v6.2.1-linux-x64.tar.xz

Luego, seguir las instrucciones de instalacion del proyecto en github:

LINK: https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md


Instalar en forma global:
npm install --global gulp-cli

Instalar en proyecto:
$ npm install --save-dev gulp

Además instalar:

npm install gulp bower
npm install gulp-sass gulp-minify-css gulp-rename gulp-gzip gulp-livereload
bower install foundation


Crear un archivo [gulpfile.js] que contenga los requerimientos y dependencias del proyecto.

Esto es, cada operación como actualización de browser, compilacion de sass, etc.

<CODE>
var gulp = require('gulp');
//var path = require('path');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var minifycss = require('gulp-clean-css');
var rename = require('gulp-rename');
var gzip = require('gulp-gzip');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;

var gzip_options = {
    threshold: '1kb',
    gzipOptions: {
        level: 9
    }
};
var sassDir = './mjd/apps/'
/* Compile Our Sass */
gulp.task('sass', function() {
    return gulp.src(sassDir+'**/static/scss/*.scss')
        .pipe(sass())
       // .pipe(parsePath())
        .pipe(
            rename(function(path){
                path.dirname += "/../css";
            })
        )
        .pipe(gulp.dest(sassDir))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(sassDir))
        .pipe(gzip(gzip_options))
        .pipe(gulp.dest(sassDir))
        //.pipe(livereload())
});

/* Watch Files For Changes */
gulp.task('watch', function() {
    //livereload.listen(10000);
    gulp.watch('**/static/scss/*.scss', ['sass']);
    /* Trigger a live reload on any Django template changes */
    browserSync.init({
        notify: false,
        proxy: "localhost:8000"
    });
    gulp.watch(['./**/*.{scss,css,html,py,js}'], reload);

});


gulp.task('default', ['sass'])

</CODE>

Correr gulp:

gulp
