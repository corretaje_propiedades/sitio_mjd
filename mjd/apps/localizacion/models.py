from django.contrib.gis.db import models
# Create your models here.
from django_countries.fields import CountryField
#database regiones, provincias y comunas Chile
#https://github.com/knxroot/bdcut-cl
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
#https://docs.djangoproject.com/es/1.9/ref/contrib/contenttypes/
from django.urls import reverse
from .geo_methods import geolocale, point_field
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point

class Region(models.Model):
    """
   :model:`localizacion.Region` es el modelo que registra la unidad geográfica inmediatamente menor al país Chile, consite en un territorio que contiene territorialmente Provincias y Comunas, zonas urbanas y rurales.
   Contiene cuatro campos, a los que se les debe ingresar información en solo nombre y número decimal, ya que los otros se autocompletan y por defecto el país es Chile
    """
    nombre = models.CharField(max_length=200)
    numero_decimal = models.CharField(max_length=100, unique = True)
    numero_romano = models.CharField(max_length=80, blank=True)
    pais = CountryField(default="CL")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def nombre_region(self):
        return self.nombre

    class Meta:
        app_label= 'localizacion'
        verbose_name='Region'
        verbose_name_plural = 'Regiones'
        ordering = ("numero_decimal", )

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        print(self.pais)
        roman=['RM','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV']
        print("<"+self.numero_decimal+">")
        if self.numero_decimal.isdigit():
            self.numero_romano = roman[int(self.numero_decimal)]
        elif self.numero_decimal == "RM" or self.numero_decimal == "13":
            self.numero_romano = roman[0]
        print(self.numero_romano)
        super(Region,self).save(*args, **kwargs)

class Comuna(models.Model):
    """
    :model:`localizacion.Comuna` contiene el nombre de la Comuna y referencia a la :model:`localizacion.Region`
    """
    nombre = models.CharField(max_length = 150, blank=False, unique = True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def nombre_comuna(self):
        return self.nombre

    class Meta:
        app_label= 'localizacion'
        verbose_name='Comuna'
        verbose_name_plural = 'Comunas'

    def __str__(self):
        return self.nombre

class Localizacion(models.Model):
    """
    :model:`localizacion.Localizacion` contiene las ubicaciones o direcciones particulares, referenciadas a la comuna y al punto geográfico.
    """
    ubicacion = models.CharField(max_length=200,help_text="Nombre de calle", verbose_name='Ubicación')
    numero = models.IntegerField(verbose_name='Número')
    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    geo_point = models.PointField(blank = True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def direccion(self):
        return self.ubicacion+" "+str(self.numero)

    class Meta:
        app_label= 'localizacion'
        verbose_name='Localización'
        verbose_name_plural = 'Localizaciones'

    def __str__(self):
        return self.ubicacion+" "+str(self.numero)+", "+self.comuna.nombre

    #Definir metodo save para generar el punto geográfico
    #def save(self):

    def get_absolute_url(self):
        return reverse('ubicacion', kwargs={'pk':self.pk})

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.geo_point == None:
            lat_lon = geolocale(self.ubicacion+" "+str(self.numero)+", "+self.comuna.nombre+", CL")
            self.geo_point = point_field(lat_lon)
        super(Localizacion,self).save(force_insert=False, force_update=False, using=None,
             update_fields=None)
