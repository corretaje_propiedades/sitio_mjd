import geocoder

from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point

def geolocale(direccion):
    geocode = geocoder.google(direccion+', CL')
    lat_lon = geocode.latlng
    return lat_lon

def point_field(latlon):
    latlon.reverse()
    geo_point = GEOSGeometry( Point(latlon), srid = 4326)
    return geo_point