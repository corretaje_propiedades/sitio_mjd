from django.shortcuts import render
from django.views.generic import ListView, CreateView, TemplateView
from .models import Region, Comuna, Localizacion
from django.contrib.gis.geoip2 import GeoIP2
from IPy import IP
import geocoder
from django.urls import reverse
from django.contrib.auth.decorators import login_required

#Widget autocomplete

from dal import autocomplete


#from queryset_sequence import QuerySetSequence
#http://django-autocomplete-light.readthedocs.org/en/master/gfk.html
#Ref querysets (conjuntos de busquedas): https://github.com/percipient/django-querysetsequence
#https://github.com/yourlabs/django-autocomplete-light/tree/master/test_project

class ComunaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return  Comuna.objects.none()

        qs = Comuna.objects.all()

        if self.q:
            qs = qs.filter( nombre__istartswith= self.q )

        return qs


#Some methods

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin


class RegionListView(ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'list_locale.html'
    model = Region

    def get_context_data(self, **kwargs):
        context = super(RegionListView, self).get_context_data(**kwargs)
        context['vista'] = 'Regiones'
        return context

class ComunaListView(ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'list_locale.html'
    model = Comuna

    def get_context_data(self, **kwargs):
        context = super(ComunaListView, self).get_context_data(**kwargs)
        context['vista'] = 'Comunas'
        return context

class LocalizacionView(TemplateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'view_locale.html'

class LocalizacionListView(ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'list_locale.html'
    model = Localizacion

    def get_context_data(self, **kwargs):
        context = super(LocalizacionListView, self).get_context_data(**kwargs)
        context['vista'] = 'Ubicaciones'
        return context

from .forms import NewLocalizacionForm
from django.http import JsonResponse
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point

from .geo_methods import geolocale, point_field


class NewLocalizacionView(CreateView):
    template_name = 'test_locale.html'
    form_class = NewLocalizacionForm
    success_url='ubicacion'
    #model = Localizacion
    #fields = ['ubicacion', 'comuna']
    g = GeoIP2()
    lat_lon = []
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            direccion = request.GET['direccion']
            print("Es ajax")
            print(direccion)
            lat_lon = geolocale(direccion)
            self.lat_lon = lat_lon
            return JsonResponse({'v_lat':lat_lon[0],'v_lon':lat_lon[1]})
        else:
            return super(NewLocalizacionView, self).get(request, *args, **kwargs)

    def get_initial(self):
        ip=get_client_ip(self.request)
        type=IP(ip).iptype()
        if (type=='PRIVATE'):
            geocode = geocoder.google('Santiago, CL')
            lat_lon = geocode.latlng
        else:
            lat_lon = self.g.lat_lon(ip)
        print(lat_lon)
        self.lat_lon = lat_lon
        return self.initial

    def get_context_data(self, **kwargs):
        print("Vista de busqueda")
        ctx = super(NewLocalizacionView, self).get_context_data(**kwargs)
        ctx['lat'] = self.lat_lon[0]
        ctx['lon'] = self.lat_lon[1]
        return ctx

    def form_valid(self, form):
        """
        Si el formulario es válido, entonces se guarda asociado al modelo.
        """

        #si no hay verificacion
        if len(self.lat_lon) == 0:
            self.lat_lon = geolocale(form['direccion']+", "+form['comuna']+", CL")

        print(self.lat_lon)

        self.lat_lon.reverse()

        geo_point = GEOSGeometry( Point(self.lat_lon), srid = 4326)

        form.instance.geo_point = geo_point

        print("Punto geografico en formulario")

        print(form.instance)

        print(form.instance.geo_point)

        self.object = form.save()

        return super(NewLocalizacionView, self).form_valid(form)

