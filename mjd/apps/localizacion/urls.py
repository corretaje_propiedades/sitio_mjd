from django.conf.urls import url

from .views import RegionListView, ComunaListView, LocalizacionListView,NewLocalizacionView, LocalizacionView
from .views import ComunaAutocomplete


app_name='localizacion'
urlpatterns = [
    url(r'^regiones$', RegionListView.as_view(), name='lista-regiones'),
    url(r'^comunas$', ComunaListView.as_view(), name='lista-comunas'),
    url(r'^ubicaciones$', LocalizacionListView.as_view(), name='lista-comunas'),
    url(r'^agregar/ubicacion$', NewLocalizacionView.as_view(), name='nueva-ubicacion'),
    url(r'^ubicacion/(?P<pk>\d+)$', LocalizacionView.as_view(), name='ubicacion'),
    url(r'^comuna-autocomplete/$',ComunaAutocomplete.as_view(), name="comuna-autocomplete"),
]