from django.forms import modelformset_factory
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from .form import ContactForm
# Create your views here.
from django.contrib import messages

def formulario_contacto(request):
	#post = get_object_or_404(ContactForm)
	if request.method == "POST":
		form = ContactForm(request.POST)
		if form.is_valid():
			post = form.save(commit=True)
			post.save()
			messages.success(request, 'Formulario de contacto enviado. ¡Muchas gracias!')
			#return redirect('formulario_enviado')#cambiar a pagina de enviado
	else:
		form = ContactForm()
	return render(request, 'contacto/contacto.html', {'form': form})
