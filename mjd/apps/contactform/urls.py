from django.conf.urls import url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

app_name='contact_form'
urlpatterns = [
    url(r'^contacto/$', views.formulario_contacto, name= 'formulario_contacto'),
]