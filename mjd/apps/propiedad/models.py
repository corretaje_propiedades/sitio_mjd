from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

from django.utils.translation import ugettext_lazy as _

from apps.localizacion.models import Localizacion

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django.urls import reverse
from django.contrib.auth.models import User


from crum import get_current_user





# Create your models here.
class Propietario(models.Model):
    nombre=models.CharField(max_length=100)
    website=models.URLField(max_length=300)
    email=models.EmailField()
    fono=models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('propietario_detail', kwargs={'pk':self.pk})

    @property
    def Propietario(self):
        return self.__str__()


    class Meta:
        app_label = 'propiedad'
        verbose_name = _("Propietario")
        verbose_name_plural = _("Propietarios")
        ordering = ("nombre","website")

class Arquitecto(models.Model):
    nombre=models.CharField(max_length=100)
    website=models.URLField(max_length=300)
    email=models.EmailField()
    fono=models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('arquitecto_detail', kwargs={'pk':self.pk})

    @property
    def Arquitecto(self):
        return self.__str__()


    class Meta:
        app_label = 'propiedad'
        verbose_name = _("Arquitecto")
        verbose_name_plural = _("Arquitectos")
        ordering = ("nombre","website")



class Propiedad(models.Model):
    CARACTER_CHOICES=(
        ('Buy','Vender'),
        ('Lease','Arrendar')
    )
    PROPIEDAD_CHOICES=(
        ("Depto","Departamento"),
        ("Casa","Casa"),
        ("Of","Oficina"),
        ("Sitio","Sitio"),
        ("Comerc","Comercial"),
        ("Ing","Industrial"),
        ("Agr","Agrícola"),
        ("Lot","Loteo"),
        ("Bod","Bodega"),
        ("Parc","Parcela"),
        ("Est","Estacionamiento"),
        ("Cons","Terreno en Construcción"),
    )
    ESTADO_CHOICES=(
        ('new','Nuevo'),
        ('used','Usado')
    )
    nombre=models.CharField(max_length=100)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_publica')
    #modificado = models.ForeignKey(User, related_name='usuario_modifica')
    estado = models.CharField(max_length=30, choices=ESTADO_CHOICES)
    caracter=models.CharField(max_length=100,choices=CARACTER_CHOICES)
    tipo_propiedad=models.CharField(max_length=100,choices=PROPIEDAD_CHOICES)
    fecha_registro=models.DateField(auto_now_add=True)
    #foreign key to comuna
    localizacion=models.ForeignKey(Localizacion, on_delete=models.CASCADE)
    #valores que se guardan en save, al hacer geocode
    #
    cantidad_dormitorios=models.PositiveSmallIntegerField()
    cantidad_baños=models.PositiveIntegerField()
    #=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
    #dimension_ancho=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
    area = models.FloatField(verbose_name='Área')
    descripcion= RichTextUploadingField(help_text="Describe a cabalidad la propiedad")
    #aspectos_destacables= RichTextUploadingField(help_text="Remarca los aspectos de mayor interés")
    fecha_entrega= models.DateField()
    propietario= models.ForeignKey(Propietario, blank=True, on_delete=models.CASCADE)
    arquitecto=models.ForeignKey(Arquitecto, blank=True, on_delete=models.CASCADE)
    en_oferta=models.BooleanField(default=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.nombre

    def get_tipo_propiedad(self):
        PC=dict(self.PROPIEDAD_CHOICES)
        return PC[self.tipo_propiedad]

    def get_absolute_url(self):
        return reverse('propiedad:propiedad_detail', kwargs={'pk':self.pk})

    @property
    def Busqueda(self):
        return self.__str__()

    def save(self, *args, **kwargs):
        user = get_current_user()
        if not self.usuario:
            self.usuario= user
        super(Propiedad, self).save(*args, **kwargs)

    class Meta:
        app_label = 'propiedad'
        verbose_name = _("Propiedad")
        verbose_name_plural = _("Propiedades")
        ordering = ("nombre","caracter","tipo_propiedad","fecha_registro")

# class LogSearchPropietadad(Propiedad):
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

class ImagenPropiedad(models.Model):
    imagen = models.ImageField(upload_to='propiedad/imagen')
    etiqueta = models.CharField(max_length=100)
    principal = models.BooleanField(default=False)
    propiedad = models.ForeignKey(Propiedad, related_name='imagen_propiedad', on_delete=models.CASCADE)
    imagen_thumbnail = ImageSpecField(source='imagen',
                                      processors=[ResizeToFill(300, 250)],
                                      format='JPEG',
                                      options={'quality': 60})
    def __str__(self):
        return self.etiqueta

    class Meta:
        app_label = 'propiedad'
        verbose_name = 'imagen'
        verbose_name_plural = 'imágenes'

class PrecioPropiedad(models.Model):
    MONEDA_CHOICES=(
        ("Peso","$"),
        ("Unidad de Fomento","U.F."),
        ("Dólar Americano","US$")
    )
    moneda = models.CharField(max_length=100,choices=MONEDA_CHOICES)
    cantidad = models.FloatField()
    propiedad = models.ForeignKey(Propiedad, related_name='precio_propiedad', on_delete=models.CASCADE)

    def __str__(self):
        return self.moneda +" "+ str(self.cantidad)

    class Meta:
        app_label = 'propiedad'
        verbose_name = _("Precio Propiedad")
        verbose_name_plural = _("Precios de Propiedades")
        #ordering = ("moneda","cantidad","propiedad.fecha_entrega")
        managed = True
