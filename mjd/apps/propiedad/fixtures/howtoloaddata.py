#http://stackoverflow.com/questions/11875770/how-to-overcome-datetime-datetime-not-json-serializable-in-python
#from apps.propiedad.fixtures.howtoloaddata import load_propiedades

import json
import datetime
from pyexcel_ods3 import get_data
from apps.localizacion.models import Localizacion, Comuna
from apps.propiedad.models import Propietario, Arquitecto, Propiedad, ImagenPropiedad, PrecioPropiedad
from django.contrib.auth.models import User
from django.core.files import File
import os

def load_propiedades():
    json.JSONEncoder.default = lambda self,obj: (obj.isoformat() if isinstance(obj, datetime.datetime) else None)
    path=os.path.dirname(os.path.abspath(__file__))
    file_path=os.path.join(path,'test_datas.ods')
    print(file_path)
    p=get_data(file_path)
    pk = []
    for key in p.keys():
        pk.append(key)
    #cargar pk[0]='Propietarios'
    IDs = {}
    for i in range(0,len(pk)):
        if pk[i]=='Propietarios':
            n_p_id = {}
            for j in range(1,len(p[pk[i]])):
                prop_tuple = p[pk[i]][j]
                prop_orig_id = int(prop_tuple[0])
                new_prop = Propietario()
                new_prop.nombre = prop_tuple[1]
                new_prop.website = prop_tuple[2]
                new_prop.email = prop_tuple[3]
                new_prop.fono = int(prop_tuple[4])
                new_prop.save()
                n_p_id[prop_orig_id] = new_prop.id
                del prop_tuple
                del new_prop
            IDs['propietario'] = n_p_id
        if pk[i] == 'Arquitectos':
            n_a_id = {}
            for j in range(1,len(p[pk[1]])):
                arq_tuple = p[pk[i]][j]
                arq_orig_id = int(arq_tuple[0])
                new_arq = Arquitecto()
                new_arq.nombre = arq_tuple[1]
                new_arq.website = arq_tuple[2]
                new_arq.email = arq_tuple[3]
                new_arq.fono = int(arq_tuple[4])
                new_arq.save()
                n_a_id[arq_orig_id] = new_arq.id
                del arq_tuple
                del new_arq
            IDs['arquitecto']=n_a_id
        if pk[i] == 'Localizaciones':
            n_loc_id = {}
            for j in range(1,len(p[pk[i]])):
                loc_tuple = p[pk[i]][j]
                loc_orig_id = int(loc_tuple[0])
                new_loc = Localizacion()
                new_loc.ubicacion = loc_tuple[1]
                new_loc.numero = int(loc_tuple[2])
                #print(loc_tuple[3])
                comuna=Comuna.objects.get(nombre=loc_tuple[3])
                new_loc.comuna = comuna
                new_loc.save()
                n_loc_id[loc_orig_id] = new_loc.id
                del loc_tuple
                del new_loc
            IDs['localizacion']=n_loc_id
        if pk[i] == 'Propiedades':
            n_propiedad_id = {}
            for j in range(1,len(p[pk[i]])):
                propiedad_tuple = p[pk[i]][j]
                #print(propiedad_tuple)
                propiedad_orig_id = int(propiedad_tuple[0])
                new_propiedad = Propiedad()
                new_propiedad.nombre = propiedad_tuple[1]
                new_propiedad.usuario = User.objects.get(id=propiedad_tuple[2])
                new_propiedad.estado = new_propiedad.ESTADO_CHOICES[int(propiedad_tuple[3]-1)][0]
                new_propiedad.caracter = new_propiedad.CARACTER_CHOICES[int(propiedad_tuple[4]-1)][0]
                #print(int(propiedad_tuple[5]))
                new_propiedad.tipo_propiedad = new_propiedad.PROPIEDAD_CHOICES[int(propiedad_tuple[5])-1][0]
                new_propiedad.fecha_registro = propiedad_tuple[6]
                new_propiedad.localizacion = Localizacion.objects.get(id = IDs['localizacion'][int(propiedad_tuple[7])])
                new_propiedad.cantidad_dormitorios = propiedad_tuple[8]
                new_propiedad.cantidad_baños = propiedad_tuple[9]
                new_propiedad.area = propiedad_tuple[10]
                new_propiedad.descripcion = '<p>'+propiedad_tuple[11]+'</p>'
                new_propiedad.fecha_entrega = propiedad_tuple[12]
                new_propiedad.propietario = Propietario.objects.get(id = IDs['propietario'][int(propiedad_tuple[13])])
                new_propiedad.arquitecto = Arquitecto.objects.get(id = IDs['arquitecto'][int(propiedad_tuple[14])])
                if int(propiedad_tuple[15])==1:
                    new_propiedad.en_oferta = True
                else:
                    new_propiedad.en_oferta = False
                new_propiedad.save()
                n_propiedad_id[propiedad_orig_id] = new_propiedad.id
                del propiedad_tuple
                del new_propiedad
            IDs['propiedad']=n_propiedad_id
        if pk[i] == 'ImagenesPropiedades':
            n_im_prop_id = {}
            for j in range(1,len(p[pk[i]])):
                img_propiedad_tuple = p[pk[i]][j]
                img_propiedad_orig_id = int(img_propiedad_tuple[0])
                new_img_propiedad = ImagenPropiedad()
                #aabrir archivo en ruta
                ruta_img = img_propiedad_tuple[1]
                imagen = File(open(os.path.join(path,ruta_img),'rb'))
                new_img_propiedad.imagen = imagen
                new_img_propiedad.etiqueta = img_propiedad_tuple[2]
                new_img_propiedad.principal = img_propiedad_tuple[3]
                new_img_propiedad.propiedad = Propiedad.objects.get(id = IDs['propiedad'][img_propiedad_tuple[4]])
                new_img_propiedad.save()
                n_im_prop_id[img_propiedad_orig_id] = new_img_propiedad.id
                del img_propiedad_tuple
                del new_img_propiedad
            IDs['imagenpropiedad']=n_im_prop_id
        if pk[i] == 'Precios':
            n_precio_prop_id = {}
            for j in range(1,len(p[pk[i]])):
                precio_propiedad_tuple = p[pk[i]][j]
                precio_propiedad_orig_id = int(precio_propiedad_tuple[0])
                new_precio_propiedad = PrecioPropiedad()
                #MONEDA_CHOICES
                new_precio_propiedad.moneda = new_precio_propiedad.MONEDA_CHOICES[int(precio_propiedad_tuple[1])-1][0]
                new_precio_propiedad.cantidad = precio_propiedad_tuple[2]
                new_precio_propiedad.propiedad = Propiedad.objects.get(id = IDs['propiedad'][int(precio_propiedad_tuple[3])])
                new_precio_propiedad.save()
                n_precio_prop_id[precio_propiedad_orig_id] = new_precio_propiedad.id
                del precio_propiedad_tuple
                del new_precio_propiedad
            IDs['preciopropiedad']=n_precio_prop_id