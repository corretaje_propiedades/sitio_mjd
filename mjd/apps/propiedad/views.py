from django.http import HttpResponse
from django.views.generic import TemplateView,ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Propiedad, Propietario, Arquitecto
from .forms import PropiedadForm, PropietarioForm
from .multiforms import PropiedadMultiForm
#autcomplete viewe
#https://django-autocomplete-light.readthedocs.org/en/master/tutorial.html

from dal import autocomplete

class PropietarioAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return  Propietario.objects.none()

        qs = Propietario.objects.all()

        if self.q:
            qs = qs.filter( nombre__istartswith= self.q )

        return qs

class ArquitectoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return  Arquitecto.objects.none()

        qs = Arquitecto.objects.all()

        if self.q:
            qs = qs.filter( nombre__istartswith= self.q )

        return qs

class PropiedadChoicesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs=[]

        i=0

        for e in Propiedad.PROPIEDAD_CHOICES:
            qs.append({'nombre':e[1],'pk':i})
            i+=1

        if self.q:
            qs = [x for x in qs if self.q.lower() in x['nombre'].lower()]

        return qs

class CaracterChoicesAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):

        qs=[]

        i=0

        for e in Propiedad.CARACTER_CHOICES:
            qs.append({'nombre':e[1],'pk':i})
            i+=1

        if self.q:
            qs = [x for x in qs if self.q.lower() in x['nombre'].lower()]

        return qs
##############################
##############################
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormMixin
from apps.principal.forms import BusquedaForm

class VisitaPropiedadMixin(FormMixin):
    form_class = BusquedaForm

class PropiedadMixin(LoginRequiredMixin,VisitaPropiedadMixin):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'


##########################CRUD

class PropiedadList(ListView):
    # login_url = '/usuario/login/'
    # redirect_field_name = 'redirect_to'
    model = Propiedad
    paginate_by=9

class PropiedadCreate(PropiedadMixin,CreateView):
    form_class = PropiedadMultiForm
    template_name = 'propiedad/propiedad_form.html'
    success_url = reverse_lazy('propiedad_list')


class PropiedadUpdate(UpdateView):
    model = Propiedad
    success_url = reverse_lazy('propiedad_list')
    fields = ['nombre','localizacion', 'caracter', 'tipo_propiedad','cantidad_dormitorios', 'cantidad_baños', 'area', 'descripcion','fecha_entrega','propietario', 'arquitecto']

class PropiedadDelete(DeleteView):
    model = Propiedad
    success_url = reverse_lazy('propiedad_list')

class PropiedadDetail(VisitaPropiedadMixin,DetailView):
    model = Propiedad


######################################
######################################

class PropietarioList(PropiedadMixin,ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Propietario

class PropietarioCreate(CreateView):
    model = Propietario
    success_url = reverse_lazy('propietario_list')
    fields = ['nombre', 'email', 'fono']

class PropietarioUpdate(UpdateView):
    model = Propietario
    success_url = reverse_lazy('propietario_list')
    fields = ['nombre', 'email', 'fono']

class PropietarioDelete(DeleteView):
    model = Propietario
    success_url = reverse_lazy('propietario_list')

class PropietarioDetail(VisitaPropiedadMixin,DetailView):
    model = Propietario

######################################
######################################

class ArquitectoList(PropiedadMixin,ListView):
    model = Arquitecto

class ArquitectoCreate(CreateView):
    model = Arquitecto
    success_url = reverse_lazy('arquitecto_list')
    fields = ['nombre', 'email', 'fono']


class ArquitectoUpdate(UpdateView):
    model = Arquitecto
    success_url = reverse_lazy('arquitecto_list')
    fields = ['nombre', 'email', 'fono']


class ArquitectoDelete(DeleteView):
    model = Arquitecto
    success_url = reverse_lazy('arquitecto_list')


class ArquitectoDetail(VisitaPropiedadMixin,DetailView):
    model = Arquitecto


######################################################
######################################################
######################################################

from haystack.generic_views import SearchView
from .forms import PropiedadSearchForm

class PropiedadSearchView(SearchView):
    template_name = 'search/propiedad.html'
    form_class = PropiedadSearchForm
    success_url = reverse_lazy('haystack_buscar_propiedad')

    def create_response(self):
        logger.info(self.query)  #or log self.query as you like
        return super(LoggingSearchView, self).create_response()
