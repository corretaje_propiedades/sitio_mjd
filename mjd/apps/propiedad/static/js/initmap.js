var map;

var view;

var this_layer = [
    new ol.layer.Tile({
        preload: 4,
        source: new ol.source.OSM(),
    })
]


var lat = parseFloat(document.getElementById("lat").textContent);
var lon = parseFloat(document.getElementById("lon").textContent);

var geolocale = ol.proj.fromLonLat([lon,lat]);


function inicializar( ){


        var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point([lat, lon]),
        name: 'Propiedad',
      });
        console.log(iconFeature)

      var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
          anchor: [0.5, 46],
          anchorXUnits: 'fraction',
          anchorYUnits: 'pixels',
          src: 'https://openlayers.org/en/v4.3.3/examples/data/icon.png'
        }))
      });

      console.log(iconStyle)

      iconFeature.setStyle(iconStyle);

      var vectorSource = new ol.source.Vector({
        features: [iconFeature]
      });

      var vectorLayer = new ol.layer.Vector({
        source: vectorSource
      });
      console.log(vectorLayer)


    console.log(geolocale)
    view = new ol.View({
    // the view's initial state
    center: geolocale,
    zoom: 12
  });

    map = new ol.Map({
        layers : [this_layer, vectorLayer],
        loadTilesWhileAnimating : true,
        target: 'map',
        view: view,
    })

}

inicializar();

/*
* Selector múltiple
* */



/*
Verificar dirección
==================

Aquí se ejecuta una consulta ajax en servidor que entrega un string con la dirección, en servidor se completa con el país y se hace un geocoder para obtener lonlat.

Una vez que se devuelve lonlat, se visualiza en el mapa ol3 la posición y se coloca un marcador.

Si se realiza otra consulta, se debe quitar el marcador y colar otro en la nueva posición.

metodo: GET
 */

var direccion;
var com_id;
var comuna;
var dir_com;
var nuevo_punto;

$(document).ready(function(){
    $("#verificar").click(function(event){
        event.preventDefault();
        direccion = document.getElementById("id_ubicacion").value;
        com_id = document.getElementById("id_comuna");
        comuna = com_id.options[com_id.selectedIndex].text;
        dir_com = direccion +", "+ comuna
        console.log("Verificando direccion");
        console.log(direccion +", "+ comuna)
        $.ajax({
           type: 'GET',
           cache: true,
           data: {
               'direccion': dir_com
           },
           contentType: "application/json;charset=utf-8",
           dataType: 'json',
           success: function(resp){
               console.log("Verificación exitosa, ver mapa");
               console.log(resp['v_lat']);
               console.log(resp['v_lon']);
               nuevo_punto = ol.proj.fromLonLat([resp['v_lon'],resp['v_lat']]);
               var pan = ol.animation.pan({
                  duration: 2000,
                  source: /** @type {ol.Coordinate} */ (view.getCenter())
                });
               map.beforeRender(pan);
               view.setCenter(nuevo_punto);
               view.setResolution(18);
           },
           error: function(ts){
               console.log("Hubo un error");
           }
        });
    });
})