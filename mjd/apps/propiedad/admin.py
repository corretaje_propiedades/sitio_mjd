from django.contrib import admin

from .models import Propietario,Arquitecto,Propiedad, ImagenPropiedad, PrecioPropiedad
# Register your models here.
admin.site.register(Propietario)
admin.site.register(Arquitecto)

class ImagenPropiedadInline(admin.StackedInline):
    model = ImagenPropiedad
    can_delete = False
    verbose_name_plural = 'Imagenes de Propiedad'

# Register your models here.

class PrecioPropiedadInline(admin.StackedInline):
    model = PrecioPropiedad
    can_delete = False
    verbose_name_plural = 'Precios de Propiedad'

class PropiedadAdmin(admin.ModelAdmin):
    inlines= (ImagenPropiedadInline,PrecioPropiedadInline)

admin.site.register(Propiedad,PropiedadAdmin)

admin.site.register(ImagenPropiedad)

admin.site.register(PrecioPropiedad)