# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-22 08:15
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('propiedad', '0003_propiedad_usuario'),
    ]

    operations = [
        migrations.AddField(
            model_name='propiedad',
            name='modificado',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='usuario_modifica', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='propiedad',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuario_publica', to=settings.AUTH_USER_MODEL),
        ),
    ]
