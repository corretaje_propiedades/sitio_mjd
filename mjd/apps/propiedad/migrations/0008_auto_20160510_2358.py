# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-11 02:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('propiedad', '0007_auto_20160510_2204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='preciopropiedad',
            name='propiedad',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='precio_propiedad', to='propiedad.Propiedad'),
        ),
    ]
