# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-19 02:44
from __future__ import unicode_literals

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('localizacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Arquitecto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('website', models.URLField(max_length=300)),
                ('email', models.EmailField(max_length=254)),
                ('fono', models.PositiveIntegerField()),
                ('object_id', models.PositiveIntegerField(blank=True, editable=False, null=True)),
                ('content_type', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Arquitecto',
                'ordering': ('nombre', 'website'),
                'verbose_name_plural': 'Arquitectos',
            },
        ),
        migrations.CreateModel(
            name='Propiedad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('estado', models.CharField(choices=[('new', 'Nuevo'), ('used', 'Usado')], max_length=30)),
                ('caracter', models.CharField(choices=[('Buy', 'Vender'), ('Lease', 'Arrendar')], max_length=100)),
                ('tipo_propiedad', models.CharField(choices=[('Depto', 'Departamento'), ('Casa', 'Casa'), ('Of', 'Oficina'), ('Sitio', 'Sitio'), ('Comerc', 'Comercial'), ('Ing', 'Industrial'), ('Agr', 'Agrícola'), ('Lot', 'Loteo'), ('Bod', 'Bodega'), ('Parc', 'Parcela'), ('Est', 'Estacionamiento'), ('Cons', 'Terreno en Construcción')], max_length=100)),
                ('fecha_registro', models.DateField(auto_now_add=True)),
                ('telefono', models.PositiveIntegerField()),
                ('cantidad_dormitorios', models.PositiveSmallIntegerField()),
                ('cantidad_baños', models.PositiveIntegerField()),
                ('area', models.FloatField(verbose_name='Área')),
                ('descripcion', ckeditor_uploader.fields.RichTextUploadingField(help_text='Describe a cabalidad la propiedad')),
                ('fecha_entrega', models.DateField()),
                ('en_oferta', models.BooleanField(default=True)),
                ('object_id', models.PositiveIntegerField(blank=True, editable=False, null=True)),
                ('arquitecto', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='propiedad.Arquitecto')),
                ('content_type', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('localizacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='localizacion.Localizacion')),
            ],
            options={
                'verbose_name': 'Propiedad',
                'ordering': ('nombre', 'caracter', 'tipo_propiedad', 'fecha_registro'),
                'verbose_name_plural': 'Propiedades',
            },
        ),
        migrations.CreateModel(
            name='Propietario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('website', models.URLField(max_length=300)),
                ('email', models.EmailField(max_length=254)),
                ('fono', models.PositiveIntegerField()),
                ('object_id', models.PositiveIntegerField(blank=True, editable=False, null=True)),
                ('content_type', models.ForeignKey(blank=True, editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'Propietario',
                'ordering': ('nombre', 'website'),
                'verbose_name_plural': 'Propietarios',
            },
        ),
        migrations.AddField(
            model_name='propiedad',
            name='propietario',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='propiedad.Propietario'),
        ),
    ]
