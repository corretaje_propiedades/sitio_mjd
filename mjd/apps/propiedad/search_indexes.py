from haystack import indexes
from .models import Propiedad

class PropiedadIndex(indexes.SearchIndex, indexes.Indexable):
    """
    nombre=models.CharField(max_length=100)
    caracter=models.CharField(max_length=100,choices=CARACTER_CHOICES)
    tipo_propiedad=models.CharField(max_length=100,choices=PROPIEDAD_CHOICES)
    fecha_registro=models.DateField(auto_now_add=True)
    #foreign key to comuna
    localizacion=models.ForeignKey(Localizacion)
    #valores que se guardan en save, al hacer geocode
    cantidad_dormitorios=models.PositiveSmallIntegerField()
    cantidad_baños=models.PositiveIntegerField()
    dimension_largo=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
    dimension_ancho=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
    descripcion= RichTextUploadingField(help_text="Describe a cabalidad la propiedad")
    aspectos_destacables= RichTextUploadingField(help_text="Remarca los aspectos de mayor interés")
    fecha_entrega= models.DateField()
    propietario= models.ForeignKey(Propietario, blank=True)
    arquitecto=models.ForeignKey(Arquitecto, blank=True)
    """

    text = indexes.CharField(document = True, use_template= True)
    nombre = indexes.CharField(model_attr='nombre')
    caracter = indexes.CharField(model_attr='caracter')
    tipo_propiedad = indexes.CharField(model_attr='tipo_propiedad')
    aspectos_destacables = indexes.CharField(model_attr='aspectos_destacables')
    #localizacion = indexes.LocationField(model_attr='localizacion')
    dimension_largo = indexes.FloatField(model_attr='dimension_largo')
    dimension_ancho = indexes.FloatField(model_attr='dimension_ancho')
    descripcion = indexes.CharField(model_attr='descripcion')
    fecha_entrega = indexes.DateField(model_attr='fecha_entrega')
    propietario = indexes.CharField(model_attr='propietario')
    arquitecto = indexes.CharField(model_attr='arquitecto')

    def get_model(self):
        return Propiedad

    def index_queryset(self, using=None):
        """A usar cuando el índice esta actualizado"""
        return self.get_model().objects.all()