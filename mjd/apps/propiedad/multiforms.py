from multiform import MultiModelForm
from apps.localizacion.forms import NewLocalizacionForm
from .forms import PropiedadForm


class PropiedadMultiForm(MultiModelForm):
    base_forms = [
        ('localizacion', NewLocalizacionForm),
        ('propiedad', PropiedadForm),
    ]

    def save(self, commit = True):
        """
        Guardar primero localizacion y, luego, propiedad
        :param commit:
        :return:
        """
        instances = super(PropiedadMultiForm,self).save(commit=False)
        instances['localizacion'].save()
        instances['propiedad'].localizacion = instances['localizacion']

        if commit:
            for instance in instances.values():
                instance.save()
        return instances