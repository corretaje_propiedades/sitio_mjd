from django import forms
from django.forms import ModelForm
from .models import Arquitecto, Propietario, Propiedad, ImagenPropiedad
from dal import autocomplete

"""
nombre=models.CharField(max_length=100)
website=models.URLField(max_length=300)
email=models.EmailField()
fono=models.PositiveIntegerField()
"""

class ArquitectoForm(ModelForm):
    class Meta:
        model = Arquitecto
        fields = ['nombre','website','email','fono']

"""
nombre=models.CharField(max_length=100)
website=models.URLField(max_length=300)
email=models.EmailField()
fono=models.PositiveIntegerField()
"""

class PropietarioForm(ModelForm):
    class Meta:
        model = Propietario
        fields = ['nombre','website','email','fono']

"""
nombre=models.CharField(max_length=100)
caracter=models.CharField(max_length=100,choices=CARACTER_CHOICES)
tipo_propiedad=models.CharField(max_length=100,choices=PROPIEDAD_CHOICES)
fecha_registro=models.DateField(auto_now_add=True)
#foreign key to comuna
localizacion=models.ForeignKey(Localizacion) -----> LocalizacionForm
#valores que se guardan en save, al hacer geocode
#
telefono=models.PositiveIntegerField()
cantidad_dormitorios=models.PositiveSmallIntegerField()
cantidad_baños=models.PositiveIntegerField()
dimension_largo=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
dimension_ancho=models.FloatField(help_text="Dimensión en metros [m], decimales con '.' ")
descripcion= RichTextUploadingField(help_text="Describe a cabalidad la propiedad")
aspectos_destacables= RichTextUploadingField(help_text="Remarca los aspectos de mayor interés")
fecha_entrega= models.DateField()
propietario= models.ForeignKey(Propietario) -----> Autocomplete
arquitectos=models.ManyToManyField(Arquitecto) -----> Autocomplete
"""
from datetimewidget.widgets import DateTimeWidget

fecha_entrega = forms.DateField(input_formats=['%d/%m/%Y', '%d %m %Y','%d-%m-%Y'])
class PropiedadForm(ModelForm):
    class Meta:
        model = Propiedad
        fields = [
            'nombre',
            'caracter',
            'tipo_propiedad',
            'cantidad_dormitorios',
            'cantidad_baños',
            'area',
            'descripcion',
            'fecha_entrega',
            'propietario',
            'arquitecto'
                  ]
        dateTimeOptions = {
            'format': 'dd/mm/yyyy',
            'autoclose': True,
            'startView': '1',
            'minView': '1',
        }

        widgets = {
            'propietario' : autocomplete.ModelSelect2( url = 'propietario-autocomplete'),
            'arquitecto' : autocomplete.ModelSelect2( url = 'arquitecto-autocomplete'),
            'fecha_entrega': DateTimeWidget(attrs={'id':"fecha_entrega"}, options = dateTimeOptions ,bootstrap_version=3)
        }

#WIDGETS

from django.urls import reverse
from django.utils.safestring import mark_safe
from django.forms import widgets
from django.conf import settings

class RelatedFieldWidgetCanAdd(widgets.Select):

    def __init__(self, related_model, related_url=None, *args, **kw):

        super(RelatedFieldWidgetCanAdd, self).__init__(*args, **kw)

        if not related_url:
            rel_to = related_model
            info = (rel_to._meta.app_label, rel_to._meta.object_name.lower())
            related_url = 'admin:%s_%s_add' % info

        # Be careful that here "reverse" is not allowed
        self.related_url = related_url

    def render(self, name, value, *args, **kwargs):
        self.related_url = reverse(self.related_url)
        output = [super(RelatedFieldWidgetCanAdd, self).render(name, value, *args, **kwargs)]
        output.append('<a href="%s" class="add-another" id="add_id_%s" onclick="return showAddAnotherPopup(this);"> ' % \
            (self.related_url, name))
        output.append('<img src="%sadmin/img/icon_addlink.gif" width="10" height="10" alt="%s"/></a>' % (settings.STATIC_URL, 'Add Another'))
        return mark_safe(''.join(output))

#Busqueda de propiedades

from haystack.forms import model_choices
from haystack.query import EmptySearchQuerySet, SearchQuerySet
from haystack.forms import HighlightedModelSearchForm

class PropiedadSearchForm(ModelForm):

    """
    Buscador de propiedades con campos del modelo Propiedad

    """

    descripcion = forms.CharField(max_length=200)
    aspectos_destacables = forms.CharField(max_length=200)

    class Meta:
        model = Propiedad
        fields = [
            'nombre',
            'caracter',
            'tipo_propiedad',
            'cantidad_dormitorios',
            'cantidad_baños',
            'area',
            'descripcion',
            'fecha_entrega',
            'propietario',
            'arquitecto'
        ]
        dateTimeOptions = {
            'format': 'dd/mm/yyyy',
            'autoclose': True,
            'startView': '1',
            'minView': '1',
        }


        widgets = {
            #'descripcion' : forms.TextInput(attrs={'size': 10, 'title': 'Search',}),
            #'aspectos_destacables' : forms.TextInput(attrs={'size': 10, 'title': 'Search',}),
            'propietario' : autocomplete.ModelSelect2( url = 'propietario-autocomplete'),
            'arquitecto' : autocomplete.ModelSelect2( url = 'arquitecto-autocomplete'),
            'fecha_entrega': DateTimeWidget(attrs={'id':"fecha_entrega"}, options = dateTimeOptions ,bootstrap_version=3),
            #'imagenes': RelatedFieldWidgetCanAdd()
        }


    def __init__(self,*args,**kwargs):
        self.searchqueryset = kwargs.pop('searchqueryset', None)
        self.load_all = kwargs.pop('load_all', False)

        if self.searchqueryset is None:
            self.searchqueryset = SearchQuerySet().models(Propiedad)

        super(PropiedadSearchForm, self).__init__(*args, **kwargs)

        self.fields['nombre'].required = False
        self.fields['caracter'].required = False
        self.fields['tipo_propiedad'].required = False
        self.fields['cantidad_dormitorios'].required = False
        self.fields['cantidad_baños'].required = False
        self.fields['area'].required = False
        self.fields['descripcion'].required = False
        self.fields['fecha_entrega'].required = False
        self.fields['propietario'].required = False
        self.fields['arquitecto'].required = False

        self.fields['descripcion'].widget.attrs['rows'] = 50
        self.fields['aspectos_destacables'].widget.attrs['rows'] = 50


    def no_query_found(self):
        """
        Determines the behavior when no query was found.
        By default, no results are returned (``EmptySearchQuerySet``).
        Should you want to show all results, override this method in your
        own ``SearchForm`` subclass and do ``return self.searchqueryset.all()``.
        """
        return EmptySearchQuerySet()

    def get_suggestion(self):
        if not self.is_valid():
            return None

        return self.searchqueryset.spelling_suggestion(self.cleaned_data['q'])


    def get_models(self):
        return self.model


    def search(self):

        sqs = self.searchqueryset
        if not self.is_valid():
            return self.no_query_found()
        #Crear dicccionario de consulta: {'campo':'valor'}

        if self.cleaned_data['nombre']:
            sqs = sqs.filter( caracter  = self.cleaned_data['nombre'])

        if self.cleaned_data['caracter']:
            sqs = sqs.filter( caracter  = self.cleaned_data['caracter'])

        if self.cleaned_data['tipo_propiedad']:
            sqs = sqs.filter( tipo_propiedad  = self.cleaned_data['tipo_propiedad'])

        if self.cleaned_data['cantidad_dormitorios']:
            sqs = sqs.filter( cantidad_dormitorios__gte  = self.cleaned_data['cantidad_dormitorios'])

        if self.cleaned_data['cantidad_baños']:
            sqs = sqs.filter( cantidad_baños__gte  = self.cleaned_data['cantidad_baños'])

        if self.cleaned_data['descripcion']:
            sqs = sqs.filter( descripccion__contains  = self.cleaned_data['descripcion'])

        if self.cleaned_data['aspectos_destacables']:
            sqs = sqs.filter( aspectos_destacables__contains  = self.cleaned_data['aspectos_destacables'])

        if self.cleaned_data['fecha_entrega']:
            sqs = sqs.filter( fecha_entrega__lte  = self.cleaned_data['fecha_entrega'])


        return sqs.highlight()

    def save(self, commit=True):
        return None
