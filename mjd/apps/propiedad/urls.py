from django.conf.urls import url
from . import views


app_name='propiedad'
urlpatterns = [
    url(r'^listar/propietarios$',views.PropietarioList.as_view(), name='propietario_list'),
    url(r'^agregar/propietario$', views.PropietarioCreate.as_view(), name='propietario_new'),
    url(r'^editar/propietario/(?P<pk>\d+)$', views.PropietarioUpdate.as_view(), name='propietario_edit'),
    url(r'^borrar/propietario/(?P<pk>\d+)$', views.PropietarioDelete.as_view(), name='propietario_delete'),
    url(r'^propietario/(?P<pk>\d+)$', views.PropietarioDetail.as_view(), name='propietario_detail'),

    ################################
    url(r'^listar/arquitectos$',views.ArquitectoList.as_view(), name='arquitecto_list'),
    url(r'^agregar/arquitecto$', views.ArquitectoCreate.as_view(), name='arquitecto_new'),
    url(r'^editar/arquitecto/(?P<pk>\d+)$', views.ArquitectoUpdate.as_view(), name='arquitecto_edit'),
    url(r'^borrar/arquitecto/(?P<pk>\d+)$', views.ArquitectoDelete.as_view(), name='arquitecto_delete'),
    url(r'^arquitecto/(?P<pk>\d+)$', views.ArquitectoDetail.as_view(), name='arquitecto_detail'),
    #################################
    url(r'^listar/propiedades$',views.PropiedadList.as_view(), name='propiedad_list'),
    url(r'^agregar/propiedad$', views.PropiedadCreate.as_view(), name='propiedad_new'),
    url(r'^editar/propiedad/(?P<pk>\d+)$', views.PropiedadUpdate.as_view(), name='propiedad_edit'),
    url(r'^borrar/propiedad/(?P<pk>\d+)$', views.PropiedadDelete.as_view(), name='propiedad_delete'),
    url(r'^propiedad/(?P<pk>\d+)$', views.PropiedadDetail.as_view(), name='propiedad_detail'),
    ##Autocomplete urls
    url(r'^propietario-autocomplete/$',views.PropietarioAutocomplete.as_view( create_field = 'nombre' ), name='propietario-autocomplete'),
    url(r'^arquitecto-autocomplete/$',views.ArquitectoAutocomplete.as_view( create_field = 'nombre' ), name='arquitecto-autocomplete'),
    url(r'^propiedad-choices-autocomplete/$',views.PropiedadChoicesAutocomplete.as_view( ), name='propiedad-choices-autocomplete'),
    url(r'^caracter-choices-autocomplete/$',views.CaracterChoicesAutocomplete.as_view( ), name='caracter-choices-autocomplete'),
    url(r'^buscar/propiedad/?$', views.PropiedadSearchView.as_view(),name='haystack_buscar_propiedad'),
    ]