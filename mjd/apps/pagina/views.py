from django.shortcuts import get_object_or_404, render
from .models import Pagina, Clasificacion
from django.conf import settings
#autcomplete view
#https://django-autocomplete-light.readthedocs.org/en/master/tutorial.html

from dal import autocomplete

class ClasificacionAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return  Clasificacion.objects.none()

        qs = Clasificacion.objects.all()

        if self.q:
            qs = qs.filter( tipo__istartswith= self.q )

        return qs

# Create your views here.

def ver_pagina ( request, this_slug_titulo ):
    pagina = get_object_or_404 ( Pagina, slug_titulo=this_slug_titulo )
    static_media_url = settings.MEDIA_URL
    context = { 'slug_titulo': this_slug_titulo, 'pagina': pagina, 'static_media_url': static_media_url }
    return render ( request, 'pagina.html', context )


def ver_clasificacion ( request, this_slug_tipo ):
    clase = get_object_or_404 ( Clasificacion, slug_tipo=this_slug_tipo )
    paginas = Pagina.objects.filter ( clasificacion=clase.id )
    context = { 'slug_tipo': this_slug_tipo, 'clase': clase, 'paginas': paginas }
    return render ( request, 'tipo.html', context )


from django.http import HttpResponse
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import PaginaForm

class PaginaList ( LoginRequiredMixin,ListView ):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = "pagina/pagina_list.html"
    model = Pagina


class PaginaCreate ( LoginRequiredMixin,CreateView ):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    form_class = PaginaForm
    template_name = "pagina/pagina_form.html"
    success_url = reverse_lazy ( 'pagina_list' )


class PaginaUpdate ( LoginRequiredMixin,UpdateView ):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    form_class = PaginaForm
    template_name = "pagina/pagina_form.html"
    success_url = reverse_lazy ( 'pagina_list' )



class PaginaDelete ( LoginRequiredMixin,DeleteView ):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Pagina
    success_url = reverse_lazy ( 'pagina_list' )
