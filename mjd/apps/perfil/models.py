from django.db import models
from django.contrib.auth.models import User, UserManager
from django.utils.translation import ugettext_lazy as _

from . import managers

# Create your models here.
class UsuarioCorredora(models.Model):
    USER_CHOICES = (
        ('administrador','Administrador'),
        ('vendedor','Vendedor'),
        ('cliente','Cliente'),
    )
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, default = 1)
    nombre = models.CharField(max_length=50,blank=True)
    apellido = models.CharField(max_length=50,blank=True)
    telefono = models.IntegerField(blank=True)
    imagen = models.ImageField(upload_to='imagen_perfil', blank=True)
    grupo = models.CharField(max_length=50,choices=USER_CHOICES, blank=True)

    objects = managers.ProfileManager()

    @property
    def nombre(self):
        return self.nombre+" "+self.apellido

    class Meta:
        app_label= 'perfil'
        verbose_name='Usuario(a) Corredora'
        verbose_name_plural = 'Usuarios(as) Corredora'
        ordering = ("usuario", )

    def __str__(self):
        return self.nombre()


class ProfileUserCorredora(models.Model):
    # Relations
    user = models.OneToOneField(
        #settings.AUTH_USER_MODEL
        UsuarioCorredora,
        related_name="profile",
        verbose_name=_("usuario"),
        on_delete=models.CASCADE
        )
    # Attributes - Mandatory
    interaction = models.PositiveIntegerField(
        default=0,
        verbose_name=_("interacción")
        )
    # Attributes - Optional
    # Object Manager
    objects = managers.ProfileManager()

    # Custom Properties
    def username(self):
        return self.user.usuario.username

    # Methods

    # Meta and String
    class Meta:
        app_label= 'perfil'
        verbose_name = _("Perfil")
        verbose_name_plural = _("Perfiles")
        ordering = ("user",)

    def __str__(self):
        return self.user.username

from django.dispatch import receiver
from django.db.models.signals import post_save

@receiver(post_save, sender=ProfileUserCorredora)
def create_profile_for_new_user(sender, created, instance, **kwargs):
    if created:
        profile = ProfileUserCorredora(user=instance)
        profile.save()
