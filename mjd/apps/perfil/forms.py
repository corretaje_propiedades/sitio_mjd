from django.contrib.auth.forms import UserCreationForm
from .models import UsuarioCorredora

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = UsuarioCorredora
        fields =  ('telefono','imagen','grupo')


class CustomUserRegisterForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = UsuarioCorredora
        fields =  ('telefono','imagen')

        #Modificar save --> grupo: cliente