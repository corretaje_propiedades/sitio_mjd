from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import UsuarioCorredora, ProfileUserCorredora

class UsuarioCorredoraInline(admin.StackedInline):
    model = UsuarioCorredora
    can_delete = False
    verbose_name_plural = 'Usuarios(as)MJD'
# Register your models here.

class UserAdmin(BaseUserAdmin):
    inlines = (UsuarioCorredoraInline,)

admin.site.unregister(User)
admin.site.register(User,UserAdmin)

class ProfileAdmin(admin.ModelAdmin):

    list_display = ("username", "interaction")

    search_fields = ["user__username"]

admin.site.register(ProfileUserCorredora,ProfileAdmin)