"""mjd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from password_reset.views import Reset,Recover,RecoverDone,ResetDone
from django.core.urlresolvers import reverse

from . import views


app_name='perfil'
urlpatterns = [
    url(r'^listar/usuarios$',views.UsuarioList.as_view(), name='usuario_list'),
    url(r'^agregar/usuario$', views.UsuarioCreate.as_view(), name='usuario_new'),
    url(r'^registrarse$', views.UsuarioRegistre.as_view(), name='usuario_register'),
    url(r'^editar/usuario/(?P<pk>\d+)$', views.UsuarioUpdate.as_view(), name='usuario_edit'),
    url(r'^borrar/usuario/(?P<pk>\d+)$', views.UsuarioDelete.as_view(), name='usuario_delete'),
    url(r'^usuario/perfil/$',views.MyProfileView.as_view(), name='user_profile'),
    url(r'^usuario/perfil/id/(?P<pk>\d+)/$',views.UsuarioDetail.as_view(), name='user_profile_id'),
    url(r'^usuario/perfil/username/(?P<slug>[\w+.@+-]+)/$',views.UsuarioDetail.as_view(), name='user_profile_slug'),
    url(r'^usuario/login/$',views.LoginView.as_view(), name='user_login'),
    url(r'^usuario/logout/$', views.LogoutView.as_view(),name='user_logout'),
    url('^', include('django.contrib.auth.urls')),
    url(r'^password/recuperar/$',auth_views.password_reset,{
        'template_name':'usuario/registration/password_reset_form.html','email_template_name':'usuario/registration/password_reset_email.html','subject_template_name':'usuario/registration/password_reset_subject.txt'                                    },name='password_reset'),
    url(r'^password/recuperar/hecho$',auth_views.password_reset_done,{'template_name':'usuario/registration/password_reset_done.html'},name='password_reset_done'),
    url(r'^password/cambiar/$',auth_views.password_change,{
        'template_name':'usuario/registration/password_change_form.html'
    },name='password_change'),
    url(r'^password/cambiar/hecho$',auth_views.password_change_done,{
        'template_name':'usuario/registration/password_change_done.html'
    },name='password_change_done'),    #url(r'^usuario/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',       auth_views.password_reset_confirm,{'post_reset_redirect' : '/user/password/hecho/'}),
    #url(r'^usuario/password/hecho/$', auth_views.password_reset_complete),
]