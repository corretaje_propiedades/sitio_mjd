from django.shortcuts import render

# Create your views here.
#-*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CustomUserCreationForm, CustomUserRegisterForm
from .models import UsuarioCorredora

class UsuarioRegistre(CreateView):
    form_class = CustomUserRegisterForm
    template_name = 'propiedad/propiedad_form.html'
    success_url = reverse_lazy('user_profile')


class UsuarioDetail(LoginRequiredMixin,DetailView):
    login_url = '/usuario/login/'
    redirect_field_name = 'user_profile'
    model = UsuarioCorredora
    slug_field = "username"

class UsuarioList(LoginRequiredMixin,ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'user_profile'
    model = UsuarioCorredora

class UsuarioCreate(LoginRequiredMixin,CreateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'user_profile'
    form_class = CustomUserCreationForm
    template_name = 'propiedad/propiedad_form.html'
    success_url = reverse_lazy('usuario_list')


class UsuarioUpdate(LoginRequiredMixin,UpdateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'user_profile'
    model = UsuarioCorredora
    success_url = reverse_lazy('usuario_list')


class UsuarioDelete(LoginRequiredMixin,DeleteView):
    login_url = '/usuario/login/'
    redirect_field_name = 'user_profile'
    model = UsuarioCorredora
    success_url = reverse_lazy('usuario_list')


try:
    import urlparse
except ImportError:
    from urllib import parse as urlparse # python3 support
from class_based_auth_views.utils import default_redirect
from django.contrib import auth
from django.contrib.auth import REDIRECT_FIELD_NAME, login
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect, resolve_url
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic import View
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.edit import FormView
from django.conf import settings
from django.core.urlresolvers import reverse


class LoginView(FormView):
    """
    This is a class based version of django.contrib.auth.views.login.
    Usage:
        in urls.py:
            url(r'^login/$',
                LoginView.as_view(
                    form_class=MyCustomAuthFormClass,
                    success_url='/my/custom/success/url/),
                name="login"),
    """
    username=''
    form_class = AuthenticationForm
    redirect_field_name = 'user_profile_slug'
    template_name = 'usuario/registration/login.html'

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        The user has provided valid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        can check the test cookie stuff and log him in.
        """
        self.check_and_delete_test_cookie()
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)

    def form_invalid(self, form):
        """
        The user has provided invalid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        set the test cookie again and re-render the form with errors.
        """
        self.set_test_cookie()
        return super(LoginView, self).form_invalid(form)

    def get_success_url(self):
        if self.success_url:
            redirect_to = self.success_url
        else:
            redirect_to = self.request.POST.get(
                self.redirect_field_name,
                self.request.GET.get(self.redirect_field_name, ''))

        netloc = urlparse.urlparse(redirect_to)[1]
        if not redirect_to:
            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
        # Security check -- don't allow redirection to a different host.
        elif netloc and netloc != self.request.get_host():
            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
        return redirect_to

    def set_test_cookie(self):
        self.request.session.set_test_cookie()

    def check_and_delete_test_cookie(self):
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
            return True
        return False

    def get(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.get(), but adds test cookie stuff
        """
        self.set_test_cookie()
        self.username = request.user.username
        return super(LoginView, self).get(request, *args, **kwargs)


class LogoutView(TemplateResponseMixin, View):
    template_name = "registration/logout.html"
    redirect_field_name = "next"

    def get(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect(self.get_redirect_url())
        context = self.get_context_data()
        return self.render_to_response(context)

    def post(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            auth.logout(self.request)
        return redirect(self.get_redirect_url())

    def get_context_data(self, **kwargs):
        context = kwargs
        redirect_field_name = self.get_redirect_field_name()
        redirect_field_value = self.request.POST.get(
            redirect_field_name, self.request.GET.get(redirect_field_name, ''))
        context.update({
            "redirect_field_name": redirect_field_name,
            "redirect_field_value": redirect_field_value,
            })
        return context

    def get_redirect_field_name(self):
        return self.redirect_field_name

    def get_redirect_url(self, fallback_url=None, **kwargs):
        if fallback_url is None:
            fallback_url = settings.LOGIN_URL
        kwargs.setdefault("redirect_field_name", self.get_redirect_field_name())
        return default_redirect(self.request, fallback_url, **kwargs)


"""
^login/$ [name='login']
^logout/$ [name='logout']
^password_change/$ [name='password_change']
^password_change/done/$ [name='password_change_done']
^password_reset/$ [name='password_reset']
^password_reset/done/$ [name='password_reset_done']
^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$ [name='password_reset_confirm']
^reset/done/$ [name='password_reset_complete']

"""

from django.contrib.auth.decorators import login_required
from django.views.generic.detail import SingleObjectMixin
from django.views.generic import UpdateView
from django.utils.decorators import method_decorator

class ProfileObjectMixin(SingleObjectMixin):
    """
    Provee la vista para el perfil de usuario actual
    """

    model = UsuarioCorredora

    def get_object(self, queryset=None):
        try:
            return self.request.user.get_profile()
        except UsuarioCorredora.DoesNotExist:
            raise NotImplemented(
                "Tal vez el usuario no tiene un perfil asociado?"
            )

    @method_decorator(login_required)
    def dispatch(self,request, *args,**kwargs):
        """
        Se asegura que solo los usuarios autenticados acceden
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        klass = ProfileObjectMixin
        return super(klass,self).dispatch(request, *argas,**kwargs)


class ProfileUpdateView(ProfileObjectMixin,UpdateView):
    """
    Una vista que muestra un formulario para editar el perfil
    """
    pass


from django.contrib.auth.models import User

class MyProfileView(TemplateView):
    template_name = 'usuario/myprofile.html'

    def get_context_data(self, **kwargs):
        context = super(MyProfileView, self).get_context_data(**kwargs)
        user = User.objects.get(id = self.request.user.id)
        user_object = UsuarioCorredora.objects.get(id = user.id)

        if user_object:
            context['usuario'] = user_object
        else:
            context['usuario'] = user

        return context