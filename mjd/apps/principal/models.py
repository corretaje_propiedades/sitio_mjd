from django.db import models
import datetime
# Create your models here.
from django.utils.translation import ugettext_lazy as _
from apps.propiedad.models import Propiedad
from apps.localizacion.models import Region, Comuna
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation

from gm2m import GM2MField
from django.urls import reverse

class Busqueda(models.Model):
    ADQUISICION_CHOICES=(
        ('Buy','Comprar'),
        ('Lease','Arrendar')
    )
    PROPIEDAD_CHOICES=Propiedad.PROPIEDAD_CHOICES
    ESTADO_CHOICES=Propiedad.ESTADO_CHOICES

    tipo_adquisicion=models.CharField(max_length=20,choices=ADQUISICION_CHOICES)
    estado = models.CharField(max_length=30, choices=ESTADO_CHOICES)
    tipo_propiedad=models.CharField(max_length=30,choices=PROPIEDAD_CHOICES)
    #lugar=models.CharField(max_length=40)
    tiempo_publicacion=models.DateField(auto_now_add=True, auto_now=False)
    fecha_busqueda= models.DateTimeField(auto_now=True)
    set_resultados = models.ManyToManyField(Propiedad)
    
    ubicacion = models.ForeignKey(Comuna, related_name='busqueda',null=True, blank=True, on_delete=models.CASCADE)
    ubicacion_region = models.ForeignKey(Region, related_name='busqueda',null=True, blank=True, on_delete=models.CASCADE)
    
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False, related_name='%(class)s_requests_created')
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.tipo_adquisicion +" "+self.tipo_propiedad

    @property
    def Busqueda(self):
        return self.__str__()


    def get_absolute_url(self):
        return reverse('busqueda_especifica', kwargs={'pk':self.pk})

    class Meta:
        app_label='principal'
        verbose_name = _("Búsqueda de propiedades")
        verbose_name_plural = _("Búsquedas de propiedades")
        ordering = ("fecha_busqueda","tipo_adquisicion")

from django.utils import timezone


###
class SetHomeImage(models.Model):
    nombre = models.CharField(max_length=200)
    fecha = models.DateField(default=timezone.now)


    def get_absolute_url(self):
        return reverse('setimageshome_detail', kwargs={'pk':self.pk})


    def __str__(self):
        return self.nombre

    class Meta:
        app_label='principal'
        verbose_name='set-imagenes-home'
        verbose_name_plural='sets-imagenes-home'


class HomeImage(models.Model):
    imagen = models.ImageField(upload_to='home/imagen')
    etiqueta = models.CharField(max_length=100)
    set_images = models.ForeignKey(SetHomeImage,related_name='home_images', blank=True,null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.etiqueta

    class Meta:
        app_label='principal'
        verbose_name='imagen-home'
        verbose_name_plural='imágenes home'

from autoslug import AutoSlugField

class ConfiguracionHome(models.Model):
    nombre = models.CharField(max_length=200, help_text="Nombre de la configuración en particular")
    slug_nombre = AutoSlugField(blank=True,populate_from='nombre', editable=True)
    set_images =  models.ForeignKey(SetHomeImage, related_name='configuraciones_home', blank=True,null=True, on_delete=models.CASCADE)#--->borrar
    mostrar_ultimas_propiedades = models.BooleanField(default=True, help_text="Si mostrar o no últimas propiedades")
    cantidad_propiedades = models.PositiveIntegerField(blank = True)
    orden_propiedades = models.BooleanField(default=True, help_text="Si va en orden descendente o ascendente según fecha de publicación")
    clase_css_propiedades_home = models.CharField(max_length=100,blank = True, help_text="ingresa un nombre de la clase css especial  (o varias separadas de ,) para esta configuración de las propiedades a mostrar")
    mostrar_destacados = models.BooleanField(default=True)
    cantidad_destacados = models.PositiveIntegerField(blank = True, help_text="Ingresa la cantidad de destacados a mostrar")
    aleatorio_destacados = models.BooleanField(default=False,blank = True,help_text="Selecciona si se ordenan aleatoriamente desde la fecha definida para mostrar los destacados")
    primera_fecha_destacados = models.DateField(default=timezone.now)
    clase_css_propiedades_destacadas_home = models.CharField(max_length=100, blank = True,help_text="define una clase css adicional (o varias separadas de ,) para los destacados ")
    activar_blog = models.BooleanField(default=False, blank = True, help_text="¿Deseas activar el blog?")

    @models.permalink
    def get_absolute_url(self):
        return 'configuracion:post', (self.slug_nombre,)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label='principal'
        verbose_name='configuracion-home'
        verbose_name_plural='configuraciones-home'

class PropiedadDestacada(models.Model):
    propiedad=models.ForeignKey(Propiedad, related_name='destacadas', on_delete=models.CASCADE)
    activada=models.BooleanField(default=True)

    def __str__(self):
        return self.propiedad.nombre
