# Generated by Django 2.0 on 2017-12-22 01:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0014_auto_20170923_1745'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homeimage',
            name='set_images',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='home_images', to='principal.SetHomeImage'),
        ),
    ]
