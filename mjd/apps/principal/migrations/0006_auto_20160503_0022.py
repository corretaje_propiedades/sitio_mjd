# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-03 03:22
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0005_auto_20160503_0021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuracionhome',
            name='primera_fecha_destacados',
            field=models.DateField(default=datetime.datetime(2016, 5, 3, 0, 22, 8, 801607)),
        ),
    ]
