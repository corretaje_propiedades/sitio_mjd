from django.contrib import admin
from .models import Busqueda, HomeImage, SetHomeImage,ConfiguracionHome, PropiedadDestacada
# Register your models here.
admin.site.register(Busqueda)


class ImagePropiedadInline(admin.StackedInline):
    model = HomeImage
    can_delete = True
    verbose_name_plural = 'Imagen de Home'

class SetImagenesAdmin(admin.ModelAdmin):
    inlines= (ImagePropiedadInline,)

admin.site.register(SetHomeImage,SetImagenesAdmin)

admin.site.register(ConfiguracionHome)

admin.site.register(PropiedadDestacada)