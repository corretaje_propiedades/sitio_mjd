from django import forms
from django.forms import ModelForm
from .models import Busqueda
from tools.widgets.semantic_search import AutoCompleteSearchWidget
from apps.localizacion.models import Region, Comuna


class BusquedaForm ( ModelForm ):

    COMUNAS=[(comuna.pk, comuna.nombre) for comuna in Comuna.objects.all()]

    # the fields using this widgets enabled the semantic ui dropdown and search

    tipo_adquisicion=forms.ChoiceField(
        widget=AutoCompleteSearchWidget({
            'attrs':{'id':'tipo_adquisicion','name':"Tipo de Adquisición"},
            "choices":Busqueda.ADQUISICION_CHOICES
            })
        )
    tipo_propiedad=forms.ChoiceField(
        widget=AutoCompleteSearchWidget({
            'attrs':{'id':'tipo_propiedad','name':"Tipo de Propiedad"},
            "choices":Busqueda.PROPIEDAD_CHOICES
            })
        )
            
    estado=forms.ChoiceField(
        widget=AutoCompleteSearchWidget({
            'attrs':{'id':'estado','name':"Estado de propiedad"},
            "choices":Busqueda.ESTADO_CHOICES
            })
        )

    ubicacion=forms.ChoiceField(
        widget=AutoCompleteSearchWidget({
            'attrs':{'id':'ubicacion','name':'Ubicación'},
            "choices":COMUNAS
            })
        )

    # ubicacion =
    class Meta:
        model = Busqueda
        fields = [ 'tipo_adquisicion',
                   'tipo_propiedad',
                   'estado',
                   'ubicacion',
                   ]


    def __init__(self, *args, **kwargs):
        super(BusquedaForm,self).__init__(*args,**kwargs)
        print(self.fields)
        self.fields['tipo_adquisicion'].widget.attrs['class'] = "ui fluid search selection dropdown"
        self.fields['tipo_propiedad'].widget.attrs['class'] = "ui fluid search selection dropdown"
        self.fields['estado'].widget.attrs['class'] = "ui fluid search selection dropdown"
        self.fields['ubicacion'].widget.attrs['class'] = "ui fluid search selection dropdown"
        #self.fields['ubicacion'].widget.attrs['id'] = 'id_ubicacion'
        #self.fields['ubicacion'].widget.attrs.update({'class' : 'ui search dropdown  prop-form', 'id' :'id_ubicacion'})
        self.fields['tipo_adquisicion'].widget.attrs['placeholder'] = 'Tipo adquisición'
        #self.fields['ubicacion'].widget.attrs['placeholder'] = "Ubicación"
        #self.fields['email'].label = ""



from django.forms import formset_factory
from django.forms.models import inlineformset_factory

from .models import HomeImage, SetHomeImage
from djangoformsetjs.utils import formset_media_js


class SetHomeImagesForm ( ModelForm ):
    class Meta:
        model = SetHomeImage
        fields = [
            'nombre'
        ]
        js = formset_media_js + (
            # Other form media here
        )


HomeImageFormSet = inlineformset_factory ( SetHomeImage, HomeImage, fields=('imagen', 'etiqueta',),extra=1 )
