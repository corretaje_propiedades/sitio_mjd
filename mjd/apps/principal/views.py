from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, ListView, DetailView

from django.views.generic.edit import CreateView
from django.views.generic.edit import FormMixin

from django.conf import settings
from django.urls import reverse_lazy
import itertools


from .models import Busqueda, ConfiguracionHome, HomeImage, SetHomeImage, PropiedadDestacada
from .forms import BusquedaForm
from apps.localizacion.models import Comuna,Region
from apps.propiedad.models import Propiedad
from django.db.models import Q


class PrincipalView(CreateView):
    template_name = 'principal/index.html'
    model = Busqueda
    form_class = BusquedaForm

    def get_context_data(self, **kwargs):
        context = super(PrincipalView, self).get_context_data(**kwargs)
        print("Vista de busqueda")
        try:
            settings.CONFIG_HOME_ID
        except NameError:
            CONFIG_HOME_ID_exists = False
        else:
            CONFIG_HOME_ID_exists = True

        if not CONFIG_HOME_ID_exists:
            conf = ConfiguracionHome.objects.last()
            context['conf'] = conf
        else:
            conf = ConfiguracionHome.objects.get(id = settings.CONFIG_HOME_ID)
            context['conf'] = conf
        #buscar la ultima configuracion para poder filtrar los parámetros de la vista home
        #Cargar propiedades segun configuracion
        #ultimas_propiedades = Propiedad.objects.all().order_by('-fecha_registro')[:conf.cantidad_propiedades]
        if conf.orden_propiedades:
            ultimas_propiedades = Propiedad.objects.all().latest('fecha_registro')
            #ultimas_propiedades = ultimas_propiedades[::-1]

        context['ultimas_propiedades'] = ultimas_propiedades #queryset

        print(context['ultimas_propiedades'])

        if conf.mostrar_destacados:
            print("Obteniendo propiedades destacadas")
            propiedades_destacadas=PropiedadDestacada.objects.all()
            if propiedades_destacadas:
                print(propiedades_destacadas.latest('id'))
                context['propiedades_destacadas'] = propiedades_destacadas
            else:
                print("no hay propiedades destacadas aún")
                context['propiedades_destacadas'] =[] 
        return context

class BusquedaEspecificaListView(FormMixin,ListView):
    """
    Busqueda bajo parámetros determinados por formulario de busqueda
    """
    paginate_by = 10
    template_name = 'principal/busqueda_especifica_list.html'
    queryset = Propiedad.objects.none()
    model = Propiedad
    success_url = reverse_lazy('busqueda_especifica')
    model = Busqueda
    form_class = BusquedaForm
    query = {}
    Comuna = Comuna()

    def get_queryset(self):
        print('Consulta')
        print(self.args)
        query = {}
        comunas =[]
        regiones = []
        QUERY=None
        query_loc=None
        queryset=None


        #if not self.request.GET:
        #raise error
        if self.request.GET.get('ubicacion'):
            ubicacion = int(self.request.GET.get('ubicacion'))

            #7 es de region
            #8 es de comuna
            #se toma campo localizacion dentro de la propiedad
            query_loc = Q(localizacion__comuna__pk = ubicacion)


        else:
            query_loc = Q(pk__gte=1)


        if query_loc:
            QUERY=query_loc

        print("QUery for localizacion %s" %query_loc)

        query_adq=None

        if self.request.GET.get('tipo_adquisicion'):
            query.update({'tipo_adquisicion' : self.request.GET.get('tipo_adquisicion')})
            if query['tipo_adquisicion'] == 'Buy':
                print('Comprar propiedad')
                query_adq=Q(caracter = 'Buy')
            elif  query['tipo_adquisicion'] == 'Lease':
                print('Arrendar propiedad')
                query_adq=Q(caracter = 'Lease')


        if QUERY:
            print("Añadiendo AND query tipo:")
            if query_adq:
                QUERY=QUERY  & query_adq
        else:
            if query_adq:
                QUERY= query_adq


        query_tpo=None
        if self.request.GET.get('tipo_propiedad'):
            query.update({'tipo_propiedad' : self.request.GET.get('tipo_propiedad')})
            query_tpo=Q(tipo_propiedad = query['tipo_propiedad'])

        if QUERY:
            if query_tpo:
                QUERY=QUERY  & query_tpo
        else:
            if query_tpo:
                QUERY=query_tpo

        query_est=None
        if self.request.GET.get('estado'):
            query.update({'estado' : self.request.GET.get('estado')})
            query_est=Q(estado = query['estado'])

        if QUERY:
            if query_est:
                QUERY=QUERY  & query_est
        else:
            if query_est:
                QUERY=query_est

        if QUERY:
            print("Filtro: %s" %QUERY)
            queryset=Propiedad.objects.filter(QUERY)
        else:
            print("Obtener todo")
            queryset=Propiedad.objects.all()
        self.query = QUERY
        return queryset

from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

##########################CRUD


class BusquedaList(LoginRequiredMixin,ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Busqueda
    paginate_by = 10


class BusquedaCreate(LoginRequiredMixin,CreateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Busqueda
    template_name = 'principal/busqueda_form.html'
    success_url = reverse_lazy('busqueda_list')


class BusquedaUpdate(LoginRequiredMixin,UpdateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Busqueda
    success_url = reverse_lazy('busqueda_list')
    fields = ['tipo_adquisicion',
              'tipo_propiedad',
              'estado',
              'tiempo_publicacion',
              'fecha_busqueda',
              'set_resultados'
              ]


class BusquedaDelete(LoginRequiredMixin,DeleteView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Busqueda
    success_url = reverse_lazy('busqueda_list')


class BusquedaDetail(LoginRequiredMixin,DetailView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = Busqueda

class ConfiguracionHomeList(LoginRequiredMixin,ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = ConfiguracionHome
    paginate_by = 10


class ConfiguracionHomeCreate(LoginRequiredMixin,CreateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = ConfiguracionHome
    template_name = 'principal/configuracionhome_form.html'
    success_url = reverse_lazy('configuracionhome_list')
    fields = [
            'nombre' ,
            'slug_nombre',
            'set' ,
            'mostrar_ultimas_propiedades' ,
            'cantidad_propiedades' ,
            'orden_propiedades',
            'clase_css_propiedades_home',
            'mostrar_destacados' ,
            'cantidad_destacados' ,
            'aleatorio_destacados',
            'primera_fecha_destacados' ,
            'clase_css_propiedades_destacadas_home',
            'activar_blog',
    ]

class ConfiguracionHomeUpdate(LoginRequiredMixin,UpdateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = ConfiguracionHome
    template_name = 'principal/configuracionhome_form.html'
    success_url = reverse_lazy('configuracionhome_list')
    fields = [
            'nombre' ,
            'slug_nombre',
            'set' ,
            'mostrar_ultimas_propiedades' ,
            'cantidad_propiedades' ,
            'orden_propiedades',
            'clase_css_propiedades_home',
            'mostrar_destacados' ,
            'cantidad_destacados' ,
            'aleatorio_destacados',
            'primera_fecha_destacados' ,
            'clase_css_propiedades_destacadas_home',
            'activar_blog',
    ]

class ConfiguracionHomeDelete(LoginRequiredMixin,DeleteView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = ConfiguracionHome
    success_url = reverse_lazy('configuracionhome_list')


class ConfiguracionHomeDetail(LoginRequiredMixin,DetailView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = ConfiguracionHome

######################################
##CRUD ImageHome + SetImageHome

from .forms import SetHomeImagesForm, HomeImageFormSet
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404




class SetImagesHomeList(LoginRequiredMixin,ListView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'principal/setimages_list.html'
    model = SetHomeImage
    paginate_by = 10

class SetImagesHomeCreate(LoginRequiredMixin,CreateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'principal/setimages_form.html'
    model = SetHomeImage
    form_class = SetHomeImagesForm
    success_url = reverse_lazy('setimageshome_list')


    def get(self,request, *args, **kwargs):
        """
        Handle GET requests and instantiates blank versions of the forms and its inline formsets

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        homeimage_formset = HomeImageFormSet()
        return self.render_to_response(
            self.get_context_data(
                form=form,
                homeimage_formset=homeimage_formset
            )
        )

    def post(self, request, *args, **kwargs):
        """

        :param self:
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        print("Guardando nueva instancia de form")
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        homeimage_formset = HomeImageFormSet(self.request.POST, self.request.FILES)
        if form.is_valid():
            print("Form es valido")
            return self.form_valid(form, homeimage_formset)

        else:
            print("Form no es valido")
            return self.form_invalid(form, homeimage_formset)


    def form_valid(self, form, homeimage_formset):
        """
        Called if all forms are valid, Create SetImage instance along with associated imageshome, then redirects to a succes page
        :param self:
        :param form:
        :return:
        """
        self.object = form.save()
        print("Se guarda instancia")
        print(homeimage_formset)
        homeimage_formset.instance = self.object
        homeimage_formset.save()
        print("Se guarda instancia de imagen : id")
        print(str(homeimage_formset.instance.id))
        return HttpResponseRedirect(self.object.get_absolute_url())


    def form_invalid(self, form, homeimage_formset):
        """
        Callid if a form is invalid- Re render context data qith data filled forms and errors.
        :param form:
        :param imagehome_form:
        :return:
        """
        return self.render_to_response(
            self.get_context_data(
                form=form,
                homeimage_formset=homeimage_formset
            )
        )


class SetImagesHomeUpdate(LoginRequiredMixin,UpdateView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = SetHomeImage
    form_class = SetHomeImagesForm
    template_name = 'principal/setimages_form.html'
    success_url = reverse_lazy('setimageshome_list')

    def get_context_data(self, **kwargs):
        context = super(SetImagesHomeUpdate, self).get_context_data(**kwargs)
        if self.request.POST:
            context['form']=SetHomeImage(self.request.POST, instance=self.object)
            context['homeimage_formset']=HomeImageFormSet(initial=homeimages)
        else:
            context['form']=SetHomeImage(instance=self.object)
            context['homeimage_formset']=HomeImageFormSet(initial=homeimages)
        return context



    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline formsets with the passed POST variables and then checking them for validity
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        homeimage_formset = HomeImageFormSet(self.request.POST)
        if (form.is_valid() and homeimage_formset.is_valid()):
            return self.form_valid(form, homeimage_formset)
        else:
            return self.form_invalid(form, homeimage_formset)



    def form_valid(self, form):
        """
        Called if all forms are valid, Create SetImage instance along with associated imageshome, then redirects to a succes page
        :param self:
        :param form:
        :param imageform_valid:
        :return:
        """
        self.object = form.save()
        formset.instance = self.object
        formset.save()
        return HttpResponseRedirect(self.object.get_absolute_url())

    def form_invalid(self, form, imagehome_form):
        """
        Callid if a form is invalid- Re render context data qith data filled forms and errors.
        :param form:
        :param imagehome_form:
        :return:
        """
        return self.render_to_response(
            self.get_context_data(
                form=form,
                imagehome_form=imagehome_form
            )
        )


    def get(self, request, *args, **kwargs):
        self.object = SetHomeImage.objects.get(pk=self.request.pk)
        form_class = self.get_form_class()
        print("Tipo de formulario a editar:")
        print(form_class)
        form = self.get_form(form_class)
        homeimages = HomeImage.objects.get(set = self.object)
        return self.render_to_response(self.get_context_data(form=form, homeimages=homeimages))




class SetImagesHomeDelete(LoginRequiredMixin,DeleteView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = SetHomeImage
    success_url = reverse_lazy('setimageshome_list')
    template_name = 'principal/setimages_confirm_delete.html'

class SetImagesHomeDetail(LoginRequiredMixin,DetailView):
    login_url = '/usuario/login/'
    redirect_field_name = 'redirect_to'
    model = SetHomeImage
    template_name = 'principal/setimages_detail.html'

######################################

from dal_select2_queryset_sequence.views import Select2QuerySetSequenceView

from queryset_sequence import QuerySetSequence

from apps.localizacion.models import Region, Comuna


class UbicacionAutocompleteView(Select2QuerySetSequenceView):
    def get_queryset(self):
        comunas = Comuna.objects.all()
        regiones = Region.objects.all()

        if self.q:
            comunas = comunas.filter(nombre__icontains=self.q)
            regiones = regiones.filter(nombre__icontains=self.q)

        # Aggregate querysets
        qs = QuerySetSequence(comunas, regiones)

        if self.q:
            # This would apply the filter on all the querysets
            qs = qs.filter(nombre__icontains=self.q)

        # This will limit each queryset so that they show an equal number
        # of results.
        qs = self.mixup_querysets(qs)

        return qs
