from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill

class Thumbnail(ImageSpec):
    processors = [ResizeToFill(300, 250)]
    format = 'JPEG'
    options = {'quality': 60}

register.generator('mjd:thumbnail', Thumbnail)


class Destacada(ImageSpec):
    processors = [ResizeToFill(280, 250)]
    format = 'JPEG'
    options = {'quality': 90}

register.generator('mjd:destacada', Destacada)

class Principal(ImageSpec):
    processors = [ResizeToFill(1200,500)]
    format = 'JPEG'
    options = {'quality': 90}

register.generator('mjd:principal', Principal)


class Propiedad(ImageSpec):
    processors = [ResizeToFill(600,500)]
    format = 'JPEG'
    options = {'quality': 90}

register.generator('mjd:propiedad', Propiedad)
