from django.conf.urls import url
from .views import PrincipalView
from . import views

app_name='principal'
urlpatterns = [
    url(r'^$',PrincipalView.as_view(),name='home_page'),
    url(r'^busqueda$', views.BusquedaEspecificaListView.as_view(), name='busqueda_especifica'),
    ################################
    url(r'^listar/busquedas$',views.BusquedaList.as_view(), name='busqueda_list'),# X
    url(r'^agregar/busqueda$', views.BusquedaCreate.as_view(), name='busqueda_new'),# X
    url(r'^editar/busqueda/(?P<pk>\d+)$', views.BusquedaUpdate.as_view(), name='busqueda_edit'),# X
    url(r'^borrar/busqueda/(?P<pk>\d+)$', views.BusquedaDelete.as_view(), name='busqueda_delete'),# X
    url(r'^resultados/busqueda/(?P<pk>\d+)$', views.BusquedaDetail.as_view(), name='busqueda_detail'),# X
    #####CRUD Cónfiguracion
    url(r'^listar/conf_home$',views.ConfiguracionHomeList.as_view(), name='conf_home_list'),# X
    url(r'^agregar/conf_home', views.ConfiguracionHomeCreate.as_view(), name='conf_home_new'),# X
    url(r'^editar/conf_home/(?P<pk>\d+)$', views.ConfiguracionHomeUpdate.as_view(), name='conf_home_edit'),# X
    url(r'^borrar/conf_home/(?P<pk>\d+)$', views.ConfiguracionHomeDelete.as_view(), name='conf_home_delete'),# X
    url(r'^resultados/conf_home/(?P<pk>\d+)$', views.ConfiguracionHomeDetail.as_view(), name='conf_home_detail'),# X
    #####CRUD SetImages
    url(r'^listar/images_home$',views.SetImagesHomeList.as_view(), name='setimageshome_list'),# X
    url(r'^agregar/images_home', views.SetImagesHomeCreate.as_view(), name='setimageshome_new'),# X
    url(r'^editar/images_home/(?P<pk>\d+)$', views.SetImagesHomeUpdate.as_view(), name='setimageshome_edit'),# X
    url(r'^borrar/images_home/(?P<pk>\d+)$', views.SetImagesHomeDelete.as_view(), name='setimageshome_delete'),# X
    url(r'^resultados/images_home/(?P<pk>\d+)$', views.SetImagesHomeDetail.as_view(), name='setimageshome_detail'),# X
    #################################
    url(r'^ubicacion-autocomplete/$',  views.UbicacionAutocompleteView.as_view(),name='ubicacion-autocomplete'),
]