from django import forms
from django.template.loader import render_to_string

class AutoCompleteSearchWidget(forms.widgets.Input):
    def render(self, name, value, attrs=None):
        print("Atributos: %s"%self.attrs)
        data = {'attrs': self.attrs['attrs'],'choices': self.attrs["choices"]}
        return render_to_string('widgets/_autocomplete_search_widget.html', data)
