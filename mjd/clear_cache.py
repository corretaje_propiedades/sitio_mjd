import django
from django.core.cache import cache

django.setup() # Needed to make django ready from the external script
cache.clear()