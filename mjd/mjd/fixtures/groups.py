from groups_manager.models import Group, GroupType, Member

admin = 'Administración'
seller = 'Vendedor'
client = 'Cliente'

admin_group_type = models.GroupType.objects.create(label=admin )
seller_group_type = models.GroupType.objects.create(label=seller)
client_group_type = models.GroupType.objects.create(label=client)


admin_group = Group.objects.create(name = admin, group_type = admin_group_type)
seller_group = Group.objects.create(name = seller, group_type = seller_group_type)
client_group = Group.objects.create(name = client, group_type = client_group_type)

