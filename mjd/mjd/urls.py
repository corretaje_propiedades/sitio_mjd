"""mjd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views

from django.contrib.sitemaps.views import sitemap
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),    
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    #url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^',include('apps.principal.urls', 
            namespace='principal')),
    url(r'^',include('apps.pagina.urls', 
            namespace='pagina')),
    url(r'^',include('apps.contactform.urls', 
            namespace='contact_form')),
    url(r'^',include('apps.propiedad.urls', 
            namespace='propiedad')),
    url(r'^',include('apps.localizacion.urls', 
            namespace='localizacion')),
    #url(r'^',include('apps.perfil.urls',namespace='perfil')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^buscar/', include('haystack.urls')),
    #url(r'^weblog/', include('zinnia.urls', namespace='zinnia')),
    url(r'^comments/', include('django_comments.urls')),
    #url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    #url(r'^sitemap-(?P<section>.+)\.xml$', sitemap, {'sitemaps': sitemaps}),
    #url(r'^accounts/', include('userena.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

