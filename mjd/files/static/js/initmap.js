
/*
* Selector múltiple
* */



/*
Verificar dirección
==================

Aquí se ejecuta una consulta ajax en servidor que entrega un string con la dirección, en servidor se completa con el país y se hace un geocoder para obtener lonlat.

Una vez que se devuelve lonlat, se visualiza en el mapa ol3 la posición y se coloca un marcador.

Si se realiza otra consulta, se debe quitar el marcador y colar otro en la nueva posición.

metodo: GET
 */

var direccion;
var com_id;
var comuna;
var dir_com;
var nuevo_punto;

$(document).ready(function(){
    $("#verificar").click(function(event){
        event.preventDefault();
        direccion = document.getElementById("id_ubicacion").value;
        com_id = document.getElementById("id_comuna");
        comuna = com_id.options[com_id.selectedIndex].text;
        dir_com = direccion +", "+ comuna
        console.log("Verificando direccion");
        console.log(direccion +", "+ comuna)
        $.ajax({
           type: 'GET',
           cache: true,
           data: {
               'direccion': dir_com
           },
           contentType: "application/json;charset=utf-8",
           dataType: 'json',
           success: function(resp){
               console.log("Verificación exitosa, ver mapa");
               console.log(resp['v_lat']);
               console.log(resp['v_lon']);
               nuevo_punto = ol.proj.fromLonLat([resp['v_lon'],resp['v_lat']]);
               var pan = ol.animation.pan({
                  duration: 2000,
                  source: /** @type {ol.Coordinate} */ (view.getCenter())
                });
               map.beforeRender(pan);
               view.setCenter(nuevo_punto);
               view.setResolution(18);
           },
           error: function(ts){
               console.log("Hubo un error");
           }
        });
    });
})